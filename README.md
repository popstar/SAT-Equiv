SAT-Equiv : An equivalence checker using SAT solving.
=====================================================

Websites :
- https://gitlab.inria.fr/popstar/SAT-Equiv
- http://projects.lsv.fr/satequiv
- http://lsv.fr/~dallon

0. Changes
1. Installation
2. Usage
3. Input format
4. Output
5. Uninstall

LICENSE
-------

GNU AFFERO GENERAL PUBLIC LICENSE


CHANGES
-------

* More efficient.
* Stops when an attack is found.
* Minor bugs correction.

INSTALLATION
------------

Note that SAT-Equiv has not been tested on non-Unix platforms.

To install SAT-Equiv, you will first need minisat.
You can find it at: 

    http://www.minisat.se/MiniSat.html


It is also possible to get it on Debian/Ubuntu through:

    apt-get install minisat

To install SAT-Equiv, you will need ocaml.
Version 4.04.2 is known to work.

Then reach the extracted directory SAT-Equiv,
and enter: 

    make

Once the installation is done, an executable 'satequiv' should be created.


USAGE
-----

Synopsis:

	satequiv FILE [-p|-q|-e] [-r|-k] [-c|-o|-n] [--three-constants | -w | -t ] [-help]  

Options:
  -p : Tests only the inclusion of P in Q
  -q : Tests only the inclusion of Q in P
  -e : Tests the equivalence between P and Q (Default)
  -r : Removes the files used by the SAT solver (Default)
  -k : Keeps the files used by th SAT solver
  -c : Checks compliancy and print a message if the protocol is not type-compliant. (Default)
       Recall that type-compliancy is an hypothesis of the soundness theorem for SAT-Equiv
  -o : Only checks compliancy. As it is very quick, it is recommended to first check compliancy
       before launching the tool.
  -n : Does not check compliancy at all.
  --three-constants Uses three constants (c0,c1,c_<omega,omega>) (default)
  -w Uses only one constant plus Omega (c0,Omega)
  -t Uses only two constants (c0,c1)
  -help : Display help.


Note that using:
./satequiv exemple.pi -p -t
./satequiv exemple.pi -p -w
./satequiv exemple.pi -q -t
./satequiv exemple.pi -q -w

And adding the corresponding times
should be equivalent to get them through:
./satequiv exemple.pi -t
./satequiv exemple.pi -w

But it is NOT equivalent to the time taken by:
./satequiv exemple.pi

INPUT FORMAT
------------

The input format is described in the file format.pi.

Other examples are in the Examples/ directory.


OUTPUT
------

If an attack is found, then the tool will give a trace of this attack.

Else, the protocol is proven to be sure if SAT-Equiv terminates.
Then you will get the following message :

No attack has been found.

Additionnal statistics are printed:
 * Number of nodes of the planning graph in the final step.
 * Number of steps.
 * Number of calls of MiniSAT.

When an attack is found:
 * Approximative number of variables.
 * Number of clauses. 

The exact number of variables is never computed in SAT-Equiv.

UNINSTALL
---------

Go to the SAT-Equiv directory, and execute the following commands:

    rm satequiv
    make clean

To uninstall minisat, see minisat documentation.
