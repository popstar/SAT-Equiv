
def printSeparation(s):
    print("______ ",end='')
    print(s,end='')
    print(" ______")

def printPublics():
    printSeparation("Declaration of public constants")
    print("yes:bool")
    print("no:bool")

def printPrives(n):
    printSeparation("Declaration of private constants")
    for i in range(1,n):
        print("toka"+str(i)+":token")
    for i in range(1,n):
        print("tokb"+str(i)+":token")
    for i in range(1,n):
        print("k"+str(i)+":key")

def printProcess(n,p,yes):
    printSeparation("Declaration of process "+p)
    print("")
    c=0
    for i in range(1,n):
        sc=str(c)
        si=str(i)
        print("out("+sc+",senc(toka"+si+",k"+si+")).out("+sc+",senc(tokb"+si+",k"+si+"));")
        c+=1
    print("")
    for i in range(1,n):
        sc=str(c)
        si=str(i)
        print("in("+sc+",senc(x:token,k"+si+")).out("+sc+",x);")
        c+=1
    print("")
    for i in range(1,n):
        sc=str(c)
        si=str(i)
        print("out("+sc+",senc(senc("+yes+",toka"+si+"),tokb"+si+"));")
        c+=1
    print("")
    
def printComplet(n):
    printPublics()
    printPrives(n+1)
    printProcess(n+1,"P","yes")
    printProcess(n+1,"Q","no")

printComplet(60)
