Otway Rees symmetric key
  A -> B : M,A,B,{1,Na,M,A,B}Kas
  B -> S : M,A,B,{1,Na,M,A,B}Kas,{2,Nb,M,A,B}Kbs
  S -> B : M, {3,Na,Kab}Kas, {4,Nb,Kab}Kbs
  B -> A : M,{3,Na,Kab}Kas
  
Secrecy of the key Kab (from the point of view of B)
encoded as a combination of key usability and “which key-concealing”
B -> : {m1}Kab vs {m2}K with K fresh and m1/m2 public constants

Scenario: simple
one session for each role, each played by an honest agent
________ Declaration of public constants _____ 
c1:tag1
c2:tag2
c3:tag3
c4:tag4
a:agent
b:agent
c:agent
mP:cst
mQ:cst
kcs:lgtkey
________ Declaration of private constants _______
kas:lgtkey
kbs:lgtkey
kab:sessionkey
kbc:sessionkey
kcb:sessionkey
kac:sessionkey
kca:sessionkey
kba:sessionkey
k:sessionkey
m1:sid
m2:sid
m3:sid
m4:sid
na1:nonce
na2:nonce
na3:nonce
na4:nonce
nb1:nonce
nb2:nonce
nb3:nonce
nb4:nonce
________ Description of process P ________________
% Role A played by a with b (and s)
out(0,pair(m1,pair(a,pair(b,senc(pair(c1,pair(na1,pair(m1,pair(a,b)))),kas))))).
in(0,pair(m1,senc(pair(c3,pair(na1,xkab:sessionkey)),kas)));

% Role A played by a with c (and s)
out(3,pair(m2,pair(a,pair(c,senc(pair(c1,pair(na2,pair(m2,pair(a,c)))),kas))))).
in(3,pair(m2,senc(pair(c3,pair(na2,xkab:sessionkey)),kas)));

% Role A played by b with a (and s)
out(7,pair(m3,pair(b,pair(a,senc(pair(c1,pair(na3,pair(m3,pair(b,a)))),kbs))))).
in(7,pair(m3,senc(pair(c3,pair(na3,xkab:sessionkey)),kas)));

% Role A played by b with c (and s)
out(11,pair(m4,pair(b,pair(c,senc(pair(c1,pair(na4,pair(m4,pair(b,c)))),kbs))))).
in(11,pair(m4,senc(pair(c3,pair(na4,xkab:sessionkey)),kcs)));


% Role S played by s with a and b
in(1,pair(ym:sid,pair(a,pair(b,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(a,b)))),kas),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(a,b)))),kbs)))))).
out(1,pair(ym,pair(senc(pair(c3,pair(yna,kab)),kas),senc(pair(c4,pair(ynb,kab)),kbs))));

% Role S played by s with a and c
in(4,pair(ym:sid,pair(a,pair(c,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(a,c)))),kas),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(a,c)))),kbs)))))).
out(4,pair(ym,pair(senc(pair(c3,pair(yna,kac)),kas),senc(pair(c4,pair(ynb,kac)),kcs))));

% Role S played by s with c and b
in(6,pair(ym:sid,pair(c,pair(b,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(c,b)))),kcs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(c,b)))),kbs)))))).
out(6,pair(ym,pair(senc(pair(c3,pair(yna,kbc)),kcs),senc(pair(c4,pair(ynb,kbc)),kbs))));

% Role S played by s with b and a
in(8,pair(ym:sid,pair(b,pair(a,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(b,a)))),kbs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(b,a)))),kas)))))).
out(8,pair(ym,pair(senc(pair(c3,pair(yna,kba)),kbs),senc(pair(c4,pair(ynb,kba)),kas))));

% Role S played by s with b and c
in(10,pair(ym:sid,pair(b,pair(c,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(b,c)))),kbs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(b,c)))),kcs)))))).
out(10,pair(ym,pair(senc(pair(c3,pair(yna,kcb)),kbs),senc(pair(c4,pair(ynb,kcb)),kcs))));

% Role S played by s with c and a
in(12,pair(ym:sid,pair(c,pair(a,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(c,a)))),kcs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(c,a)))),kas)))))).
out(12,pair(ym,pair(senc(pair(c3,pair(yna,kca)),kcs),senc(pair(c4,pair(ynb,kca)),kas))));


% Role B played by b with a (and s) + property
in(2,pair(zm:sid,pair(a,pair(b,za1:cfw)))).
out(2,pair(zm,pair(a,pair(b,pair(za1,senc(pair(c2,pair(nb1,pair(zm,pair(a,b)))),kbs)))))).
in(2,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb1,zkab:sessionkey)),kbs)))).
out(2,pair(zm,za2)).
out(2,senc(mP,zkab));

% Role B played by b with c (and s)
in(5,pair(zm:sid,pair(c,pair(b,za1:cfw)))).
out(5,pair(zm,pair(c,pair(b,pair(za1,senc(pair(c2,pair(nb2,pair(zm,pair(c,b)))),kbs)))))).
in(5,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb2,zkab:sessionkey)),kbs)))).
out(5,pair(zm,za2));

% Role B played by a with b (and s)
in(9,pair(zm:sid,pair(b,pair(a,za1:cfw)))).
out(9,pair(zm,pair(b,pair(a,pair(za1,senc(pair(c2,pair(nb3,pair(zm,pair(b,a)))),kas)))))).
in(9,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb3,zkab:sessionkey)),kas)))).
out(9,pair(zm,za2));

% Role B played by a with c (and s) 
in(13,pair(zm:sid,pair(c,pair(a,za1:cfw)))).
out(13,pair(zm,pair(c,pair(a,pair(za1,senc(pair(c2,pair(nb4,pair(zm,pair(c,a)))),kas)))))).
in(13,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb4,zkab:sessionkey)),kas)))).
out(13,pair(zm,za2));

________ Description of process Q ________________
% Role A played by a with b (and s)
out(0,pair(m1,pair(a,pair(b,senc(pair(c1,pair(na1,pair(m1,pair(a,b)))),kas))))).
in(0,pair(m1,senc(pair(c3,pair(na1,xkab:sessionkey)),kas)));

% Role A played by a with c (and s)
out(3,pair(m2,pair(a,pair(c,senc(pair(c1,pair(na2,pair(m2,pair(a,c)))),kas))))).
in(3,pair(m2,senc(pair(c3,pair(na2,xkab:sessionkey)),kas)));

% Role A played by b with a (and s)
out(7,pair(m3,pair(b,pair(a,senc(pair(c1,pair(na3,pair(m3,pair(b,a)))),kbs))))).
in(7,pair(m3,senc(pair(c3,pair(na3,xkab:sessionkey)),kas)));

% Role A played by b with c (and s)
out(11,pair(m4,pair(b,pair(c,senc(pair(c1,pair(na4,pair(m4,pair(b,c)))),kbs))))).
in(11,pair(m4,senc(pair(c3,pair(na4,xkab:sessionkey)),kcs)));



% Role S played by s with a and b
in(1,pair(ym:sid,pair(a,pair(b,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(a,b)))),kas),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(a,b)))),kbs)))))).
out(1,pair(ym,pair(senc(pair(c3,pair(yna,kab)),kas),senc(pair(c4,pair(ynb,kab)),kbs))));

% Role S played by s with a and c
in(4,pair(ym:sid,pair(a,pair(c,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(a,c)))),kas),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(a,c)))),kbs)))))).
out(4,pair(ym,pair(senc(pair(c3,pair(yna,kac)),kas),senc(pair(c4,pair(ynb,kac)),kcs))));

% Role S played by s with c and b
in(6,pair(ym:sid,pair(c,pair(b,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(c,b)))),kcs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(c,b)))),kbs)))))).
out(6,pair(ym,pair(senc(pair(c3,pair(yna,kbc)),kcs),senc(pair(c4,pair(ynb,kbc)),kbs))));

% Role S played by s with b and a
in(8,pair(ym:sid,pair(b,pair(a,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(b,a)))),kbs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(b,a)))),kas)))))).
out(8,pair(ym,pair(senc(pair(c3,pair(yna,kba)),kbs),senc(pair(c4,pair(ynb,kba)),kas))));

% Role S played by s with b and c
in(10,pair(ym:sid,pair(b,pair(c,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(b,c)))),kbs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(b,c)))),kcs)))))).
out(10,pair(ym,pair(senc(pair(c3,pair(yna,kcb)),kbs),senc(pair(c4,pair(ynb,kcb)),kcs))));

% Role S played by s with c and a
in(12,pair(ym:sid,pair(c,pair(a,pair(senc(pair(c1,pair(yna:nonce,pair(ym,pair(c,a)))),kcs),senc(pair(c2,pair(ynb:nonce,pair(ym,pair(c,a)))),kas)))))).
out(12,pair(ym,pair(senc(pair(c3,pair(yna,kca)),kcs),senc(pair(c4,pair(ynb,kca)),kas))));



% Role B played by b with a (and s) + property
in(2,pair(zm:sid,pair(a,pair(b,za1:cfw)))).
out(2,pair(zm,pair(a,pair(b,pair(za1,senc(pair(c2,pair(nb1,pair(zm,pair(a,b)))),kbs)))))).
in(2,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb1,zkab:sessionkey)),kbs)))).
out(2,pair(zm,za2)).
out(2,senc(mQ,k));

% Role B played by b with c (and s)
in(5,pair(zm:sid,pair(c,pair(b,za1:cfw)))).
out(5,pair(zm,pair(c,pair(b,pair(za1,senc(pair(c2,pair(nb2,pair(zm,pair(c,b)))),kbs)))))).
in(5,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb2,zkab:sessionkey)),kbs)))).
out(5,pair(zm,za2));

% Role B played by a with b (and s)
in(9,pair(zm:sid,pair(b,pair(a,za1:cfw)))).
out(9,pair(zm,pair(b,pair(a,pair(za1,senc(pair(c2,pair(nb3,pair(zm,pair(b,a)))),kas)))))).
in(9,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb3,zkab:sessionkey)),kas)))).
out(9,pair(zm,za2));

% Role B played by a with c (and s) 
in(13,pair(zm:sid,pair(c,pair(a,za1:cfw)))).
out(13,pair(zm,pair(c,pair(a,pair(za1,senc(pair(c2,pair(nb4,pair(zm,pair(c,a)))),kas)))))).
in(13,pair(zm,pair(za2:cfw,senc(pair(c4,pair(nb4,zkab:sessionkey)),kas)))).
out(13,pair(zm,za2));

