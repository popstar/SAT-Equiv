open Planning_graph ;;

let is_satisfiable ic = match (input_line ic).[0] with
        |'S' -> true
        |'U' -> false
        |_ -> raise (Invalid_argument "is_satisfiable in read_solution.ml")
;;
let clause_res ic = if is_satisfiable ic then input_line ic else " 0" ;;


let next_step c state = match (c,state) with
        (* State 0 : initial state - a space has been read
         * State 1 : final state
         * State 2 : '-' has been read
         * State 3 : A number is beeing read
         *)
        | ' ', 0 -> 0
        | '0', 0 -> 1
        | '-', 0 -> 2
        | '1', 0 -> 3
        | '2', 0 -> 3
        | '3', 0 -> 3
        | '4', 0 -> 3
        | '5', 0 -> 3
        | '6', 0 -> 3
        | '7', 0 -> 3
        | '8', 0 -> 3
        | '9', 0 -> 3
        | '1', 2 -> 3
        | '2', 2 -> 3
        | '3', 2 -> 3
        | '4', 2 -> 3
        | '5', 2 -> 3
        | '6', 2 -> 3
        | '7', 2 -> 3
        | '8', 2 -> 3
        | '9', 2 -> 3
        | ' ', 3 -> 0
        | '0', 3 -> 3
        | '1', 3 -> 3
        | '2', 3 -> 3
        | '3', 3 -> 3
        | '4', 3 -> 3
        | '5', 3 -> 3
        | '6', 3 -> 3
        | '7', 3 -> 3
        | '8', 3 -> 3
        | '9', 3 -> 3
        |  _ -> raise (Invalid_argument "next_step in read_solution.ml")
;;

let read_output ic  =
        try
        let str = clause_res ic in
        let rec aux numvar state pos listvars =
                let future_state = next_step (str.[pos]) (state) 
                in match (state,future_state) with
                |0,2 -> aux (numvar+1) 2 (pos+1) (listvars) (* we only care for variable that are true *)
                |0,3 -> aux (numvar+1) 3 (pos+1) (numvar::listvars)
                |0,1 -> listvars
                | _,n -> aux numvar n (pos+1) listvars
        in (aux 1 0 0 [])
        with
        | Invalid_argument s -> let new_m = String.concat " in " [s ; "read_output" ; "read_solution.ml" ] in raise (Invalid_argument new_m )
;; 

exception Bug of int*int * int ;;

let node_from_var max_step var =
  let node,step = (var/(max_step+1))-1,var mod (max_step+1)
  in match (var = Dimacs.dimacs_var max_step node step) with
  | true -> node,step
  | false -> raise (Bug (var,node,step))
;;

let rec node_list gph max_step = function
  | [] -> []
  | v::t -> let n,step = node_from_var max_step v in
            let act = get_activation gph n in
            if act > step 
            then node_list gph max_step t
            else 
              (get_node gph n,step)::(node_list gph max_step t)
;;

let print_node_list nodes =
    let rec aux = function
        | [] -> ()
        | (a,m)::t -> print_string "(" ; print_node a ; print_string "@" ; print_int m ; print_string ") \n" ; aux t
        in aux nodes
;;
