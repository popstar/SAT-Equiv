type mutex_date = 
  For_ever_since of int 
  | Until_end_since of int 
  | Between of int * int 
  | Never  
;;

let origin_mutex = function
  | For_ever_since n -> n
  | Until_end_since n -> n
  | Between (n,_) -> n
  | Never -> raise (Invalid_argument "origin_mutex in mutex.ml")
;;

let is_mutex_at date = function
  | For_ever_since _ -> true
  | Until_end_since _ -> true
  | Between (_,m) when m >= date -> true
  | _ -> false
;;

let cut_at (n:int) = function
  | Until_end_since m -> Between (m,n)
  | Between (m,p) when n<=p -> Between (m,n)
  | _ -> raise (Invalid_argument "cut_at in mutex.ml")
;;

let is_until_end = function
  | Until_end_since _ -> true
  | _ -> false
;;

let sup_eq x1 x2 =
  match x1,x2 with
  |For_ever_since _,_ -> true
  |_,For_ever_since _ -> false
  |Until_end_since _,_ -> true
  |_, Until_end_since _ -> false
  | Between (_,m1),Between (_,m2) -> (m1 >= m2)
  | Between (_,_),Never -> true 
  | Never,Between (_,_) -> false
  | Never,Never -> true
;;
        
let sup_str x1 x2 = (sup_eq x1 x2) && not (sup_eq x2 x1) ;;

let print_date_mutex = function
  |For_ever_since _ -> print_string "EVER" 
  |Until_end_since _ -> print_string "END"
  |Between (_,m) -> print_int m 
  |Never -> print_string "_" (* should not be used in the graph *)
;;

(* J'ajoute Mut pour protéger de l'utilisation non symétrique *)
type mutex = Mut of int * (mutex_date array array) ;;

(* The mutex relation is symmetric.
 * So it will be sufficient to define f i j when i > j. 
 *)

let symmetric mat i j =
  (* en principe i>j
   * Comme la matrice est triangulaire,
   * il n'y a rien dans la case 0
   * donc on l'enlève en mettant m-1 et n-1.*)
  match i,j with
  | n,m when n=m -> Never
  | n,m when n<m -> mat.(m-1).(n)
  | n,m -> mat.(n-1).(m)
;;

let mutex_result = function
  | (Mut (_,m)) -> symmetric m
;;

let print_mutex (m:mutex) bnd =
  let rec print_line k l =
    let dt = mutex_result m k l in
    match l with
    | 0 -> print_string " ]\n"
    | n when sup_str dt Never -> 
        print_string " (" ; 
        print_int (n-1) ; 
        print_string "@" ; 
        print_date_mutex dt ; 
        print_string ") " ; 
        print_line k (n-1)
    | n -> print_line k (n-1)
  in let rec aux = function
    | 0 -> ()
    | n -> 
        print_int (n-1) ; 
        print_string ": [" ; 
        print_line (n-1) bnd ; 
        aux (n-1)
  in print_string "\n" ; aux bnd
;;
        

exception Mutex_not_defined ;;

let make size = 
  let matrice = Array.make size [||] in
  let rec rempli = function
    | 0 -> ()
    | n -> 
        begin 
          matrice.(n-1) <- (Array.make n Never) ; 
          rempli (n-1) 
        end
  in rempli size ; Mut(size, matrice) 
;;


let bigger delta mut =
  let Mut(s,m) = mut in
  let Mut(k,nouvelle) = make (s+delta) in
  let rec copie = function
    | 0 -> ()
    | n ->
       begin
         nouvelle.(n-1) <- m.(n-1) ;
         copie (n-1) 
       end
  in copie s ; Mut(k, nouvelle)
;;

(* we assume that we add the node in the correct order, 
 * that is when we add node n it means that all node 0..(n-1) exist
 *)

let add_a_node (m:mutex) n =
  let Mut(size,_) = m in
  if (n>=size)
  then bigger 100 m
  else m
;;

let pair_order (a,b) =
  if a>=b
  then (a,b)
  else (b,a)
;;

let declare_mutex (m:mutex) a b (date:mutex_date) =
  let Mut(_,mat) = m in
  let a1,b1 = pair_order (a,b)
  in begin
    mat.(a1-1).(b1) <- date ; 
    m
  end
;; 

let stop_mutex (m:mutex) a b (date:int) =
  let Mut(_,mat) = m in
  let a1,b1 = pair_order (a,b) in
  begin 
    mat.(a1-1).(b1) <- cut_at date mat.(a1-1).(b1) ; 
    m
  end
;;

(* This function will be used to find if the preconditions of a rule
 * to be activated will be in mutex  
 * *)

let is_mutex_list (m:mutex) node_list date =
  let rec is_mutex_against_list a = function
    | [] -> false
    | h::_ when is_mutex_at date (mutex_result m a h) -> true
    | _::t -> is_mutex_against_list a t
  in let rec aux = function
    | []   -> false
    | h::t -> is_mutex_against_list h t || aux t
  in aux node_list
;;

(* This function will be used for the propagation of mutex to rules *)

let are_mutex_pre (m:mutex) pre1 pre2 (date:int) =
  let rec is_mutex_against_list a = function
    | [] -> false
    | h::_ when sup_eq (mutex_result m a h) (Between (0,date)) -> true
    | _::t -> is_mutex_against_list a t
  in let rec aux = function
    | [] -> false
    | h::t -> is_mutex_against_list h pre2 || aux t
  in aux pre1
;;

(* This function will be used for the propagation of mutex to facts *)

let are_mutex_add (m:mutex) add1 add2 (date:int) =
  let rec is_mutex_against_all_list a = function
    | [] -> true
    | h::t when sup_eq (mutex_result m a h) (Between (0,date)) -> 
        is_mutex_against_all_list a t 
    | _ -> false
  in let rec aux = function
    | [] -> true
    | h::t -> is_mutex_against_all_list h add2 && aux t
  in aux add1
;;


(* Useful for the leveled off *)

let number_of_mutex mut (date:int) bnd =
  let rec nb_mutex_line k = function
    | 0 -> 0
    | n when sup_eq (mutex_result mut k (n-1)) (Between (0,date)) -> 
        1 + nb_mutex_line k (n-1)
    | n -> nb_mutex_line k (n-1)
  in let rec total = function
    | 0 -> 0
    | n -> (nb_mutex_line n n) + ( total (n-1) )
  in total (bnd-1)
;;
