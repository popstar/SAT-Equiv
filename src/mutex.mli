type mutex_date =
    For_ever_since of int
  | Until_end_since of int
  | Between of int * int
  | Never

type mutex

exception Mutex_not_defined

val origin_mutex : mutex_date -> int
val is_mutex_at : int -> mutex_date -> bool
val is_until_end : mutex_date -> bool
val print_date_mutex : mutex_date -> unit
val mutex_result : mutex -> int -> int -> mutex_date
val print_mutex : mutex -> int -> unit
val make : int -> mutex
val add_a_node : mutex -> int -> mutex
val declare_mutex : mutex -> int -> int -> mutex_date -> mutex
val stop_mutex : mutex -> int -> int -> int -> mutex
val is_mutex_list : mutex -> int list -> int -> bool
val are_mutex_pre : mutex -> int list -> int list -> int -> bool
val are_mutex_add : mutex -> int list -> int list -> int -> bool
val number_of_mutex : mutex -> int -> int -> int
