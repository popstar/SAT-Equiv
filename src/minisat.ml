open Planning_graph;;
open Dimacs;;
open Read_solution;;

exception Sat of graph * int * int * int ;;

let appels = ref 0;;

let un_appel () =
  appels := !appels + 1
;;

let get_appels () = !appels ;;


let launch_minisat file_sat file_out =
  let instr = 
    String.concat " " ["minisat" ; file_sat ; file_out ; "> /dev/null" ;"2> /dev/null " ] in
  let _ = Unix.system instr in un_appel ()
;;


let verifie file_sat file_out gph1 our_facts step n =
  let oc = open_out file_sat in
  let nb_vars,nb_clauses = 
    explore_and_output oc gph1 step our_facts n in
  begin
    close_out oc ;
    launch_minisat file_sat file_out ;
    let ic = open_in file_out in
    match is_satisfiable ic with
    | true  -> close_in ic ; raise (Sat(gph1,step,nb_vars,nb_clauses))
    | false -> close_in ic ; gph1
  end
;;

