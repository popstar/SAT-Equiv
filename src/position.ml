(* On travaille d'abord sur les positions elles-mêmes *)

type position = int list;;

let rec is_smaller (l0:position) (l1:position) =
        match l0,l1 with
        | _ , [] -> false
        | [], _  -> true
        | a::t0,b::t1 -> (a < b && t0 = t1) || (is_smaller t0 t1)
;;

let rec insert_position pos = function
        | [] -> [pos]
        | a::t when is_smaller pos a -> pos::(a::t)
        | a::t -> a::(insert_position pos t)
;; 

let sort l =
        let rec help_sort res = function
                | [] -> res
                | (Some a)::t -> help_sort (insert_position a res) t
                | None::_ -> raise (Invalid_argument "in sort in position.ml" )
        in help_sort [] l
;;



(* Ensuite on considère des "positionneurs" get_pos 
 * ik -> Some(position) | None
 *)

type positionneur = ( Multiset.fact -> (position option)) ;;

let update (get_pos:positionneur) ik ik1 ik2 ik0 =
        match ik0,get_pos ik with
        | _,None -> raise (Invalid_argument  "update")
        | ik0,Some(p) when ik0 = ik1 -> Some(0::p)
        | ik0,Some(p) when Some(ik0) = ik2 -> Some(1::p)
        | _,Some(p) -> Some(p)
;;

(* Ensuite on travaille sur les listes de positions qui sont considérés dans chaque règle *)
