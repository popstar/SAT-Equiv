open Mutex ;;
open Planning_graph ;;

let output_int oc n = output_string oc (string_of_int n);;

let dimacs_var max_step node_number step =
  (max_step+1)*(node_number+1) + step
;;

let output_var oc n = output_string oc " " ; output_int oc n ;;

let clauses = ref 0;;
let end_clause oc =
  clauses := !clauses + 1 ;
  output_string oc " 0\n"
;;


let universal_axioms_pre oc gph max_step node step = 
  let pre = get_pre gph node in
  let var_node = dimacs_var max_step node step in
  let rec aux = function
     | [] -> ()
     | h::t when get_activation gph h <= step -> 
       begin 
       output_var oc (dimacs_var max_step h step) ; 
       output_var oc (- var_node) ; 
       end_clause oc ; 

       aux t 
       end
     | _::t -> aux t
  in aux pre
;;

let universal_axioms_add oc gph max_step node step = 
  let add = get_add gph node in
  let next_step = step+1 in
  let var_node = dimacs_var max_step node step in
  let rec aux = function
     | [] -> ()
     | h::t when get_activation gph h <= step -> 
       begin  
       output_var oc (dimacs_var max_step h next_step) ; 
       output_var oc (- var_node) ; 
       end_clause oc ; 
       aux t 
       end
     | _::t -> aux t
  in aux add
;;


let universal_axioms_del oc gph max_step node step = 
  let del = get_del gph node in
  let next_step = step+1 in
  let var_node = dimacs_var max_step node step in
  let rec aux = function
    | [] -> ()
    | h::t when get_activation gph h <= step -> 
      begin 
      output_var oc (-(dimacs_var max_step h next_step)) ; 
      output_var oc (- var_node) ; 
      end_clause oc ; 
      aux t 
      end
    | _::t -> aux t
  in aux del
;;

let explanatory_frame_axioms_add oc gph max_step node step =
  let add = get_add gph node in
  let next_step = step+1 in
  let var_node_now = dimacs_var max_step node step in
  let var_node_next = dimacs_var max_step node next_step in
  let rec aux = function
    | []   -> ()
    | h::t when get_activation gph h <= step -> 
       begin 
       output_var oc (dimacs_var max_step h step) ; 
       aux t 
       end
    | _::t -> aux t
  in aux add ; 
  output_var oc var_node_now ; 
  output_var oc (- var_node_next) ; 
  end_clause oc ; 
;;

let explanatory_frame_axioms_del oc gph max_step node step =
  let del = get_del gph node in
  let next_step = step+1 in
  let var_node_now = dimacs_var max_step node step in
  let var_node_next = dimacs_var max_step node next_step in
  let rec aux = function
     | []   -> begin output_var oc ( - var_node_now) ; output_var oc ( var_node_next) ; end_clause oc end
     | h::t when get_activation gph h <= step -> 
       begin 
       output_var oc (dimacs_var max_step h step) ; 
       aux t 
       end
     | _::t -> aux t
  in aux del 
;;

let supplementary_axiom oc gph max_step node step =
  let add0 = get_add gph node in
  let act = get_activation gph in
  let last_step = step-1 in
  let rec remove_unborn = function
     | [] -> []
     | a::t when (act a > last_step) -> remove_unborn t
     | a::t -> a::(remove_unborn t) 
  in let add = remove_unborn add0
  in let var_node = dimacs_var max_step node step
  in let rec aux = function
     | []   -> begin output_var oc ( - var_node ) ; end_clause oc end
     | h::t -> begin output_var oc (dimacs_var max_step h last_step) ; aux t end
  in aux add
;;

let mutex_axiom oc max_step node1 node2 step =
  let n1 = dimacs_var max_step node1 step in
  let n2 = dimacs_var max_step node2 step in
  output_var oc (-n1) ; output_var oc (-n2) ; end_clause oc
;;

let explore_and_output oc gph final_step initial_facts goal =
  let bnd = get_bound gph in
  let rec initialization = function
   | [] -> output_string oc "c FIN INITIALISATION\n"
   | f::t -> let k = dimacs_var final_step (get_fact_number gph f) 0 in 
     begin output_var oc k ; end_clause oc ; initialization t end 
  in let universal_axioms rho step = 
   begin 
     universal_axioms_pre oc gph final_step rho step ; 
     universal_axioms_add oc gph final_step rho step ;
     universal_axioms_del oc gph final_step rho step 
   end
  in let universal_from_rule rho =
   let act = get_activation gph rho in
   let rec aux = function
     | step when step = final_step -> 
       universal_axioms_pre oc gph final_step rho final_step ; 
       output_string oc "c FIN UNIVERSAL FROM RULE\n"
     (* no add and del for the final step *)
     | step -> universal_axioms rho step ; aux (step+1)
   in aux act

  in let explanatory_axioms f step =
    begin explanatory_frame_axioms_add oc gph final_step f step; 
    explanatory_frame_axioms_del oc gph final_step f step end 

  in let explanatory_from_fact f =
     let act = get_activation gph f in
     let rec aux = function
       | step when step = 0  -> 
           output_string oc "c FIN EXPLANATORY\n" 

       | step when step = act && act > 0 -> 
           supplementary_axiom oc gph final_step f step
       
       | step -> explanatory_axioms f (step-1) ; aux (step-1)

     in aux final_step

  in let rec main  = function
     
     | 0 -> ()
     
     | n when get_sort (get_node gph (n-1)) = F -> 
         (explanatory_from_fact (n-1)) ; main (n-1)
     
     | n when get_sort (get_node gph (n-1)) = R -> 
         (universal_from_rule (n-1))   ; main (n-1)
     
     | _ -> 
         raise 
         (Invalid_argument "function explore_and_output in dimacs.ml")

  in let mut = get_mutex gph
  in let dimacs_mutex step =
     let rec line k = function
       | 0 -> ()
       | n when (let mut_k_n = mutex_result mut k (n-1) in ( is_mutex_at step mut_k_n) && ( origin_mutex mut_k_n <= step) ) -> 
         begin mutex_axiom oc final_step k (n-1) step ; line k (n-1) end
       | n -> line k (n-1)
     in let rec total = function
       | 0 -> ()
       | n -> begin line n n ; total (n-1) end
     in total (bnd-1)
  in let rec total_mutex = function
     | 0 -> output_string oc "c FIN MUTEX\n"
     | n -> dimacs_mutex (n-1) ; total_mutex (n-1)
  in let add_goal () =
     let act_goal = get_activation gph goal in
     let rec since_reachable = function
       | step when step = final_step+1 -> ()
       | step -> let var = dimacs_var final_step goal step in 
       begin output_var oc var ; output_string oc " " ; since_reachable (step+1) end
     in output_string oc "c AJOUT DU BUT :\n" ; since_reachable act_goal ; end_clause oc

  in begin 
    initialization initial_facts ; 
    total_mutex (final_step+1) ; 
    main bnd ; 
    add_goal () ; 
    (final_step*bnd),!clauses (* nb_vars,nb_clauses *)  
   end
;;

