open Terms;;
open Process;;


let rec all_types_process res = function
  | Null -> res
  | Out(_,t,p) -> all_types_process ((get_type t)::res) p
  | In(_,_,p)  -> all_types_process res p
;;

let all_types_biprotocol pq =
  let rec aux res = function
    | [] -> res
    | (p,_)::l -> (aux (all_types_process res p) l)
  in aux [] pq
;;  

let rec height_type = function
  | (At(_):complete_type) -> 0
  | Omega -> 0
  | Senc(a,b) -> 1 + max (height_type a) (height_type b)
  | Pair(a,b) -> 1 + max (height_type a) (height_type b)
  | Aenc(a,b) -> 1 + max (height_type a) (height_type b)
  | Sign(a,b) -> 1 + max (height_type a) (height_type b)
  | Hash(_) -> 0 (* inutile de descendre sous le hash *)
  | Vk(a)  -> 1 + height_type a
  | Pub(a) -> 1 + height_type a
;;

let rec max_height_type res = function
  | [] -> res
  | t::l -> max_height_type (max (height_type t) res) l
;;

let rec insert b = function
  | [] -> [b]
  | a::t when a = b -> a::t
  | a::t -> a::(insert b t)
;;

let rec key_position_types res = function
  | (At(_):complete_type) -> res
  | Omega -> res
  | Senc(a,b) -> key_position_types (insert b res) a
  | Aenc(a,Pub(b)) -> key_position_types (insert b res) a
  | Aenc(a,_) -> key_position_types res a
  | Pair(a,b) -> key_position_types (key_position_types res b) a
  | Sign(a,b) -> key_position_types (insert b res) a
  | Hash(_) -> res (* inutile de descendre sous le hash *)
  | Pub(a) -> insert a res
  | Vk(a)  -> insert a res
;;

let rec key_position_types_list res = function
  | [] -> res
  | a::t -> key_position_types_list (key_position_types res a) t
;;

let rec number_of_possible_keys key_positions = function
  | [] -> 0
  | a::t when List.mem (get_type a) key_positions ->
     1 + number_of_possible_keys key_positions t
  | _::t -> number_of_possible_keys key_positions t
;;  


let borne pq private_constants =
  let (n_out,n_in) = max_interleaving_length pq
  in let types = all_types_biprotocol pq
  in let h = max_height_type 0 types
  in let key_positions = key_position_types_list [] types
  in let k = number_of_possible_keys  key_positions private_constants
  in n_out*(k+1)*h + n_in + n_out
