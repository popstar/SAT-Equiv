open Atoms ;;

(* Constructor Terms *)

type 'a term = 
        | At of 'a 
        | Omega 
        (* Omega est la constante <w,w> qui fait la différence entre typé et pseudo-typé. *)
        | Senc  of 'a term * 'a term 
        | Pair  of 'a term * 'a term 
        | Aenc  of 'a term * 'a term
        | Hash  of 'a term
        | Pub   of 'a term
        | Sign  of 'a term * 'a term
        | Vk of 'a term
;;

(* Quick atoms constructors : *)

let x n   =  At(Var(n)) ;;
let c n   =  At(Cst(n)) ;;
let nce n =  At(Nonce(n)) ;;

(* Print function *)

let to_string_generic at_to_string t =
        let rec main = function
        | At(x) -> at_to_string x
        | Omega -> "<w,w>"
        | Senc(a,b) -> String.concat "" ["senc(" ; main a ; "," ; main b ; ")" ]
        | Pair(a,b) -> Printf.sprintf "<%s,%s>"     (main a) (main b) 
        | Aenc(a,b) -> Printf.sprintf "aenc(%s,%s)" (main a) (main b) 
        | Hash(a)   -> Printf.sprintf "hash(%s)" (main a) 
        | Pub(a)    -> Printf.sprintf "pub(%s)" (main a)
        | Sign(a,b) -> Printf.sprintf "sign(%s,%s)" (main a) (main b) 
        | Vk(a)     -> Printf.sprintf "vk(%s)" (main a)

        in main t
;;

let print_term t = print_string (to_string_generic Atoms.to_string t) ;;

(* Generic functions on terms *)

let is_ground m =
  let rec acc = function
    | a::l -> aux l a
    | [] -> true
  and aux l = function
    | At(x) -> is_ground_atom x
    | Omega -> acc l
    | Pub(x) -> aux l x
    | Vk(x)  -> aux l x
    | Pair(a,b) -> acc (a::b::l)
    | Senc(a,b) -> acc (a::b::l)
    | Aenc(a,b) -> acc (a::b::l)
    | Sign(a,b) -> acc (a::b::l)
    | Hash(x) -> aux l x
  in aux [] m
;;


(* Boolean functions on terms *)

let is_atomic = function
  | At(_) -> true
  | _ -> false
;;

let has_atomic_keys t =
  let rec acc = function
    | a::l -> aux l a
    | [] -> true 
  and aux l = function
    |At(_) -> acc l
    |Omega -> acc l
    |Pub(_) -> acc l
    |Vk(_) -> acc l
    |Hash(x) -> aux l x
    |Pair(a,b) -> acc (a::b::l)
    |Senc(a,At(_)) -> aux l a
    |Aenc(a,Pub(At(_))) -> aux l a
    |Sign(a,At(_)) -> aux l a 
    |_ -> false
  in aux [] t
;;  

(* Types *)

type atom_type = Atomic_type of string | Cameleon ;;

type complete_type = atom_type term ;; 
(* Type Message is only used for analyze rules which may be untyped *)

let type_checks t1 t2 =
  let rec acc = function
    | a::l -> aux l a
    | [] -> true
  and aux l = function
    | At(Cameleon), _  -> acc l
    | _ , At(Cameleon) -> acc l
    | At(x),At(y)   -> (x = y)&&(acc l)
    | Pub(x),Pub(y) -> aux l (x,y)
    | Vk(x), Vk(y)  -> aux l (x,y)
    | Hash(x), Hash(y) -> aux l (x,y)
    | Senc(a,b),Senc(c,d) -> acc ((a,c)::(b,d)::l)
    | Aenc(a,b),Aenc(c,d) -> acc ((a,c)::(b,d)::l)
    | Sign(a,b),Sign(c,d) -> acc ((a,c)::(b,d)::l)
    | Pair(a,b),Pair(c,d) -> acc ((a,c)::(b,d)::l)
    | _ -> false
  in aux [] (t1,t2)
;;

(* Atoms, and in particular variables, may have a non atomic type : *)

type typed_term = (atom*complete_type) term ;;

let rec untype = function
  | At(x,_) -> At(x)
  | Omega -> Omega
  | Pub(a) -> Pub(untype a)
  | Vk(a)  -> Vk (untype a)
  | Hash(a)  -> Hash (untype a)
  | Sign(a,b) -> Sign(untype a,untype b)
  | Aenc(a,b) -> Aenc(untype a,untype b)
  | Senc(a,b) -> Senc(untype a,untype b)
  | Pair(a,b) -> Pair(untype a,untype b)
;;

let rec get_type = function
  | At(_,t) -> t
  | Omega   -> At(Cameleon) (* Il n'y a pas de contrôle de type sur Omega *)
  | Pub(a) -> Pub(get_type a)
  | Vk(a)  -> Vk (get_type a)
  | Hash(a)  -> Hash (get_type a)
  | Sign(a,b) -> Sign(get_type a, get_type b)
  | Aenc(a,b) -> Aenc(get_type a, get_type b)
  | Senc(a,b) -> Senc(get_type a, get_type b)
  | Pair(a,b) -> Pair(get_type a, get_type b)
;;

let type_matches t1 t2 = type_checks (get_type t1) (get_type t2) ;;

(* Quick terms constructors *)

let xt n str   =  At(Var(n),At(Atomic_type(str))) ;;
let yt n tp    =  At(Var(n),tp) ;;
let ct n str   =  At(Cst(n),At(Atomic_type(str))) ;;
let ncet n str =  At(Nonce(n),At(Atomic_type(str))) ;;
let nce_from_tp n tp =  At(Nonce(n),tp) ;;
let pub n str = Pub(At(Var(n),At(Atomic_type(str))));;
let vk  n str = Vk( At(Var(n),At(Atomic_type(str))));;

(* Print functions for types, terms of types and typed terms *)

let atom_type_to_string = function
  | Atomic_type t -> String.concat "" ["'t_" ; t ] 
  | Cameleon -> "cameleon"
;;

let term_of_type_to_string (t0:atom_type term) = to_string_generic atom_type_to_string t0 ;;

let complete_type_to_string t = term_of_type_to_string t ;;


let typed_term_to_string (t0:typed_term) = to_string_generic (fun (x,t) -> String.concat " : " [Atoms.to_string x ; complete_type_to_string t ])  t0 ;;
let print_typed_term t0 = print_string (typed_term_to_string t0);;

let without_type_to_string (t0:typed_term) = to_string_generic (fun (x,_) -> Atoms.to_string x) t0 ;;
let print_term_without_type t0 = print_string (without_type_to_string t0) ;;
