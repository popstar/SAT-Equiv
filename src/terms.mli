type 'a term = 
        | At of 'a 
        | Omega 
        | Senc  of 'a term * 'a term 
        | Pair  of 'a term * 'a term 
        | Aenc  of 'a term * 'a term
        | Hash  of 'a term
        | Pub   of 'a term
        | Sign  of 'a term * 'a term
        | Vk of 'a term


val x : int -> Atoms.atom term
val c : int -> Atoms.atom term
val nce : int -> Atoms.atom term
val print_term : Atoms.atom term -> unit
val is_ground : Atoms.atom term -> bool
val is_atomic : 'a term -> bool
val has_atomic_keys : 'a term -> bool

type atom_type = Atomic_type of string | Cameleon
type complete_type = atom_type  term

val type_checks : complete_type -> complete_type -> bool

type typed_term = (Atoms.atom * complete_type ) term

val untype : ('a * 'b) term -> 'a term
val get_type : ('a * complete_type) term -> complete_type
val type_matches :
  ('a * complete_type) term -> ('b * complete_type) term -> bool
val xt : int -> string -> typed_term
val yt : int -> 'a -> (Atoms.atom * 'a) term
val ct : int -> string -> typed_term
val ncet : int -> string -> typed_term
val nce_from_tp : int -> 'a -> (Atoms.atom * 'a) term
val pub : int -> string -> typed_term
val vk  : int -> string -> typed_term
val print_typed_term : typed_term -> unit
val print_term_without_type : typed_term -> unit
