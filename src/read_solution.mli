val is_satisfiable : in_channel -> bool
val read_output : in_channel -> int list
val node_list :
  Planning_graph.graph ->
  int -> int list -> (Planning_graph.node * int) list
val print_node_list : (Planning_graph.node * int) list -> unit
