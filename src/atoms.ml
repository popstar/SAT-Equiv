(* Atoms *)

type atom = Cst of int | Var of int | Nonce of int ;;

let to_string = function
        | Cst(n)   -> String.concat "" ["C(" ; string_of_int n ; ")"]
        | Var(n)   -> String.concat "" ["V(" ; string_of_int n ; ")"]
        | Nonce(n) -> String.concat "" ["N(" ; string_of_int n ; ")"]

let print_atom a = print_string (to_string a);;

let print_atom_list l =
        let rec main = function
        | [] -> ()
        | a::t -> begin print_atom a ; print_string ";" ; main t end
  
        in print_string "[" ; main l ; print_string "]"
;;

let is_ground_atom = function
        | Var(_) -> false
        | _ -> true
;;

let is_variable v = not (is_ground_atom v);;
let get_var_num = function
        | Var(n) -> n
        | _ -> raise (Invalid_argument "in get_var_num in atoms.ml")
;;
