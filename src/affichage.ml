open Read_plan;;
open Read_solution;;
let no_attack = "No attack has been found.\n";;



let print_stats bound nb_noeuds step = 
  begin
    print_string "\n Number of nodes: ";
    print_int nb_noeuds ;
    print_string "\n Number of steps: ";
    print_int step ;
    (*
    print_string "/";
    print_int (bound+2) ;
    *)
    print_string "\n MiniSAT calls: ";
    print_int (Minisat.get_appels ());
    print_string "\n\n"  
  end
;;

let print_stats_sat nb_vars nb_clauses =
  begin
    print_string "\n Number of variables (approx.): ";
    print_int nb_vars ;
    print_string "\n Number of clauses: " ;
    print_int nb_clauses ;
    print_string "\n" ;
  end
;;


let affiche file_out gph step rules =
  begin
  let ic = open_in file_out in
    print_lisible (node_list gph step (read_output ic)) rules;
  close_in ic 
  end
;;

