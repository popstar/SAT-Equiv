val dimacs_var :
  int -> int -> int -> int
val explore_and_output :
  out_channel ->
  Planning_graph.graph ->
  int -> Multiset.fact list -> int -> int * int
