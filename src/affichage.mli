
val no_attack : string

val print_stats : int -> int -> int -> unit

val print_stats_sat : int -> int -> unit

val affiche : string -> Planning_graph.graph -> int -> 
  Multiset.rule list -> unit
