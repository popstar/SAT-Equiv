type atom = Cst of int | Var of int | Nonce of int
val to_string : atom -> string
val print_atom : atom -> unit
val print_atom_list : atom list -> unit
val is_ground_atom : atom -> bool
val is_variable : atom -> bool
val get_var_num : atom -> int
