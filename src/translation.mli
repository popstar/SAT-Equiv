val get_all_rules :
  bool ->
  (Process.typed_process * Atoms.atom Process.process) list ->
   Multiset.fact list -> int -> Multiset.fact list * Multiset.rule list
