exception Sat of Planning_graph.graph * int * int * int ;;

val get_appels : unit -> int

val launch_minisat :
  string -> string -> unit

val verifie :
  string -> string -> Planning_graph.graph ->
   Multiset.fact list -> int -> int ->
   Planning_graph.graph
