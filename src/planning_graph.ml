open Multiset ;;
open Mutex ;;

type node = Rule of rule_label | Fact of fact | Undefined ;;
type node_sort = R | F | U ;; 
let get_sort = function
        | Rule(_) -> R
        | Fact(_) -> F
        | Undefined -> U
;;

exception Undef of string*string ;;


  (* type graph is as follows:
   -     (maximal node number)
   -   * (fact -> node number)
   -   * (rule_label -> node number) 
   -   * (node number -> node * PRE * ADD * DEL * ACTIVATION DATE )
   -   * ( mutex table )
   - Note that the informations PRE, ADD and DEL 
   will be the PRE, ADD and DEL of the rules
   - or the rule that have the fact in the PRE, ADD and DEL. 
   *)

type graph = int * (fact -> int) * (rule_label -> int) * 
             (node * (int list) * (int list) * (int list) * int) array
            * mutex
;; 

 (* getters *)

let get_bound = fun ((x,_,_,_,_):graph) -> x ;;
let get_fact_number = ( fun ((_,x,_,_,_):graph) -> (fun f -> x f ));;
let is_defined_fact gph f = try let _ = get_fact_number gph f in true with
                |Undef ("F",_) -> false
;;
let get_rule_number = ( fun ((_,_,x,_,_):graph) -> (fun r -> x r ));;


let get_array = ( fun ((_,_,_,x,_):graph) -> x);;
let get_tuple gph n = 
  let arr = get_array gph
  in arr.(n)
;;

let get_mutex = ( fun ((_,_,_,_,x):graph) -> x);;

let get_node gph n = let (f,_,_,_,_) = get_tuple gph n in f ;;
let get_pre  gph n = let (_,p,_,_,_) = get_tuple gph n in p ;;
let get_add  gph n = let (_,_,a,_,_) = get_tuple gph n in a ;;
let get_del  gph n = let (_,_,_,d,_) = get_tuple gph n in d ;;

let get_activation gph n = 
  let (_,_,_,_,ad) = get_tuple gph n in ad ;;

let get_fact_list gph =
  let bnd = get_bound gph in 
  let is_fact n = get_sort (get_node gph n) = F in
  let node_to_fact = function
    |Fact(f) -> f
    | _ -> 
      raise 
      (Invalid_argument 
      "function node_to_fact in get_fact_list of planning_graph.ml")
   (* We ensure that the second branch is never used 
    * by testing if the node is a fact *) 
  in let rec aux lst = function
    | 0 -> lst 
    | n when is_fact (n-1) -> 
        aux ((node_to_fact (get_node gph (n-1)))::lst) (n-1)
    | n -> aux lst (n-1)
  in aux [] bnd
;; 

let get_rule_list gph =
  let bnd = get_bound gph in 
  let is_rule n = get_sort (get_node gph n) = R in
  let node_to_rule = function
    |Rule(lab) -> lab
    | _ -> 
      raise 
      (Invalid_argument 
      "function node_to_rule in get_rule_list of planning_graph.ml")
  (* We ensure that the second branch is never used 
   * by testing if the node is a rule *) 
  in let rec aux lst = function
    | 0 -> lst 
    | n when is_rule (n-1) -> 
        aux ((node_to_rule (get_node gph (n-1)))::lst) (n-1)
    | n -> aux lst (n-1)
        in aux [] bnd
;; 
       
                 

 (* setters *)
        (* general setters *)

let set_bound (gph:graph) n = let (_,b,c,d,e) = gph in (n,b,c,d,e) ;;
let set_fact_number (gph:graph) nnf = 
  let (a,_,c,d,e) = gph in (a,nnf,c,d,e) ;;
let set_rule_number (gph:graph) rf = 
  let (a,b,_,d,e) = gph in (a,b,rf,d,e) ;;
let set_array (gph:graph) arr = 
  let (a,b,c,_,e) = gph in (a,b,c,arr,e) ;;

let set_pre gph n new_pre = 
  let arr = get_array gph in
  let (nd,_,add,del,act_date) = arr.(n) in
  arr.(n) <- (nd,new_pre,add,del,act_date)
;;

let set_add gph n new_add = 
  let arr = get_array gph in
  let (nd,pre,_,del,act_date) = arr.(n) in
  arr.(n) <- (nd,pre,new_add,del,act_date)
;;

let set_del gph n new_del = 
  let arr = get_array gph in
  let (nd,pre,add,_,act_date) = arr.(n) in
  arr.(n) <- (nd,pre,add,new_del,act_date)
;;

let set_activation gph n new_act_date = 
  let arr = get_array gph in
  let (nd,pre,add,del,_) = arr.(n) in
  arr.(n) <- (nd,pre,add,del,new_act_date)
;;

let set_mutex (gph:graph) (m:mutex) = 
  let (a,b,c,d,_) = gph in a,b,c,d,m ;;

       (* Print functions : useful for tests *)

let print_facts gph =
  let fl = get_fact_list gph
  in print_fact_list fl
;;

let print_rules gph =
  let rl = get_rule_list gph
  in print_labels_list rl
;;

let print_node = function
  | Rule(r) -> 
    print_string "Label :" ; print_rule_label r ; print_string " "

  | Fact(f) -> 
    print_string "Fact :" ; print_fact f ; print_string " "

  | Undefined -> 
    print_string "Undefined. "


let print_graph gph =
  let bnd= get_bound gph in 
  let rec nodes_list l = function
    | 0 -> l
    | n -> nodes_list ((get_node gph (n-1) )::l) (n-1)
  in let list_of_nodes = nodes_list [] bnd
  in let is_fact node = (get_sort node = F)
  in let get_fact = function
    | Fact(f) -> f
    | _ -> 
      raise 
      (Invalid_argument 
      "function get_fact in print_graph of planning_graph.ml")
  in let is_rule node = (get_sort node = R)
  in let get_rule = function
    | Rule(r) -> r
    | _ -> 
      raise 
      (Invalid_argument 
      "function get_rule in print_graph of planning_graph.ml")
  in let print_fact_number l =
    let rec aux = function
    | a::t when (is_fact a) -> 
        let fct = get_fact a in 
        begin 
        print_string "  "   ;
        print_fact fct      ;
        print_string " -> " ;
        print_int (get_fact_number gph fct) ;
        print_string "\n" ;
        aux t
        end
    | _::t -> aux t
    | [] -> ()
    in print_string "# FACT NUMBER FUNCTION :\n" ; aux l

  in let print_rule_number l =
    let rec aux = function
    | a::t when (is_rule a) -> 
      let lab = get_rule a in 
      begin 
      print_string "  "   ;
      print_rule_label lab     ;
      print_string " -> " ;
      print_int (get_rule_number gph lab) ;
      print_string "\n" ;
      aux t
      end
    | _::t -> aux t
    | [] -> ()
    in print_string "# RULE NUMBER FUNCTION :\n" ; aux l

    in let print_node = function
    | Rule(r) -> 
        print_string "Label :" ; print_rule_label r ; print_string " "
    | Fact(f) -> 
        print_string "Fact :" ; print_fact f ; print_string " "
    | Undefined -> print_string "Undefined. "

    in let print_int_list l =
      let rec aux = function
      | [] -> print_string "]"
      | [a] -> print_int a ; print_string "]"
      | a::t -> print_int a ; print_string ";" ; aux t
      in print_string "[" ; aux l

    in let print_complete_node n =
      let (node,pre,add,del,date) = get_tuple gph n in
      begin
      print_string "\\* Node " ; 
      print_int n ; 
      print_string ":\n" ;
      print_node node ;
      print_string "\n# PRE: \n" ;
      print_int_list pre ;
      print_string "\n# ADD: \n" ;
      print_int_list add ;
      print_string "\n# DEL: \n" ;
      print_int_list del ;
      print_string "\n# date : " ; print_int date ; print_string "\n" ;
      print_string "\n\n\n"
      end
    in let rec print_all_nodes = function
      | 0 -> ()
      | n -> 
      begin 
      print_complete_node (n-1) ;
      print_all_nodes (n-1) 
      end
    in
    begin
      print_string "\\* GRAPH :\n";
      print_string ">> BOUND :";
      print_int bnd ;
      print_string "\n";
      print_fact_number list_of_nodes ; 
      print_string "\n";
      print_rule_number list_of_nodes ; 
      print_string "\n>> Nodes enumeration : \n";
      print_all_nodes bnd ;
      print_string "\n>> Mutex table : \n";
      print_mutex (get_mutex gph) bnd
    end
;;

(* We want functions to modify the graph 
 * without going into the details of each function *)

        (* Basics *)
let increase_bound gph n =
  let bnd = get_bound gph in
  set_bound gph (bnd+n)
;;

let add_fact_at_n gph n fct =
  let fn = get_fact_number gph in
  let new_fn = function
    | f when f = fct -> n
    | f -> fn f
  in set_fact_number gph new_fn
;;

let add_rule_at_n gph n (r:rule_label) =
  let rn = get_rule_number gph in
  let new_rn = function
    | rr when rr = r -> n
    | rr -> rn rr
  in set_rule_number gph new_rn
;; 

let add_tuple gph n tuple =
  let arr = get_array gph in
  let rec copie nouv = function
    | 0 -> ()
    | k ->
        begin
          nouv.(k-1) <- arr.(k-1) ;
          copie nouv (k-1)
        end
  in let default = (Undefined,[],[],[],0)
  in match Array.length arr with
    | l when l <= n -> 
        let nouv = Array.make (2*l) default in
        begin
          copie nouv l ;
          nouv.(n) <- tuple ;
          set_array gph nouv
        end
    | _ ->
        begin
          arr.(n) <- tuple ;
          gph
        end
;;

let add_pre_element gph n elmt =
  let new_pre = elmt :: (get_pre gph n) in
  set_pre gph n new_pre
;;

let add_add_element gph n elmt =
  let new_add = elmt :: (get_add gph n) in
  set_add gph n new_add
;;

let add_add_element_sym gph n m =
  begin
  add_add_element gph m n ;
  add_add_element gph n m
  end
;;

let add_del_element gph n elmt =
  let new_del = elmt :: (get_del gph n) in
  set_del gph n new_del
;;

let add_del_element_sym gph n m =
  begin
  add_del_element gph m n ;
  add_del_element gph n m 
  end
;;

let add_definitive_mutex gph n m =
  let mut = get_mutex gph in
  let act_n = get_activation gph n in
  let act_m = get_activation gph m in
  let origin = max act_n act_m in
  let new_mut = declare_mutex mut n m (For_ever_since origin) in
  set_mutex gph new_mut
;;

let add_mutex gph n m =
  let mut = get_mutex gph in
  let act_n = get_activation gph n in
  let act_m = get_activation gph m in
  let origin = max act_n act_m in
  let new_mut = declare_mutex mut n m (Until_end_since origin)
  in set_mutex gph new_mut
;;


let end_mutex_sym gph n m date =
  let mut = get_mutex gph in
  let new_mut = stop_mutex mut n m date 
  in set_mutex gph new_mut
;;

        (* More complete *)

let add_fact_to_graph gph fct =
  let n = get_bound gph in
  let gph0 = increase_bound gph 1 in
  let mut = get_mutex gph in
  let new_mut = add_a_node mut n in
  let gph1 = add_fact_at_n gph0 n fct
  in set_mutex (add_tuple gph1 n (Fact(fct),[],[],[],-1)) new_mut
;;

let add_rule_to_graph gph label =
  let n = get_bound gph in
  let gph0 = increase_bound gph 1 in
  let mut = get_mutex gph in
  let new_mut = add_a_node mut n in
  let gph1 = add_rule_at_n gph0 n label
  in set_mutex (add_tuple gph1 n (Rule(label),[],[],[],-1)) new_mut
;;

let add_initial_fact gph fct =
  let gph1 = add_fact_to_graph gph fct in
  let fct_nb = get_fact_number gph1 fct in
  begin
    set_activation gph1 fct_nb 0 ;
    gph1
  end
;;

  (* Functions to find if two nodes are in mutex and when *)

let is_mutex_fact_array gph fact_array (date:int) =
  let node_list = 
    Array.to_list (Array.map (get_fact_number gph) fact_array) 
  in is_mutex_list (get_mutex gph) node_list date
;;

        (* Functions to handle mutex creation and propagation *)

let must_mutex_facts gph date n m =
  let add1 = get_add gph n in
  let add2 = get_add gph m in
  let mut  = get_mutex gph in
  are_mutex_add mut add1 add2 date
;;

let must_mutex_rules gph date n m =
  let pre1 = get_pre gph n in
  let pre2 = get_pre gph m in
  let mut = get_mutex gph in
  let date_inf = (date-1) in
  are_mutex_pre mut pre1 pre2 date_inf
;;

let create_definitive_mutex gph r =
  try
    let rn = get_rule_number gph r in
    let pre = get_pre gph rn in
    let add = get_add gph rn in
    let del = get_del gph rn in
    let dels_of_pre = List.map (get_del gph) pre in
    let pres_of_del = List.map (get_pre gph) del in
    let dels_of_add = List.map (get_del gph) add in
    let adds_of_del = List.map (get_add gph) del in
    let rec put_distinct a = function
      | l0 when a=rn -> 
          l0 (* to avoid that r is in mutex with itself *)
      | [] -> [a]
      | b::t when a=b -> b::t
      | b::t -> b::(put_distinct a t)
    in let rec append_distinct l0 = function
      | []   -> l0
      | a::t -> append_distinct (put_distinct a l0) t
    in let rec flatten_distinct l0 = function
      | [] -> l0
      | hl::ll -> flatten_distinct (append_distinct l0 hl) ll
    in let l0 = 
      flatten_distinct (flatten_distinct [] dels_of_pre) pres_of_del
    in let future_mutex = 
      flatten_distinct (flatten_distinct l0 dels_of_add ) adds_of_del
    in let rec define_all_mutex gph0 = function
      | []   -> gph0
      | a::t -> define_all_mutex (add_definitive_mutex gph0 a rn) t
    in define_all_mutex gph future_mutex

  with
  |Undef(x,_) -> 
      raise (Undef (x,"create_definitive_mutex in planning_graph.ml"))
;;

  (* The following functions are used to create new nodes
   * and to make links with existing nodes *)

let add_label_from_pre gph label pre date =
  try
    let gph0 = add_rule_to_graph gph label in
    let r_nb = get_rule_number gph0 label in
    let pre_list_with_repetition = Array.to_list pre in
    let rec simplify_list l0 = function
      | [] -> l0
      | a::t when List.mem a t -> simplify_list l0 t
      | a::t -> simplify_list (a::l0) t 
    in let pre_list = simplify_list [] pre_list_with_repetition
    (* As we have the last line, 
     * we do not need to use a symmetric version of add_pre_element 
     * in complete_pre_facts *)
    in let rec complete_pre_facts gph0 = function
      | []   -> gph0
      | n::t ->
          begin
            add_pre_element gph0 n r_nb ;
            complete_pre_facts gph0 t
          end
    in begin
      set_activation gph0 r_nb date ;
      set_pre gph0 r_nb pre_list ;
      complete_pre_facts gph0 pre_list 
    end

  with
  |Undef(x,_) -> 
      raise (Undef (x,"add_label_from_pre in planning_graph.ml") )
;;

let add_fact_from_add gph label add =
  try
    let r_nb = get_rule_number gph label in
    let date = get_activation gph r_nb in
    let on_defined (gph0,new_facts) fact = 
      begin
        add_add_element_sym gph0 (get_fact_number gph0 fact) r_nb;
        gph0,new_facts
      end
    in let on_undefined (gph0,new_facts) fact = 
      let gph1 = add_fact_to_graph gph0 fact in
      let fact_nb = get_fact_number gph1 fact in
      begin
        set_activation gph1 fact_nb (date+1) ;
        add_add_element_sym gph1 fact_nb r_nb;
        (gph1, fact::new_facts)
      end

    in let on_each_fact (gph0,l0) = function
      | fact when is_defined_fact gph0 fact -> 
          on_defined (gph0,l0) fact
      | fact -> on_undefined (gph0,l0) fact
    in Array.fold_left on_each_fact (gph,[]) add

  with
  |Undef(x,_) -> 
      raise (Undef (x,"add_fact_from_add in planning_graph.ml") )
;;

let add_fact_from_del gph label del =
  try 
    let r_nb = get_rule_number gph label in
    (* We have the property that rules can only delete facts 
     * that are in their pre. 
     * So it is not possible that the deleted fact 
     * is not already in the graph *)

    let on_each_fact gph0 fact = 
      add_del_element_sym gph0 (get_fact_number gph0 fact) r_nb
    in Array.iter (on_each_fact gph) del

  with
  |Undef(x,_) -> 
    raise (Undef (x,"add_fact_from_del in planning_graph.ml") )
;;

  (* a natural graph initialization *)
let make () =
  let initial_bound = 0 in
  let initial_rule_function _ = 
    raise (Undef ("R","make in planning_graph.ml")) in
  let initial_fact_function _ = 
    raise (Undef ("F","make in planning_graph.ml")) in
  let default = (Undefined,[],[],[],0) in
  let initial_array = Array.make 100 default
  in ((initial_bound,initial_fact_function,initial_rule_function,
  initial_array, Mutex.make 100):graph)
;;

 (* The following function builds the nodes for the initial state *)
let build_initial_graph initial_facts =
  let gph = make () in
  let rec on_facts gph0 = function
    | [] -> gph0
    | f::t -> on_facts (add_initial_fact gph0 f) t 

  in on_facts gph initial_facts
;;

 (* When facts are added, we need to add corresponding no_op rules : *)
let add_no_op gph new_facts =

  try 
    let for_each_fact gph0 fact =
      let n_f = get_fact_number gph0 fact 
      in let date = get_activation gph0 n_f
      in let label = No_op (fact) in
      let gph1 = add_label_from_pre gph0 label [| n_f |] date 
      in
      let gph2,_ = add_fact_from_add gph1 label [| fact |]
      in create_definitive_mutex gph2 label

    
    in let rec all_facts gph0 = function
        | [] -> gph0
        | fact::l -> all_facts (for_each_fact gph0 fact) l 
    in all_facts gph new_facts
    
  with
  |Undef(x,_) -> raise (Undef(x,"add_no_op"))

;;

 (* A function that adds a list of rules and 
  * their effects to the graph : *)
let add_rules gph (list_of_rules:rule list) (date:int) =
  try
    let exists_in_graph (lab,_,_,_) = 
      List.fold_left (fun x -> (fun lab0 -> x || lab = lab0)) 
      false (get_rule_list gph)
    in let is_really_applicable (lab,pre,add,del) = 
      not(is_mutex_fact_array gph pre date) && 
      not( exists_in_graph (lab,pre,add,del))
    in let on_applicable_rule gph0 ((lab,pre,add,del):rule) = 
      let pre_nb = Array.map (get_fact_number gph0) pre 
      in let gph1 = add_label_from_pre gph0 lab pre_nb date
      in let gph2, l_facts = add_fact_from_add gph1 lab add
      in begin
        add_fact_from_del gph2 lab del ;
        create_definitive_mutex gph2 lab, l_facts
      end
    in let rec on_all_rules gph0 = function
      | [] ,l_facts0  -> gph0, l_facts0
      | r::t,l_facts0 when is_really_applicable r -> 
        let gph1,l_facts1 = on_applicable_rule gph0 r in 
        on_all_rules gph1 (t,(List.rev_append l_facts1 l_facts0))
      | _::t,l_facts0 -> on_all_rules gph0 (t,l_facts0)
    
    in on_all_rules gph (list_of_rules,[])
  with
  |Undef(x,_) -> raise (Undef (x,"add_rules in planning_graph.ml"))
;;

(* Mutex propagation function : *)

let propagate_mutex gph (date:int) =
        (* 
         * The goal of this function is to propagate mutex
         * from date-1 to date for rules
         * and from date to date+1 for facts *)
        let bnd = get_bound gph in
        let compare_rules gph0 n m =
                let mut = get_mutex gph0 in
                let mut_nm = mutex_result mut n m in
                let sort_n = get_sort (get_node gph n) in
                let sort_m = get_sort (get_node gph m) in
                let date_dec = (date-1) in
                match sort_n,sort_m with
                | F,R -> gph0
                | R,F -> gph0
                | F,F -> gph0
                | R,R when (mut_nm = Never && must_mutex_rules gph0 date n m ) -> add_mutex gph0 n m
                | R,R when ( (is_until_end mut_nm) && not ( must_mutex_rules gph0 date n m )) -> end_mutex_sym gph0 n m date_dec
                (* We avoid comparing rules with For_ever mutex as it will not change and other cases.
                 * The invariant is that rules that rules that have been ended have Until(n) as mutex,
                 * and other have not. The life of the mutex is as follows:
                         * Case 1: Never -> For_ever
                         * Case 2: Never -> Until_end -> Until n 
                 * *)
                | R,R -> gph0
                | _ -> raise (Invalid_argument "function compare_rules in propagate_mutex in planning_graph.ml")
        in let rec aux_rules gph0 = function 
                | 1,0 -> compare_rules gph0 1 0
                | n,0 -> aux_rules (compare_rules gph0 n 0) (n-1, n-2)
                | n,m -> aux_rules (compare_rules gph0 n m) (n,m-1)
        in let compare_facts gph0 n m =
                let sort_n = get_sort (get_node gph n) in
                let sort_m = get_sort (get_node gph m) in
                let mut = get_mutex gph0 in
                let mut_nm = mutex_result mut n m in
                match sort_n,sort_m with
                | F,R -> gph0
                | R,F -> gph0
                | R,R -> gph0
                | F,F when (mut_nm = Never && must_mutex_facts gph0 date n m ) -> add_mutex gph0 n m
                | F,F when ((is_until_end mut_nm) && not ( must_mutex_facts gph0 date n m )) -> end_mutex_sym gph0 n m date
                (* Same idea as for the rules (only Case 2) *)
                | F,F -> gph0
                | _ -> raise (Invalid_argument "function compare_facts in propagate_mutex in planning_graph.ml")
        in let rec aux_facts gph0 = function 
                | 1,0 -> compare_facts gph0 1 0
                | n,0 -> aux_facts (compare_facts gph0 n 0) (n-1, n-2)
                | n,m -> aux_facts (compare_facts gph0 n m) (n,m-1)

        in aux_facts (aux_rules gph (bnd-1,bnd-2)) (bnd-1,bnd-2)
;;

