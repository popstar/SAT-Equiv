open Translation ;; 
open Parser ;; 
open Type_compliant;;
open Static ;;
open Terminaison;;
open Algo;;

let file = "__planning" ;;
let file_sat = String.concat "." [file ; "dimacs"];;
let file_out = String.concat "." [file ; "out"];;

exception Contradiction_between_arguments ;;
exception Without_file ;;
exception Asym ;;

let main () =
        let argument_reader =
                let equiv       = ref true  in
                let pq          = ref true in (* true : p , false : q - inutile si !equiv = true *)
                let efface      = ref true in
                let compliance_check = ref "yes" in
                let constants = ref None in (* None : (c0,c1,Omega), Some(true) : (c0,Omega), Some(false): (c0,c1) *)
                let spec_list   =
                        [
                        ("--inclusionP" , Arg.Unit (fun () -> equiv := false; pq := true ) , "Tests only the inclusion of P in Q");
                        ("-p"           , Arg.Unit (fun () -> equiv := false; pq := true ) , "Same as --inclusionP");
                        ("--inclusionQ" , Arg.Unit (fun () -> equiv := false; pq := false ) , "Tests only the inclusion of Q in P");
                        ("-q"           , Arg.Unit (fun () -> equiv := false; pq := false ) , "Same as --inclusionQ");
                        ("--equivalence", Arg.Unit (fun () -> equiv := true ) , "Tests the complete equivalence (default)");
                        ("-e"           , Arg.Unit (fun () -> equiv := true ) , "Same as --equivalence");
                        ("--remove"     , Arg.Unit (fun () -> () ) , "Removes the files used by the SAT solver (default)");
                        ("-r"           , Arg.Unit (fun () -> () ) , "Same as --remove");
                        ("--keep"       , Arg.Unit (fun () -> efface := false ) , "Keeps the last files used by the SAT solver");
                        ("-k"           , Arg.Unit (fun () -> efface := false ) , "Same as --keep");
                        ("--compliance"     , Arg.Unit (fun () -> () ) , "Checks the compliance and print a message if the protocol is not type-compliant (default)");
                        ("-c"               , Arg.Unit (fun () -> () ) , "Same as --compliance");
                        ("--compliance-only", Arg.Unit (fun () -> compliance_check := "only" ) , "Only checks compliance");
                        ("-o"               , Arg.Unit (fun () -> compliance_check := "only" ) , "Same as --compliance-only");
                        ("--no-compliance"  , Arg.Unit (fun () -> compliance_check := "none" ) , "Does not check compliance");
                        ("-n"               , Arg.Unit (fun () -> compliance_check := "none" ) , "Same as --no-compliance");
                        ("--three-constants", Arg.Unit (fun () -> constants := None ), "Uses three constants (c0,c1,Omega) (default)" );
                        ("-w", Arg.Unit (fun () -> constants := Some(true) ), "Uses only one constant plus Omega (c0,Omega)" );
                        (* w comme omega *)
                        ("-t", Arg.Unit (fun () -> constants := Some(false) ), "Uses only two constants (c0,c1)" )
                        (* t comme typed *)
                        ]
                in let usage_msg = "This is a tool for verification of cryptographic protocols.\n\nUsage: ./satequiv file [options] \n\n Options available:\n"
                in Arg.parse spec_list print_endline usage_msg; (!equiv,!pq,!efface,!compliance_check,!constants)

        in let instructions assym priv twoConstants p_include_q 
        ik_parse fresh_var =
                let our_facts,our_rules = 
                  get_all_rules twoConstants p_include_q 
                  ik_parse fresh_var in
                
                let static_rules = 
                  all_static_rules assym p_include_q in
                let b = borne p_include_q priv in
                
                algo b file_sat file_out our_facts our_rules static_rules

                
        in let when_equiv assym priv twoConstants compliance_check p_include_q q_include_p ik_parse fresh_var =
                print_string "P include Q:\n" ;
                begin
                if compliance_check && not (check_compliance_biprotocol p_include_q) 
                then print_string "Warning: P is not type compliant!\n"
                end ;
                match instructions assym priv twoConstants p_include_q ik_parse fresh_var 
                with
                | true -> 
                    let _ = 
                      print_string "Q include P:\n" ;
                      begin
                        if compliance_check && not (check_compliance_biprotocol q_include_p) 
                        then print_string "Warning: Q is not type compliant!\n"
                      end ;
                      instructions assym priv twoConstants q_include_p ik_parse fresh_var
                    in ()
                | false -> ()
                
        in let when_include assym priv twoConstants pq compliance_check p_include_q ik_parse fresh_var =
                
                let warningMessage,reminder = match pq with
                | true  -> "Warning: P is not type compliant!\n","P include Q:\n"
                | false -> "Warning: Q is not type compliant!\n","Q include P:\n"
                
                in print_string reminder ;
                begin
                if compliance_check && not (check_compliance_biprotocol p_include_q) 
                then print_string warningMessage ;
                let _ =  instructions assym priv twoConstants p_include_q ik_parse fresh_var
                in ()
                end


        in let (equiv,pq,efface,compliance_check,constants) = argument_reader
        in let ic_parse = 
                try open_in (Sys.argv.(1))
                with
                | Invalid_argument _ (*"index out of bounds"*) -> raise Without_file
        in let assym,priv,ik_parse,p_include_q,q_include_p,fresh_var = complete_parser (not (constants = Some(false))) ic_parse
        in let inclusionProcess = 
                match pq with
                | true -> p_include_q
                | false -> q_include_p
        in begin match assym,equiv,compliance_check with
        | true,_,_    -> raise Asym
        | _,true,"only"  -> 
                        let compliantp = check_compliance_biprotocol p_include_q in
                        let compliantq = check_compliance_biprotocol q_include_p in
                        begin
                                match compliantp,compliantq with
                                | true,true -> print_string "P and Q are type compliant.\n"
                                | true,false -> print_string "P is type compliant but Q is not (!).\n"
                                | false,true -> print_string "P is not type compliant (!) but Q is.\n"
                                | _ -> print_string "Neither P nor Q is type compliant !!\n"
                        end
        | _,false,"only" ->
                        let compliantp = check_compliance_biprotocol inclusionProcess in
                        begin
                                match compliantp,pq with
                                | true,true  -> print_string "P is type compliant.\n"
                                | true,false -> print_string "Q is type compliant.\n"
                                | false,true  -> print_string "P is not type compliant !\n"
                                | false,false -> print_string "Q is not type compliant !\n"
                        end
        | _,true, "none" -> when_equiv assym priv (not (constants = Some(true))) false p_include_q q_include_p ik_parse fresh_var
        | _,true, "yes"  -> when_equiv assym priv (not (constants = Some(true))) true  p_include_q q_include_p ik_parse fresh_var
        | _,false,"none" -> when_include assym priv (not (constants = Some(true))) pq false inclusionProcess ik_parse fresh_var
        | _,false,"yes"  -> when_include assym priv (not (constants = Some(true))) pq true  inclusionProcess ik_parse fresh_var
        | _ -> raise (Invalid_argument "impossible")
        end ;

        if efface then let _ = Unix.system (String.concat " " ["rm -f" ; file_sat ; file_out ] ) in ()
;;

try main () with
        | Without_file -> print_string "You should indicate a file or type ./satequiv --help for more informations.\n"  
        | Asym -> print_string "Sorry, asymmetric cryptography is not available yet.\n"
