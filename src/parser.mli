(*
exception Parse_error of string
exception Undefined
exception Not_simple of int
*)

val complete_parser :
  bool ->
  in_channel ->
  bool * Terms.typed_term list *
  Multiset.fact list *
  Process.protocol *
  Process.protocol *
  int
