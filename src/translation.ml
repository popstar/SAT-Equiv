open Atoms ;;
open Terms ;;
open Multiset ;;
open Process ;;
open Variables ;;
open Flattening ;;

let untype_array = Array.map untype ;;

let to_rule (p:typed_process) (q:atom process) (list_of_rules:rule list) (input_types:atom_type term list) fresh_var fresh_id =
        let rec single_rule list_of_rules input_types c p0 q0 tp tq sn fresh_var fresh_id =
                match p0,q0 with
                |Out(cp,up,p1),Out(cq,uq,q1) ->
                                let tp1,tp_lab = variables_state_typed   tp (Out(cp,up,p1)) in
                                let tq1,tq_lab = variables_state_untyped tq (Out(cq,uq,q1)) in
                                let new_rule = 
                                        (Ptcl(fresh_id,c,sn,untype_array tp_lab,tq_lab) , 
                                        [|State(c,sn,tp,tq)|] ,
                                        [|State(c,sn+1,tp1,tq1) ; Ik(up,uq) |],
                                        [|State(c,sn,tp,tq)|])
                                in principale (new_rule::list_of_rules) input_types c p1 q1 tp1 tq1 (sn+1) fresh_var (fresh_id+1)
                |In(cp,up,p1),In(cq,uq,q1) ->
                               let tp1,tp_lab = variables_state_typed   tp (In(cp,up,p1)) in
                               let tq1,tq_lab = variables_state_untyped tq (In(cq,uq,q1)) in
                               let new_rule =
                                       (Ptcl(fresh_id,c,sn,untype_array tp_lab,tq_lab) , 
                                       [|State(c,sn,tp,tq); Ik(up,uq) |] ,
                                       [|State(c,sn+1,tp1,tq1)|],
                                       [|State(c,sn,tp,tq)|])
                               in let tp = get_type up 
                               in let decomposed_rules,nfv,nfid = decompose new_rule fresh_var (fresh_id+1)
                               in principale (List.rev_append decomposed_rules list_of_rules) (tp::input_types) c p1 q1 tp1 tq1 (sn+1) nfv nfid
               | In(cp,up,_),_ ->
                               let _,tp_lab = variables_state_typed   tp (In(cp,up,Null)) in
                               let _,tq_lab = variables_state_untyped tq (In(cp,x fresh_var,Null)) in
                               let new_rule =
                                       (Asym(fresh_id,c,sn,untype_array tp_lab,tq_lab) , 
                                       [|State(c,sn,tp,tq); Ik(up,x fresh_var)|], 
                                       [|Bad |],
                                       [||])
                               in let tp = get_type up 
                               in let decomposed_rules,nfv,nfid = decompose new_rule (fresh_var+1) (fresh_id+1)
                               in (List.rev_append decomposed_rules list_of_rules),(tp::input_types),nfv,nfid
               | _ -> raise (Invalid_argument "single_rule in to_rule in process.ml")
        
        and principale list_of_rules input_types c p0 q0 tp tq sn fresh_var fresh_id =
               match p0,q0 with
               | Null,_ -> list_of_rules,input_types,fresh_var,fresh_id
               | Out(cp,_,_),Out(cq,_,_) when ( c != cp || c != cq) -> raise (Invalid_argument "channel error in to_rule in process.ml")
               | Out(cp,up,p1),Out(cq,uq,q1) -> 
                               single_rule list_of_rules input_types c (Out(cp,up,p1)) (Out(cq,uq,q1)) tp tq sn fresh_var fresh_id
               | Out(_,up,_),_ ->
                               let new_rule =
                                       (Asym(fresh_id,c,sn,untype_array tp,tq) , 
                                       [|State(c,sn,tp,tq)|] ,
                                       [|Bad ; Ik(up,At(Cst 0)) |],
                                       [||])
                               in (new_rule::list_of_rules),input_types,fresh_var,fresh_id+1
               | In(cp,_,_),In(cq,_,_) when ( c != cp || c != cq) -> raise (Invalid_argument "channel error in to_rule in process.ml")
               | In(cp,up,p1),In(cq,uq,q1) -> single_rule list_of_rules input_types c (In(cp,up,p1)) (In(cq,uq,q1)) tp tq sn fresh_var fresh_id
               | In(cp,up,p1),q -> single_rule list_of_rules input_types c (In(cp,up,p1)) q tp tq sn fresh_var fresh_id
                               
        in match (is_null p) with
        | true -> list_of_rules,input_types,fresh_var,fresh_id
        | false -> let c = get_channel p in principale list_of_rules input_types c p q [||] [||] 0 fresh_var fresh_id
;;

let decomposition_rules =
        (* Règles de déduction *)
        [
                Adv(0,[|x 0 ; x 1|],[|x 2 ; x 3|]),
                [|Ik(Senc(yt 0 (At(Cameleon)),yt 1 (At(Cameleon))) , Senc(x 2,x 3)) ; Ik(yt 1 (At(Cameleon)),x 3) |],
                [|Ik(yt 0 (At(Cameleon)),x 2)|],
                [||];

                Adv(1,[|x 0;x 1|],[|x 2;x 3|]),
                [|Ik(Pair(yt 0 (At(Cameleon)),yt 1 (At(Cameleon))),Pair(x 2,x 3))|],
                [|Ik(yt 0 (At(Cameleon)),x 2) ; Ik(yt 1 (At(Cameleon)),x 3)|],
                [||];

                Adv(2,[|x 0;x 1|],[|x 2;x 3|]),
                [|Ik(Aenc(yt 0 (At(Cameleon)),Pub( At(Var(1), At(Cameleon)))),Aenc(x 2,Pub(x 3)));Ik(At(Var(1),At(Cameleon)),x 3)|],
                [|Ik(yt 0 (At(Cameleon)),x 2)|],
                [||];

                Adv(3,[|x 0;x 1|],[|x 2;x 3|]),
                [|Ik(Sign(yt 0 (At(Cameleon)), At(Var(1), At(Cameleon))),Sign(x 2,x 3)) |],
                [|Ik(yt 0 (At(Cameleon)),x 2)|],
                [||];
];;

(* Je suppose que le protocole arrive sous la forme suivante :
        * (P0,Q0),...(Pn,Qn)
        *) 

let get_all_rules twoConstants parsed_protocol parsed_facts fresh_var =
        let rec ptcl_rules rule_list input_types fact_list fresh_id = function
        | [] -> rule_list,input_types,fact_list
        | (p,q)::t -> 
                        let new_rule_list,new_input_types,_,nfid  =  to_rule p q rule_list input_types fresh_var fresh_id in
                        let p_null = is_null p in
                        let q_null = is_null q in
                        begin
                        match p_null,q_null with
                        | false,_     -> 
                                        let f = State(get_channel p,0,[||],[||]) 
                                        in ptcl_rules new_rule_list new_input_types (f::fact_list) nfid t
                        | true,false ->  
                                        let f = State(get_channel q,0,[||],[||]) 
                                        in ptcl_rules new_rule_list new_input_types (f::fact_list) nfid t 
                        | true,true -> ptcl_rules new_rule_list new_input_types fact_list fresh_id t 
                        end
                        
        in let rule_list,_,fact_list = ptcl_rules decomposition_rules [] parsed_facts 0 parsed_protocol 
        in let all_facts = match twoConstants with
        |true  -> Ik(At(Cst(1),(At(Cameleon))) ,c 1) :: ( Ik(At(Cst(0),(At(Cameleon))) ,c 0) :: fact_list )
        |false -> Ik(At(Cst(0),(At(Cameleon))) ,c 0) :: fact_list 
        in all_facts,rule_list
;;
