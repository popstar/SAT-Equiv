type position = int list
val sort : position option list -> position list

type positionneur = Multiset.fact -> position option

val update :
  positionneur -> Multiset.fact -> Multiset.fact -> Multiset.fact option -> positionneur
