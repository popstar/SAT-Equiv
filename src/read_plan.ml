open Terms ;;
open Multiset ;;
open Substitutions ;;
open Planning_graph;;

(* Cette partie du programme s'occupe du problème suivant :
 * On suppose qu'on a récupéré un plan de Bad
 * et on veut rendre ce plan lisible.
 * Pour ce faire :
         * 1 - remettre les règles dans l'ordre des étapes et éliminer les no-op.
         * 2 - pour chaque règle, retrouver de quelle règle abstraite elle provient
         * 3 - retrouver des informations dans la règle abstraite et transformer en notation Alice-Bob.
 *)

let sort_labels list_of_nodes =
        let rec insert_label l step = function
                |[] -> (l,step)::[]
                |(l0,s)::t when step<=s -> (l,step)::((l0,s)::t)
                |(l0,s)::t -> (l0,s)::(insert_label l step t)
        in let rec sort_help res = function
                | [] -> res
                | (Fact(_),_)::t -> sort_help res t
                | (Rule(No_op(_)),_)::t -> sort_help res t
                | (Rule(x),step)::t -> sort_help (insert_label x step res) t
                | _ -> raise (Invalid_argument "No Undefined node should occur in sort_help in sort_labels in read_plan.ml")
        in sort_help [] list_of_nodes
;;

let from_which_rule label our_rules =
        (* Nous recherchons seulement une règle dont le label s'unifie avec notre label.
         * S'il y en a plus d'une, ce sont en fait des versions de la même règle
         * obtenues par l'aplatissement. 
         * *)
        let fact_map (sigmaG,sigmaD) = function
                | Ik(tG,tD) -> (Ik( untyping_term_map_var sigmaG tG, untyped_term_map sigmaD tD):untyped_fact)
                | State(n,m,tG,tD) -> State(n,m, Array.map (fun t -> untyping_term_map_var sigmaG t) tG , Array.map (fun t -> untyped_term_map sigmaD t) tD)
                | Bad -> Bad
        in let label_map (sigmaG,sigmaD) = 
                let array_terms_mapG = Array.map (fun u -> untyped_term_map sigmaG u) in
                let array_terms_mapD = Array.map (fun u -> untyped_term_map sigmaD u) in
                function
                        | Adv (n,varsG,varsD) -> Adv (n, array_terms_mapG varsG, array_terms_mapD varsD)
                        | Ptcl(rid,c,sn,varsG,varsD) -> Ptcl(rid,c,sn, array_terms_mapG varsG, array_terms_mapD varsD)
                        | Asym(rid,c,sn,varsG,varsD) -> Asym(rid,c,sn, array_terms_mapG varsG, array_terms_mapD varsD)
                        | No_op(_) -> 
                          raise (Invalid_argument "At this point, No_op rules should have been removed in label_map in read_plan.ml")
                        | Failing_adv (_,_,_) -> 
                          raise (Invalid_argument "Failing_adv should not occur as argument of fact_map in from_which_rule in read_plan.ml ")
                        | Failing_ptcl (_,_,_,_,_,_) -> 
                          raise (Invalid_argument "Failing_ptcl should not occur as argument of fact_map in from_which_rule in read_plan.ml ")
        in let rule_map sigma = function
               (lab,pre,add,del) -> let array_map = Array.map (fact_map sigma)
                                in ((label_map sigma lab, array_map pre, array_map add, array_map del):untyped_rule)
        in let rec find_rule = function
                | [] -> raise Not_found
                | r::t -> 
                                let lab,_,_,_ = r in
                                let b,sp,sq = pm_lab label lab in
                                match b with
                                | true  -> rule_map (sp,sq) r
                                | false -> find_rule t
        in find_rule our_rules
;;

let print_one_rule r count =
        let lab,pre,add,_ = r in
        let has_ik arr = 
                let is_ik = function 
                        |Ik(_,_) -> (fun _ -> true) 
                        | _ -> (fun b -> b)
                in Array.fold_right is_ik arr false 
        in
        let print_arrow () = print_string " -> " in
        let print_fact = function
              | Ik(tp,tq) -> begin print_string "(" ; print_term tp ; print_string "," ; print_term tq ; print_string "), " end
              | _ -> ()
        in let print_array arr =
                Array.iter print_fact arr
        in let untype_pre pre =
                let untype_fact = function
                        |Ik(a,b) -> Ik(untype a,b)
                        |State(c,sn,tp,tq) -> State(c,sn,Array.map untype tp,tq)
                        |Bad -> Bad
                in Array.map untype_fact pre
        in match lab with
        (*
        |Adv(n,_,_) when n > 3 ->  count
        *)
        |Adv(_,_,_) ->
                        begin 
                        print_int count ; 
                        print_string ". Intruder uses his ability to deduce: \n " ; 
                        print_array pre ; 
                        print_arrow (); 
                        print_array add ; 
                        print_string "\n";
                        count + 1
                        end
        |Failing_adv(0,_,pre) ->
                        begin
                        print_int count ; 
                        print_string ". Intruder makes the difference\n through its symmetric decryption ability (and/or by atomicity tests)\n that breaks static equivalence: \n" ;
                        print_array (untype_pre pre) ;
                        print_string "\n";
                        count+1
                        end
        |Failing_adv(1,_,pre_lab) ->
                        begin
                        print_int count ; 
                        print_string ". Intruder makes the difference\n through its ability to decompose pairs (and to test atomicity)\n that breaks static equivalence: \n" ;
                        print_array (untype_pre pre_lab) ;
                        print_string "\n";
                        count+1
                        end
        |Failing_adv(2,_,pre_lab) ->
                        begin
                        print_int count ; 
                        print_string ". Intruder makes the difference\n through its asymmetric decryption ability (and/or by atomicity tests)\n that breaks static equivalence: \n" ;
                        print_array (untype_pre pre_lab) ;
                        print_string "\n";
                        count+1
                        end

        |Failing_adv(3,_,pre_lab) ->
                        begin
                        print_int count ; 
                        print_string ". Intruder makes the difference\n through its ability to get signed messages (and/or by atomicity tests)\n that breaks static equivalence: \n" ;
                        print_array (untype_pre pre_lab) ;
                        print_string "\n";
                        count+1
                        end
 
        |Failing_adv(4,_,pre) ->
                        begin
                        print_int count ; 
                        print_string ". Intruder makes the difference\n through its ability to verify signatures \n that breaks static equivalence: \n" ;
                        print_array (untype_pre pre) ;
                        print_string "\n";
                        count+1
                        end
        |Failing_adv(_,_,pre) ->
                        begin
                        print_int count ; 
                        print_string ". Intruder makes the difference\n through its ability to build bigger messages from the following knowledge.\n Recall that he may observe atomicity of keys and messages. \n It breaks static equivalence. \n " ;
                        print_array (untype_pre pre) ;
                        print_string "\n";
                        count+1
                        end

        | Ptcl(_,c,sn,_,_) when has_ik pre (* input *) ->
                        begin
                                print_int count ; print_string ". The intruder inputs " ; print_array pre ; 
                                print_string "at step " ; print_int sn ; 
                                print_string " on channel " ; print_int c ; print_string ". \n";
                                count+1
                        end
        | Ptcl(_,c,sn,_,_) (* output *) ->
                        begin
                                print_int count ; print_string ". The intruder learns " ; print_array add ; 
                                print_string "after output at step " ; print_int sn ; 
                                print_string " on channel " ; print_int c ; print_string ". \n";
                                count+1
                        end
        | Failing_ptcl(_,c,sn,_,pre,_) when has_ik pre (* input *) ->
                        begin
                                print_int count ; print_string ". The intruder inputs " ; print_array (untype_pre pre) ;
                                print_string "at step " ; print_int sn ;
                                print_string " on channel " ; print_int c ; print_string ". \n" ;
                                print_string "Protocol P accepts this input.\n" ;
                                print_string "We are in one of the following cases :\n";
                                print_string "  1. As either it does not match the expected pattern in Q,\n" ;
                                print_string "  or some message in key position is not atomic,\n" ;
                                print_string "  Q can not accept this input and it breaks the equivalence.\n";
                                print_string "  2. The message that should be inputed in Q is not atomic,\n" ;
                                print_string "  but the one in P is,\n" ;
                                print_string "  so the intruder could make the difference between those two inputs.\n";
                                count+1
                        end
        | Failing_ptcl(_,c,sn,_,_,add) (* output *) ->
                        begin
                                print_int count ; print_string ". The intruder learns " ; print_array (untype_pre add) ;
                                print_string "at step " ; print_int sn ;
                                print_string " on channel " ; print_int c ; print_string " in protocol P. \n" ;
                                print_string "Q can not do a similar output because it would have non atomic key.\n" ;
                                print_string "These distinct behaviours break the equivalence.\n";
                                count+1
                        end
        | Asym(_,c,sn,_,_) when has_ik pre (* input *) ->
                        begin
                                print_int count ; print_string ". The intruder inputs " ; print_array pre ; 
                                print_string "at step " ; print_int sn ; 
                                print_string " on channel " ; print_int c ; print_string ". \n" ;
                                print_string "That input passes in P but not in Q which breaks equivalence.\n";
                                count+1
                        end
        | Asym(_,c,sn,_,_) (* output *) ->
                        begin
                                print_int count ; print_string ". There is an available output at step " ; print_int sn ; 
                                print_string " on channel " ; print_int c ; print_string " in protocol P. \n" ;
                                print_string "As this cannot be imited in Q, it breaks equivalence.\n";
                                count+1
                        end
        | _ -> raise (Invalid_argument " in print_one_rule in read_plan.ml")
;;
       
let print_lisible list_of_nodes our_rules =
        let sorted_labs = sort_labels list_of_nodes in
        let rec print_all count = function
                | [] -> ()
                (* On s'arrête dans les deux cas suivants car on a trouvé une attaque
                 * et toutes les règles qui restent dans la liste se produisent ensuite. *)
                | (Failing_adv(n,t1,pre),_)::_ ->
                                let _ = print_one_rule (Failing_adv(n,t1,pre),[||],[||],[||]) count in ()
                | (Failing_ptcl(rid,c,sn,t1,pre,add),_)::_ ->
                                let _ = print_one_rule (Failing_ptcl(rid,c,sn,t1,pre,add),[||],[||],[||]) count in ()
                | (Asym(rid,c,sn,t1,t2),_)::_ ->
                                let _ = print_one_rule (from_which_rule (Asym(rid,c,sn,t1,t2)) our_rules) count in ()
                | (lab,_)::l -> let n_count = print_one_rule (from_which_rule lab our_rules) count in print_all n_count l
        in print_all 1 sorted_labs
;;


