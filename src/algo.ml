open Multiset;;
open Planning_graph;;
open Mutex;;
open Minisat;;
open Affichage;;


let algo bound file_sat file_out our_facts our_rules static_rules =
  
  let gph = build_initial_graph our_facts in
  let one_step gph0 rules0 date new_facts0 =
    let facts = get_fact_list gph0 
    in let rules =  ground_reachable_rules facts rules0
    in let gph1,new_facts1 =
      add_rules (add_no_op gph0 new_facts0) rules date 
    in propagate_mutex gph1 date, new_facts1

  in let leveled_off_values gph0 date =
    let bnd = get_bound gph0 in
    let nb_mut = number_of_mutex (get_mutex gph0) date bnd
    in bnd,nb_mut

  in let examine gph1 step  =
    try 
      let n = get_fact_number gph1 Multiset.Bad in
      verifie file_sat file_out gph1 our_facts step n
    with Undef("F",_) -> gph1

  in let rec until_end bound gph0 rules0 date (n0,m0) new_facts0 = 
    let gph1,new_facts1 = one_step gph0 rules0 date new_facts0 in
    let gph2 = examine gph1 date in
    let n1,m1 = leveled_off_values gph2 date in
    match ((n0 = n1 && m0 = m1)||bound = 0) with
    | true -> date,gph2,m1
    |false -> 
        until_end (bound-1) gph2 rules0 (date+1) (n1,m1) new_facts1

  in let rec until_end_unbounded gph0 rules0 date (n0,m0) new_facts0 =
    let gph1,new_facts1 = one_step gph0 rules0 date new_facts0 in
    let gph2 = examine gph1 date in
    let n1,m1 = leveled_off_values gph2 date in
    match (n0 = n1 && m0 = m1) with
    | true -> date,gph2,m1
    |false -> 
        until_end_unbounded gph2 rules0 (date+1) (n1,m1) new_facts1


  in 
  try
    let date1,gph1,m0 = 
      until_end_unbounded gph our_rules 0 (get_bound gph,0) our_facts
    in let date2,gph2,_ = 
      until_end_unbounded gph1 static_rules (date1+1) (get_bound gph1,m0) []

    in let nb_noeuds = get_bound gph2 in
      begin
        print_stats bound nb_noeuds date2 ;
        print_string no_attack ;
        true
      end
  with
  | Sat(gph1,step,nb_vars,nb_clauses) ->
      let nb_noeuds = get_bound gph1 in
      begin
        print_stats bound nb_noeuds step ;
        let rules_print = List.rev_append static_rules our_rules in
        affiche file_out gph1 step rules_print ;
        print_stats_sat nb_vars nb_clauses ;
        false
      end
;;
