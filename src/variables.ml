open Atoms ;;
open Terms ;;
open Process ;;

let rec add_if_notin a = function
        | [] -> [a]
        | b::t when b = a -> b::t
        | b::t -> b::(add_if_notin a t)
;;


let variables_untyped u =
        let rec aux res = function
                | Aenc(a,b) -> aux (aux res a) b
                | Sign(a,b) -> aux (aux res a) b
                | Senc(a,b) -> aux (aux res a) b
                | Pair(a,b) -> aux (aux res a) b
                | Hash(a) -> aux res a
                | Pub(a) -> aux res a
                | Vk(a)  -> aux res a
                | At(x) when is_variable x -> add_if_notin x res
                | At(_) -> res
                | Omega -> raise (Invalid_argument "Omega should not be taken as an argument of variables in variables.ml")
        in aux [] u
;;

let variables_typed u =
        let rec aux res = function
                | Aenc(a,b) -> aux (aux res a) b
                | Sign(a,b) -> aux (aux res a) b
                | Senc(a,b) -> aux (aux res a) b
                | Pair(a,b) -> aux (aux res a) b
                | Hash(a) -> aux res a
                | Pub(a) -> aux res a
                | Vk(a)  -> aux res a
                | At(x,tx) when is_variable x -> add_if_notin (x,tx) res
                | At(_) -> res
                | Omega -> raise (Invalid_argument "Omega should not be taken as an argument of variables in variables.ml")
        in aux [] u
;;

let vars_to_array u = Array.of_list (List.map (fun x -> At(x)) u);;
let array_to_vars u =
        let atomize = function
                | At(x) -> x
                | _ -> raise (Invalid_argument "atomize should only be used on atomic terms")
        in List.map atomize (Array.to_list u)
;;

let minus_set set to_remove =
        let rec remove x = function
                | [] -> []
                | a::t when a = x -> t
                | a::t -> a::remove x t
        in let rec remove_all p = function
                | [] -> p
                | a::t -> remove_all (remove a p) t
        in remove_all set to_remove
;;

let rec union setA = function
        | [] -> setA
        | a::t -> union (add_if_notin a setA) t
;;

exception Variable_bound_in_output ;;

let output_check_and_remove past_vars vars_u =
        match List.for_all (fun x -> List.mem x past_vars) vars_u with
        | true -> minus_set past_vars vars_u
        | false -> raise Variable_bound_in_output
;;

let appears_later_typed vars_u next =
        let rec explore_and_removes useless = function
                | _ when useless = [] -> []
                | Null -> useless
                | In(_,v,p)  -> explore_and_removes (minus_set useless (variables_typed v)) p
                | Out(_,v,p) -> explore_and_removes (minus_set useless (variables_typed v)) p
        in minus_set vars_u (explore_and_removes vars_u next)
;;

let appears_later_untyped vars_u next =
        let rec explore_and_removes useless = function
                | _ when useless = [] -> []
                | Null -> useless
                | In(_,v,p)  -> explore_and_removes (minus_set useless (variables_untyped v)) p
                | Out(_,v,p) -> explore_and_removes (minus_set useless (variables_untyped v)) p
        in minus_set vars_u (explore_and_removes vars_u next)
;;


let usefull_variables_typed past = function
        (* usefull_variables doit servir à reconnaître les variables qu'il sera utile de garder pour plus tard.
         * Par exemple, dans le processus in(c,x); il est inutile de garder la variable x pour la suite. *)
        | Null -> [],[]
        | In(_,u,p)  -> 
                        let vars_u = variables_typed u in
                        let past_and_later = minus_set past vars_u in
                        let vars_u_later = appears_later_typed vars_u p in
                        vars_u,List.rev_append vars_u_later past_and_later
        (* En principe il ne devrait pas y avoir de nouvelles variables dans un output : *)
        | Out(_,u,p) ->
                        let vars_u = variables_typed u in
                        let past_and_later = output_check_and_remove past vars_u in
                        let vars_u_later = appears_later_typed vars_u p in
                        vars_u,List.rev_append vars_u_later past_and_later
;;

let usefull_variables_untyped past = function
        (* usefull_variables doit servir à reconnaître les variables qu'il sera utile de garder pour plus tard.
         * Par exemple, dans le processus in(c,x); il est inutile de garder la variable x pour la suite. *)
        | Null -> [],[]
        | In(_,u,p)  -> 
                        let vars_u = variables_untyped u in
                        let past_and_later = minus_set past vars_u in
                        let vars_u_later = appears_later_untyped vars_u p in
                        vars_u, List.rev_append vars_u_later past_and_later
        (* En principe il ne devrait pas y avoir de nouvelles variables dans un output : *)
        | Out(_,u,p) ->
                        let vars_u = variables_untyped u in
                        let past_and_later = output_check_and_remove past vars_u in
                        let vars_u_later = appears_later_untyped vars_u p in
                        vars_u,List.rev_append vars_u_later past_and_later
;;

let variables_state_typed   tp p = 
        let tp_vars = array_to_vars tp in
        let vars_u,futur_vars = usefull_variables_typed tp_vars p in
        let vars_label = union tp_vars vars_u in
        (vars_to_array futur_vars), (vars_to_array vars_label) 
;;

let variables_state_untyped tq q =
        let tq_vars = array_to_vars tq in
        let vars_u,futur_vars = usefull_variables_untyped tq_vars q in
        let vars_label = union tq_vars vars_u in
        (vars_to_array futur_vars), (vars_to_array vars_label) 
;;
