open Atoms ;;
open Terms ;;
open Substitutions ;;
open Multiset ;;

let add_vars (tg:'a term array) (l:'a term) =
        let longueur = Array.length tg in
        let initialise = function
                | n when n = longueur -> l
                | n -> tg.(n)
        in Array.init (longueur+1) initialise
;;

let term_deconstruction up uq fresh_var =
        (*
         * Cette fonction sert à déconstruire les termes
         * jusqu'à obtenir un atome au moins de l'un des deux côtés.
         * C'est la partie la plus facile du problème.
         *)

        (* Premier Booléen : est-ce que c'est une deconstruction ?
         * Si ce n'en est pas une, on ajoutera une règle :
                 * Ik(a,x),Ik(b,y),... -> Bad
         * Second  Booléen : est-ce qu'il faut garder la règle complète ?
         *)

        match up,uq with
        | Pub(a),Pub(b) -> true,true,Ik(a,b),None,fresh_var
        | Vk(a),Vk(b) -> true,true,Ik(a,b),  None,fresh_var
        | Hash(a),Hash(b)     -> true,true,Ik(a,b),None,fresh_var
        | Aenc(a,b),Aenc(c,d) -> true,true,Ik(a,c),Some(Ik(b,d)),fresh_var
        | Sign(a,b),Sign(c,d) -> true,true,Ik(a,c),Some(Ik(b,d)),fresh_var
        | Pair(a,b),Pair(c,d) -> true,false,Ik(a,c),Some(Ik(b,d)),fresh_var
        | Senc(a,b),Senc(c,d) -> true,true, Ik(b,d),Some(Ik(a,c)),fresh_var
        | Pair(a,b),Senc(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,false,Ik(a,x0),Some(Ik(b,x1)),fresh_var+2
        | Pair(a,b),Aenc(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,false,Ik(a,x0),Some(Ik(b,x1)),fresh_var+2
        | Pair(a,b),Sign(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,false,Ik(a,x0),Some(Ik(b,x1)),fresh_var+2
        | Pair(a,b),Hash(_)   -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,false,Ik(a,x0),Some(Ik(b,x1)),fresh_var+2
        | Pair(a,b),Pub(_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,false,Ik(a,x0),Some(Ik(b,x1)),fresh_var+2
        | Pair(a,b), Vk(_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,false,Ik(a,x0),Some(Ik(b,x1)),fresh_var+2
        | Senc(a,b),Pair(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Senc(a,b),Aenc(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Senc(a,b),Sign(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Senc(a,b),Pub(_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Senc(a,b),Hash(_)   -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Senc(a,b),Vk (_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Aenc(a,b),Pair(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Aenc(a,b),Senc(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Aenc(a,b),Sign(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Aenc(a,b),Pub(_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Aenc(a,b),Hash(_)   -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Aenc(a,b),Vk (_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Sign(a,b),Pair(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Sign(a,b),Aenc(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Sign(a,b),Senc(_,_) -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Sign(a,b),Pub(_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Sign(a,b),Hash(_)   -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Sign(a,b),Vk (_)    -> let x0 = x fresh_var in let x1 = x (fresh_var + 1) in false,true, Ik(b,x1),Some(Ik(a,x0)),fresh_var+2
        | Pub(a),Vk (_)       -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Pub(a),Hash (_)     -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Pub(a),Pair(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Pub(a),Senc(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Pub(a),Aenc(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Pub(a),Sign(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Vk (a),Pub(_)       -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Vk (a),Hash(_)      -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Vk (a),Pair(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Vk (a),Senc(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Vk (a),Aenc(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Vk (a),Sign(_,_)    -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Hash(a),Pub(_)      -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Hash(a),Vk(_)       -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Hash(a),Pair(_,_)   -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Hash(a),Senc(_,_)   -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Hash(a),Aenc(_,_)   -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | Hash(a),Sign(_,_)   -> let x0 = x fresh_var in false,true, Ik(a,x0),None,fresh_var+1
        | _ -> raise (Invalid_argument "term_deconstruction in process.ml") 
        (* Les autres possibilités sont éliminées 
         * en amont dans deconstruction_ik
         * dans on_each_rule *)
;;

let atom_in_q up uq_atom fresh_var =
        (*
         * Cette fonction continue de déconstruire les termes 
         * quand on a rencontré un atome dans Q.
         * Le premier booléen indique si l'on peut déconstruire
         * et franchir l'étape en restant en équivalence.
         * Le second booléen indique si l'on doit garder la règle d'origine.
         *)
        match up,uq_atom with
        |Pub(a),x0 when is_variable x0 ->
                                        let x1 = x fresh_var in
                                        let n0 = get_var_num x0 in
                                        let subst = add_value (Substitutions.make ()) (n0,Pub(x1)) in
                                        true,true,Ik(a,x1),None,fresh_var+1,subst
        |Hash(a),x0 when is_variable x0 ->
                                        let x1 = x fresh_var in
                                        let n0 = get_var_num x0 in
                                        let subst = add_value (Substitutions.make ()) (n0,Hash(x1)) in
                                        true,true,Ik(a,x1),None,fresh_var+1,subst
        |Vk (a),x0 when is_variable x0 ->
                                        let x1 = x fresh_var in
                                        let n0 = get_var_num x0 in
                                        let subst = add_value (Substitutions.make ()) (n0,Vk (x1)) in
                                        true,true,Ik(a,x1),None,fresh_var+1,subst
        |Pair(a,b),x0 when is_variable x0 -> 
                                        let x1 = x fresh_var in
                                        let x2 = x (fresh_var+1) in
                                        let n0 = get_var_num x0 in
                                        let subst = add_value (Substitutions.make ()) (n0,Pair(x1,x2)) in
                                        true,false,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,subst
        |Senc(a,b),x0 when is_variable x0 ->
                                        let x1 = x fresh_var in
                                        let x2 = x (fresh_var+1) in
                                        let n0 = get_var_num x0 in
                                        let subst = add_value (Substitutions.make ()) (n0,Senc(x1,x2)) in
                                        true,true,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,subst
        |Aenc(a,b),x0 when is_variable x0 ->
                                        let x1 = x fresh_var in
                                        let x2 = x (fresh_var+1) in
                                        let n0 = get_var_num x0 in
                                        let subst = add_value (Substitutions.make ()) (n0,Aenc(x1,x2)) in
                                        true,true,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,subst
        |Sign(a,b),x0 when is_variable x0 ->
                                        let x1 = x fresh_var in
                                        let x2 = x (fresh_var+1) in
                                        let n0 = get_var_num x0 in
                                        let subst = add_value (Substitutions.make ()) (n0,Sign(x1,x2)) in
                                        true,true,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,subst
        |Pair(a,b),_  -> 
                        let x1 = x fresh_var in 
                        let x2 = x (fresh_var+1) in 
                        false,false,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,Substitutions.make ()
        |Senc(a,b),_  -> 
                        let x1 = x fresh_var in 
                        let x2 = x (fresh_var+1) in 
                        false,true,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,Substitutions.make ()
        |Aenc(a,b),_  -> 
                        let x1 = x fresh_var in 
                        let x2 = x (fresh_var+1) in 
                        false,true,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,Substitutions.make ()
        |Sign(a,b),_  -> 
                        let x1 = x fresh_var in 
                        let x2 = x (fresh_var+1) in 
                        false,true,Ik(a,x1),Some(Ik(b,x2)),fresh_var+2,Substitutions.make ()
        |Hash(a),_  -> 
                        let x1 = x fresh_var in 
                        false,true,Ik(a,x1),None,fresh_var+1,Substitutions.make ()
        |Pub(a),_  -> 
                        let x1 = x fresh_var in 
                        false,true,Ik(a,x1),None,fresh_var+1,Substitutions.make ()
        |Vk (a),_  -> 
                        let x1 = x fresh_var in 
                        false,true,Ik(a,x1),None,fresh_var+1,Substitutions.make ()
        |_ -> raise (Invalid_argument "atom_in_q in process.ml")
        (* Les autres possibilités sont éliminées 
         * en amont dans deconstruction_ik
         * dans on_each_rule *)
;;

let atom_in_p (up_atom:atom) tp uq fresh_var =
         (* Cette fonction continue de déconstruire les termes
          * quand on a rencontré un atome dans P.
          * Le premier booléen indique si l'on peut déconstruire
          * et franchir l'étape en restant en équivalence
          * le deuxième booléen est toujours vrai et indique que l'on doit garder la règle mère
          * et le dernier booléen indique si côté P
          * l'on a atteint un point où :
                  * soit on a une variable de type atomique
                  * soit on n'a pas de variable
          * c'est-à-dire un point où l'on ne peut pas réduire plus
          *)

         match is_variable up_atom,tp,uq with
         | _,Omega,_ -> raise (Invalid_argument "Omega should not be a type in atom_in_p in process.ml")
         | false,_,_ -> true,true,Ik(At(up_atom,tp),uq),Some(Ik(At(up_atom,tp),uq)),fresh_var,Substitutions.make (),Substitutions.make (),true
         | true,Pair(ta,tb),Pair(c,d) ->
                                        let (x1:typed_term) = yt  fresh_var    ta in
                                        let (x2:typed_term) = yt (fresh_var+1) tb in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Pair(x1,x2)) in
                                        true,true,Ik(x1,c),Some(Ik(x2,d)),fresh_var+2,subst,Substitutions.make (),false
         | true,Senc(ta,tb),Senc(c,d) ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Senc(x1,x2)) in
                                        true,true,Ik(x1,c),Some(Ik(x2,d)),fresh_var+2,subst,Substitutions.make (),false
         | true,Aenc(ta,tb),Aenc(c,d) ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Aenc(x1,x2)) in
                                        true,true,Ik(x1,c),Some(Ik(x2,d)),fresh_var+2,subst,Substitutions.make (),false
         | true,Sign(ta,tb),Sign(c,d) ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Sign(x1,x2)) in
                                        true,true,Ik(x1,c),Some(Ik(x2,d)),fresh_var+2,subst,Substitutions.make (),false
         | true,Hash(ta),Hash(b) ->
                                        let x1 = yt  fresh_var    ta in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Hash(x1)) in
                                        true,true,Ik(x1,b),None,fresh_var+1,subst,Substitutions.make (),false

         | true,Pub(ta),Pub(b) ->
                                        let x1 = yt  fresh_var    ta in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Pub(x1)) in
                                        true,true,Ik(x1,b),None,fresh_var+1,subst,Substitutions.make (),false
         | true,Vk(ta),Vk(b) ->
                                        let x1 = yt  fresh_var    ta in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Vk(x1)) in
                                        true,true,Ik(x1,b),None,fresh_var+1,subst,Substitutions.make (),false

         | true,At(_),_ -> 
                         true,true,Ik(At(up_atom,tp),uq),Some(Ik(At(up_atom,tp),uq)),fresh_var,
                         Substitutions.make (),Substitutions.make (),true

         | true,Pair(ta,tb),At(c) when is_variable c -> 
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let nP = get_var_num up_atom in
                                        let substP = add_value (Substitutions.make ()) (nP,Pair(x1,x2)) in
                                        let nQ = get_var_num c in
                                        let substQ = add_value (Substitutions.make ()) (nQ,Pair(x3,x4)) in
                                        true,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,substP,substQ,false
         | true,Senc(ta,tb),At(c) when is_variable c -> 
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let nP = get_var_num up_atom in
                                        let substP = add_value (Substitutions.make ()) (nP,Senc(x1,x2)) in
                                        let nQ = get_var_num c in
                                        let substQ = add_value (Substitutions.make ()) (nQ,Senc(x3,x4)) in
                                        true,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,substP,substQ,false
         | true,Aenc(ta,tb),At(c) when is_variable c -> 
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let nP = get_var_num up_atom in
                                        let substP = add_value (Substitutions.make ()) (nP,Aenc(x1,x2)) in
                                        let nQ = get_var_num c in
                                        let substQ = add_value (Substitutions.make ()) (nQ,Aenc(x3,x4)) in
                                        true,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,substP,substQ,false

         | true,Sign(ta,tb),At(c) when is_variable c -> 
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let nP = get_var_num up_atom in
                                        let substP = add_value (Substitutions.make ()) (nP,Sign(x1,x2)) in
                                        let nQ = get_var_num c in
                                        let substQ = add_value (Substitutions.make ()) (nQ,Sign(x3,x4)) in
                                        true,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,substP,substQ,false

         | true,Hash(ta),At(c)     when is_variable c -> 
                                        let x1 = yt fresh_var ta in
                                        let x2 = x (fresh_var+1) in
                                        let nP = get_var_num up_atom in
                                        let substP = add_value (Substitutions.make ()) (nP,Hash(x1)) in
                                        let nQ = get_var_num c in
                                        let substQ = add_value (Substitutions.make ()) (nQ,Hash(x2)) in
                                        true,true,Ik(x1,x2),None,fresh_var+2,substP,substQ,false


         | true,Pub(ta),At(c)     when is_variable c -> 
                                        let x1 = yt fresh_var ta in
                                        let x2 = x (fresh_var+1) in
                                        let nP = get_var_num up_atom in
                                        let substP = add_value (Substitutions.make ()) (nP,Pub(x1)) in
                                        let nQ = get_var_num c in
                                        let substQ = add_value (Substitutions.make ()) (nQ,Pub(x2)) in
                                        true,true,Ik(x1,x2),None,fresh_var+2,substP,substQ,false

         | true,Vk (ta),At(c)     when is_variable c -> 
                                        let x1 = yt fresh_var ta in
                                        let x2 = x (fresh_var+1) in
                                        let nP = get_var_num up_atom in
                                        let substP = add_value (Substitutions.make ()) (nP,Vk (x1)) in
                                        let nQ = get_var_num c in
                                        let substQ = add_value (Substitutions.make ()) (nQ,Vk (x2)) in
                                        true,true,Ik(x1,x2),None,fresh_var+2,substP,substQ,false

         | true,Pair(ta,tb),_ ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Pair(x1,x2)) in
                                        false,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,subst,Substitutions.make (),false
         | true,Senc(ta,tb),_ ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Senc(x1,x2)) in
                                        false,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,subst,Substitutions.make (),false

         | true,Aenc(ta,tb),_ ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Aenc(x1,x2)) in
                                        false,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,subst,Substitutions.make (),false

         | true,Sign(ta,tb),_ ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = yt (fresh_var+1) tb in
                                        let x3 = x (fresh_var+2) in
                                        let x4 = x (fresh_var+3) in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Sign(x1,x2)) in
                                        false,true,Ik(x1,x3),Some(Ik(x2,x4)),fresh_var+4,subst,Substitutions.make (),false
         | true,Hash(ta),_ ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = x (fresh_var+1) in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Hash(x1)) in
                                        false,true,Ik(x1,x2),None,fresh_var+2,subst,Substitutions.make (),false
         | true,Pub(ta),_ ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = x (fresh_var+1) in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Pub(x1)) in
                                        false,true,Ik(x1,x2),None,fresh_var+2,subst,Substitutions.make (),false
         | true,Vk (ta),_ ->
                                        let x1 = yt  fresh_var    ta in
                                        let x2 = x (fresh_var+1) in
                                        let n0 = get_var_num up_atom in
                                        let subst = add_value (Substitutions.make ()) (n0,Vk (x1)) in
                                        false,true,Ik(x1,x2),None,fresh_var+2,subst,Substitutions.make (),false
;;

let on_each_rule (r:rule) fresh_var fresh_id get_position  =
        let rec get_iks_and_sts iks sts  = function
                | [] -> iks,sts
                | (State (c,sn,tp,tq))::l -> get_iks_and_sts iks (State (c,sn,tp,tq)::sts) l
                | (Ik (xp,xq))::l -> get_iks_and_sts (Ik (xp,xq)::iks) sts l
                | _ -> raise (Invalid_argument "in get_iks_and_sts in on_each_rule in process.ml")
        in
        let lab,pre,add,del = r in
        let pre_iks,pre_sts = get_iks_and_sts [] [] (Array.to_list pre) in
        let get_ik_arguments = function
                |(Ik (xp,xq)) -> (xp,xq)
                | _ -> raise (Invalid_argument "get_ik_arguments in on_each_rule in process.ml")
        in let deconstruction_ik ik fresh_var =
                let up,uq = get_ik_arguments ik in
                match up,uq with
                |At(a,ta),b -> atom_in_p a ta b fresh_var
                |a,At(b) -> 
                                let b0,garde,ik1,ik2,new_fv,substQ =  atom_in_q a b fresh_var 
                                in  b0,garde,ik1,ik2,new_fv,Substitutions.make (),substQ,false
                |a,b -> 
                                let b0,garde,ik1,ik2,new_fv = term_deconstruction a b fresh_var 
                                in  b0,garde,ik1,ik2,new_fv,Substitutions.make(),Substitutions.make (),false
        in let rec get_list_deconstructibles fresh_var = function
                | [] -> [],fresh_var
                | ik::t -> 
                                let b0,garde,ik1,ik2,new_fv,substP,substQ,b1 = deconstruction_ik ik fresh_var in
                                match b1 with
                                | false -> let l_end,fv_end = (get_list_deconstructibles new_fv t) 
                                                in (b0,garde,ik,ik1,ik2,substP,substQ) :: l_end,fv_end 
                                | true  ->  get_list_deconstructibles fresh_var t
         
        in let replace1 ik pre_iks pre_sts ik1 =
                let rec in_situ = function
                        |[] -> raise (Invalid_argument "in replace in on_each_rule in process.ml")
                        |a::t when (a = ik) -> ik1::t
                        |a::t -> a::(in_situ t)
                in Array.append (Array.of_list pre_sts) (Array.of_list (in_situ pre_iks))


        in let replace2 ik pre_iks pre_sts ik1 ik2 =
                let rec in_situ = function
                        |[] -> raise (Invalid_argument "in replace in on_each_rule in process.ml")
                        |a::t when (a = ik) -> ik1::(ik2::t)
                        |a::t -> a::(in_situ t)
                in Array.append (Array.of_list pre_sts) (Array.of_list (in_situ pre_iks))

        in let rec get_vars_term = function
                | At(x) when is_variable x -> [At(x)]
                | At(_) -> []
                | Omega -> []
                | Pub(x) -> get_vars_term x
                | Vk (x) -> get_vars_term x
                | Hash (x)  -> get_vars_term x
                | Senc(x,y) -> List.rev_append (get_vars_term x) (get_vars_term y)
                | Pair(x,y) -> List.rev_append (get_vars_term x) (get_vars_term y)
                | Aenc(x,y) -> List.rev_append (get_vars_term x) (get_vars_term y)
                | Sign(x,y) -> List.rev_append (get_vars_term x) (get_vars_term y)
        in let get_vars_ik_q = function
                | Ik(_,uq) -> get_vars_term uq
                | _ -> raise (Invalid_argument "get_vars_ik in process.ml")
        in let rec add_vars_list tg = function
                | [] -> tg
                | u::l -> add_vars_list (add_vars tg u) l
        in let gives_asym_lab1 vars1 = function
                | Ptcl(r_id,c,n,ta,tb) -> Asym(r_id,c,n,ta, add_vars_list tb vars1 )
                | Asym(r_id,c,n,ta,tb) -> Asym(r_id,c,n,ta, add_vars_list tb vars1 )
                | _ -> raise (Invalid_argument "in gives_asym_lab in on_each_rule in process.ml")
        in let gives_asym_lab2 vars1 vars2 = function
                | Ptcl(r_id,c,n,ta,tb) -> Asym(r_id,c,n,ta, add_vars_list (add_vars_list tb vars1) vars2 )
                | Asym(r_id,c,n,ta,tb) -> Asym(r_id,c,n,ta, add_vars_list (add_vars_list tb vars1) vars2 )
                | _ -> raise (Invalid_argument "in gives_asym_lab in on_each_rule in process.ml")
        in let for_deconstructible1 get_pos (b0,garde,ik,ik1,substP,substQ) fresh_id =
               let new_pre = replace1 ik pre_iks pre_sts ik1 in
               let vars1 = get_vars_ik_q ik1 in 
               let n_get_pos = Position.update get_pos ik ik1 None in 
               
               let change_id rid = function
                       |Ptcl(_,c,n,ta,tb) -> Ptcl (rid,c,n,ta,tb)
                       |Asym(_,c,n,ta,tb) -> Asym (rid,c,n,ta,tb)
                       | _ -> raise (Invalid_argument "in change_id in on_each_rule in process.ml")
               in let quick_map = rule_map_old (substP,substQ) 
               in match b0 with
               |true  -> garde, (quick_map ((change_id fresh_id lab,new_pre,add,del)))  , n_get_pos  
               |false -> garde, (quick_map ((change_id fresh_id (gives_asym_lab1 vars1 lab),new_pre,[|Bad|],[||]))) , n_get_pos 
        in let for_deconstructible2 get_pos (b0,garde,ik,ik1,ik2,substP,substQ) fresh_id =
               let new_pre = replace2 ik pre_iks pre_sts ik1 ik2 in
               let vars1 = get_vars_ik_q ik1 in 
               let vars2 = get_vars_ik_q ik2 in 
               let n_get_pos = Position.update get_pos ik ik1 (Some ik2) in 
               
               let change_id rid = function
                       |Ptcl(_,c,n,ta,tb) -> Ptcl (rid,c,n,ta,tb)
                       |Asym(_,c,n,ta,tb) -> Asym (rid,c,n,ta,tb)
                       | _ -> raise (Invalid_argument "in change_id in on_each_rule in process.ml")
               in let quick_map = rule_map_old (substP,substQ) 
               in match b0 with
               |true  -> garde, (quick_map ((change_id fresh_id lab,new_pre,add,del)))  , n_get_pos  
               |false -> garde, (quick_map ((change_id fresh_id (gives_asym_lab2 vars1 vars2 lab),new_pre,[|Bad|],[||]))) , n_get_pos 
        in let for_each_deconstructible  get_pos  (b0,garde,ik,ik1,ik2,substP,substQ) fresh_id =
                match ik2 with
                | None    -> for_deconstructible1 get_pos (b0,garde,ik,ik1,substP,substQ) fresh_id
                | Some(x) -> for_deconstructible2 get_pos (b0,garde,ik,ik1,x,substP,substQ) fresh_id
        in let rec on_all_deconstructibles get_pos fresh_id = function
                | [] -> true,[], get_pos,  fresh_id
                | a::t -> 
                                let (garde,new_r ,new_get_pos  ) = for_each_deconstructible get_pos a fresh_id in
                                let garde_final,res,final_get_pos, nfid = on_all_deconstructibles new_get_pos (fresh_id+1) t in
                                (garde && garde_final),new_r::res, final_get_pos,  nfid
        in let deconstr,fv = get_list_deconstructibles fresh_var pre_iks
        in let garde,list_of_rules, get_pos, nfid = on_all_deconstructibles get_position fresh_id deconstr in
        garde,list_of_rules, get_pos, fv,nfid
;; 

let decompose (r:rule) fresh_var fresh_id =
        (* les premières fonctions qui trient les positions
         * devraient pouvoir être utilisées en amont
         * ce qui permettrait d'éviter d'effectuer plusieurs fois le tri. *)
        let rec get_iks = function
                | [] -> []
                | (State (_,_,_,_))::l -> get_iks l
                | (Ik (xp,xq))::l -> (Ik (xp,xq)):: get_iks l
                | _ -> raise (Invalid_argument "in get_iks in decompose in process.ml")


        in let sorted_pos (get_pos:Position.positionneur) r1 = 
                let (_,pre1,_,_) = r1 in
                let ik1 = get_iks (Array.to_list pre1) in
                Position.sort (List.map get_pos ik1)

        in let update_position_list get_pos get_pos_list r0 =
                let sorted1 = sorted_pos get_pos r0 in
                let new_get_position_list = function
                        | r when r = r0 -> sorted1
                        | r -> get_pos_list r
                in new_get_position_list

        in let belongs (r0:rule) list_of_rules get_pos get_position_list =
                let pos1 = sorted_pos get_pos r0
                in let rec aux = function
                | []   -> false
                | a::_ when (get_position_list a) = pos1 -> true
                | _::t -> aux t
                in aux list_of_rules
        in 
        let rec main list_of_rules result_list fv fid (get_pos:Position.positionneur) get_pos_list =
                match list_of_rules with
                | [] -> result_list,fv,fid,get_pos
                | r0::t when belongs r0 result_list get_pos get_pos_list -> main t result_list fv fid get_pos get_pos_list
                | r0::t ->
                                let garde,new_list_of_rules, new_get_pos, new_fv,new_fid = on_each_rule r0 fv fid get_pos in
                                let new_get_pos_list = update_position_list new_get_pos get_pos_list r0 in
                                let new_result_list =
                                        match garde with
                                        | true  -> r0::result_list
                                        | false -> result_list
                                in main (List.rev_append new_list_of_rules t) new_result_list new_fv new_fid new_get_pos new_get_pos_list
        in let get_position = 
                let (_,pre,_,_) = r in
                let iks_r = get_iks (Array.to_list pre) in
                let ik = 
                        match iks_r with
                        | [ik] -> ik
                        | _ -> raise (Invalid_argument "Decomposition must be used on inputs only in get_position in decompose in process.ml")
                in function
                | ik0 when ik0 = ik -> Some([])
                | _ -> None
        in let get_position_list = fun _ -> raise (Invalid_argument "get_position_list in flattening.ml")
        in let result,new_fv,new_fid,_ = main [r] [] fresh_var fresh_id get_position get_position_list
        in result,new_fv,new_fid
;;

