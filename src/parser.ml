open Terms ;;
open Process ;;

exception Parse_error of string ;;

type char_type = Chiffre | Lettre | Espace | Delimiteur | Autre ;;

(* délimiteurs : les caractères qui sont utilisés dans la syntaxe comme ( ) , . : 
 *)

let get_char_type c = 
        match Char.code c with
        |  9 -> Espace
        | 10 -> Espace
        | 32 -> Espace
        | 37 -> Autre (* % - pour mémoire *)
        | 40 -> Delimiteur (* ( *)
        | 41 -> Delimiteur (* ) *)
        | 44 -> Delimiteur (* , *)
        | 46 -> Delimiteur (* . *)
        | 58 -> Delimiteur (* : *)
        | 59 -> Delimiteur (* ; *)
        | n when (n < 48) -> Autre
        | n when (n < 58) -> Chiffre
        | n when (n < 65) -> Autre
        | n when (n < 91) -> Lettre (* Majuscules *)
        | n when (n < 97) -> Autre
        | n when (n < 123) -> Lettre (* Minuscules *)
        | n when (n < 192) -> Autre
        | _ -> Lettre (* Lettres accentuées *)
;;

let list_of_symbols str =
                (* états :
                        * 0 : état de départ : rien n'est lu
                        * 1 : lecture d'un mot ; commence par une lettre puis enchaînement de lettres et de chiffres.
                        * 2 : lecture d'un nombre ; on vient de lire un chiffre.
                 *)
        let rec word_reader list_of_words start current state =
                let len = (current-start) in
                try
                let c = str.[current] in
                match get_char_type c,state with
                | Espace,0 -> word_reader list_of_words (current+1) (current+1) 0
                | Espace,_ -> word_reader ((String.sub str start len)::list_of_words) (current+1) (current+1) 0
                | Lettre,0 -> word_reader list_of_words start (current+1) 1
                | Lettre,1 -> word_reader list_of_words start (current+1) 1
                | Chiffre,1 -> word_reader list_of_words start (current+1) 1
                | Chiffre,_ -> word_reader list_of_words start (current+1) 2
                | Delimiteur,0 ->  word_reader ((Char.escaped c)::list_of_words) (current+1) (current+1) 0
                | Delimiteur,_ ->  word_reader ((Char.escaped c)::((String.sub str start len)::list_of_words)) (current+1) (current+1) 0
                | _ -> raise (Parse_error (String.concat " " ["Unexpected character" ; (Char.escaped c)]))
                with
                | Invalid_argument _ (* "index out of bounds" *) when state = 0 -> list_of_words
                | Invalid_argument _ (* "index out of bounds" *) -> (String.sub str start len)::list_of_words
        in List.rev (word_reader [] 0 0 0)
;;

let general_parse ic =
        let rec decoupe ic (constantes_publiques,constantes_privees,processus_P,processus_Q) etape =
                try
                begin
                let str = input_line ic in
                if str = "" 
                then decoupe ic (constantes_publiques,constantes_privees,processus_P,processus_Q) etape
                else match str.[0] with
                | '%' -> decoupe ic (constantes_publiques,constantes_privees,processus_P,processus_Q) etape
                | '_' -> decoupe ic (constantes_publiques,constantes_privees,processus_P,processus_Q) (etape+1)
                | _ when etape = 0 -> decoupe ic (constantes_publiques,constantes_privees,processus_P,processus_Q) 0
                | _ when etape = 1 -> decoupe ic (str::constantes_publiques,constantes_privees,processus_P,processus_Q) 1
                | _ when etape = 2 -> decoupe ic (constantes_publiques,str::constantes_privees,processus_P,processus_Q) 2
                | _ when etape = 3 -> decoupe ic (constantes_publiques,constantes_privees,str::processus_P,processus_Q) 3
                | _ when etape = 4 -> decoupe ic (constantes_publiques,constantes_privees,processus_P,str::processus_Q) 4
                | _ (* when etape > 4 *) -> (constantes_publiques,constantes_privees,processus_P,processus_Q)
                end
                with
                | End_of_file when etape = 4 -> (constantes_publiques,constantes_privees,processus_P,processus_Q)
        in try 
                let liste_constantes_publiques,liste_constantes_privees,liste_processus_P,liste_processus_Q = decoupe ic ([],[],[],[]) 0
                in let to_line x = String.concat " " (List.rev x)
                in (to_line liste_constantes_publiques, to_line liste_constantes_privees, to_line liste_processus_P, to_line liste_processus_Q)
        with
        |End_of_file -> raise (Parse_error "missing field (public constant,private constant,P,Q)")
;;

(* En supposant que nous avons bien effectué le découpage : *)
let is_a_word   str = get_char_type (str.[0]) = Lettre ;;
let is_a_number str = get_char_type (str.[0]) = Chiffre ;;

let remove_symbol symb = function
        | a::t when a=symb -> t
        | _ -> raise (Parse_error (String.concat " " ["missing expected symbol";symb]))
;;

let rec parse_type = function
        | "senc"::l0 -> 
                        let tp1,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        let tp2,l2 = parse_type ( remove_symbol "," l1 ) in
                        Senc(tp1,tp2),remove_symbol ")" l2
        | "aenc"::l0 -> 
                        let tp1,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        let tp2,l2 = parse_type ( remove_symbol "," l1 ) in
                        Aenc(tp1,tp2),remove_symbol ")" l2
        | "sign"::l0 -> 
                        let tp1,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        let tp2,l2 = parse_type ( remove_symbol "," l1 ) in
                        Sign(tp1,tp2),remove_symbol ")" l2
        | "pair"::l0 ->
                        let tp1,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        let tp2,l2 = parse_type ( remove_symbol "," l1 ) in
                        Pair(tp1,tp2),remove_symbol ")" l2
        | "("::l0 -> 
                        let tp1,l1 = parse_type l0 in 
                        let tp2,l2 = parse_type ( remove_symbol "," l1 ) in
                        Pair(tp1,tp2),remove_symbol ")" l2
        | "h"::l0 -> 
                        let tp,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        Hash(tp),remove_symbol ")" l1
        | "hash"::l0 -> 
                        let tp,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        Hash(tp),remove_symbol ")" l1
        | "pub"::l0 -> 
                        let tp,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        Pub(tp),remove_symbol ")" l1
        | "vk"::l0 -> 
                        let tp,l1 = parse_type ( remove_symbol "(" l0 ) in 
                        Vk(tp),remove_symbol ")" l1
        | w::l0 when is_a_word w -> At(Atomic_type w),l0
        | w::_ -> raise (Parse_error (String.concat " " [w ; "is not allowed as a type"] ))
        | [] -> raise (Parse_error "No type to parse")
;;

let rec parse_typed_name_basic = function
        | "("::l0 -> let n,t,l1 = parse_typed_name_basic l0 in n,t,remove_symbol ")" l1
        | w::(":"::l0) -> let t,l1 = parse_type l0 in (w,t,l1)
        | _ -> raise (Parse_error "bad typed term format (name:type)")
;;


exception Undefined ;;

let parse_private_constant constant_line =
  try
  let symbols = list_of_symbols constant_line in
  let rec list_of_constants = function
    | [] -> []
    | l0 -> 
        let n,t,l1 = parse_typed_name_basic l0 in 
        (n,t)::(list_of_constants l1)
  in let typed_constants = list_of_constants symbols
  in let modify_assignation priv_list f symbol n tp =
    let new_f = function
      | s when s = symbol -> (nce_from_tp n tp,nce n)
      | s -> f s
    in let new_list = (nce_from_tp n tp)::priv_list
    in
      try let _ = f symbol in 
      raise (Parse_error (String.concat " " 
      ["Private constant";symbol;"has been defined several times."])) 
    with
    | Undefined -> new_list,new_f,(n+1)

  in let rec define_constants = function
    | [] -> [],(fun _ -> raise Undefined),0
    | (s,tp)::t -> 
      let priv_list,f,n = define_constants t 
      in modify_assignation priv_list f s n tp
  in define_constants typed_constants

  with
  Parse_error str -> 
    raise (Parse_error (String.concat " in " [str;"private constants"]))
;;

let parse_public_constant constant_line fresh_nce =
        try
        let symbols = list_of_symbols constant_line in
        let rec list_of_constants = function
                | [] -> []
                | l0 -> let n,t,l1 = parse_typed_name_basic l0 in (n,t)::(list_of_constants l1)
        in let typed_constants = list_of_constants symbols
        in let modify_assignation ik f symbol n tp =
                let new_f = function
                        | s when s = symbol -> (nce_from_tp n tp,nce n)
                        | s -> f s
                in try let _ = f symbol in f,ik,n
                with
                | Undefined -> new_f, (Multiset.Ik (nce_from_tp n tp,nce n))::ik,(n+1)

        in let rec define_constants initial_knowledge = function
                | [] -> (fun _ -> raise Undefined),initial_knowledge,fresh_nce
                | (s,tp)::t -> let f,ik,n = define_constants initial_knowledge t in modify_assignation ik f s n tp

        in define_constants [] typed_constants

        with
        Parse_error str -> raise (Parse_error (String.concat " in " [str;"public constants"]))
;;

let parse_number str =
        let len = String.length str in
        let val_of_char n = (Char.code (str.[n])) - 48 in
        let rec compute value = function
                | n when n=len -> value
                | n -> let c = val_of_char n in compute (10*value + c) (n+1)
        in compute 0 0
;;

let is_defined f x =
        try let _ = f x in true with
        |  Undefined -> false
;;

let get_bounded_constant publics privates bounded = function
        | w when is_defined publics  w -> let a,_ = publics w in a
        | w when is_defined privates w -> let a,_ = privates w in a
        | w when is_defined bounded  w -> bounded w
        | _ -> raise Undefined
;;

let rec parse_typed_name assym initial_knowledge priv 
                         bounded publics privates fresh_var = 
  let vus x ik =
    let prepare = function
      | x when is_defined publics x -> 
        let a,b = publics x in  Multiset.Ik(a,b)  
      | _ -> raise Undefined
    in let rec change y = function
      | [] -> []
      | (_,a)::l when a = y -> (true,y)::l
      | a::l -> a::(change y l)
    in try let y = prepare x in change y ik
    with
    | Undefined -> ik
  in let vus_priv x prv =
    let prepare = function
      | x when is_defined privates x ->
        let a,_ = privates x in a
      | _ -> raise Undefined
    in let rec change y = function
      | [] -> []
      | (_,a)::l when a = y -> (true,y)::l
      | a::l -> a::(change y l)
    in try let y = prepare x in change y prv
    with
    | Undefined -> prv

  in
  function
  | "("::l0 -> let as1,t,l1,new_b,ik,prv,fv = 
    parse_typed_name assym initial_knowledge priv 
                     bounded publics privates fresh_var l0 
               in as1,t,remove_symbol ")" l1,new_b,ik,prv,fv
  | w::(":"::l0) ->
     begin
     try
       let _ = get_bounded_constant publics privates bounded w in
       raise (Parse_error (String.concat " " ["Name";w;"is bounded twice"] ))
     with
     | Undefined -> let t,l1 = parse_type l0 in
                    let new_var = yt fresh_var t in
                    let new_bounded = function
                    | s when s = w -> new_var
                    | s -> bounded s
                    in assym,new_var,l1,new_bounded,
                    initial_knowledge,priv,(fresh_var+1)
     end
  | w::l0 ->
    begin 
    try 
    let t = get_bounded_constant publics privates bounded w 
    in assym,t,l0,bounded,vus w initial_knowledge,vus_priv w priv,
       fresh_var  
    with
    | Undefined -> 
        raise 
        (Parse_error 
        (String.concat " " 
        ["Name";w;"is not defined and has no type"]))
    end
  | _ -> raise (Parse_error "Empty list: cannot parse a typed name")
;;


let rec parse_one_process assym initial_knowledge priv 
                          bounded publics privates fresh_var = 
  function
  | "0"::l0 -> Null,assym,bounded,initial_knowledge,priv,fresh_var,l0
  | "in"::l0 -> 
    let as1,c,u,l1,ik1,priv1,bounded1,fresh_var1 = 
      parse_inside assym initial_knowledge priv 
      bounded publics privates fresh_var (remove_symbol "(" l0) 
    in let next_p,as2,new_bounded,ik2,priv2,new_fresh_var,l2 = 
      dot_expected as1 ik1 priv1
      bounded1 publics privates fresh_var1 (remove_symbol ")" l1) 
    in In(c,u, next_p),as2,new_bounded,ik2,priv2,new_fresh_var,l2
  | "out"::l0 -> 
    let as1,c,u,l1,ik1,priv1,bounded1,fresh_var1 = 
      parse_inside assym initial_knowledge priv
      bounded publics privates fresh_var (remove_symbol "(" l0) in
    let next_p,as2,new_bounded,ik2,priv2,new_fresh_var,l2 = 
      dot_expected as1 ik1 priv1 
      bounded1 publics privates fresh_var1 (remove_symbol ")" l1) 
    in Out(c,u, next_p),as2,new_bounded,ik2,priv2,new_fresh_var,l2
  | _ -> raise (Parse_error "Processes should only use in, out and 0")
and dot_expected assym initial_knowledge priv
    bounded publics privates fresh_var = function
  | "."::l0 -> parse_one_process assym initial_knowledge priv 
  bounded publics privates fresh_var l0
  | ";"::l0 -> Null,assym,bounded,initial_knowledge,priv,fresh_var,l0
  | _ -> raise (Parse_error "Instructions of the processes should be separated by dots and terminated either by .0 or ;")
and parse_inside assym initial_knowledge priv 
    bounded publics privates fresh_var = function
  | n::(","::l0) when (is_a_number n ) -> 
    let as1,t,l1,new_bounded,ik1,priv1,new_fresh_var = 
      parse_term assym initial_knowledge priv 
                 bounded publics privates fresh_var l0 
    in as1,parse_number n, t, l1,ik1,priv1,new_bounded,new_fresh_var
  | _ -> raise 
  (Parse_error 
  "inputs and outputs should be of the form in/out(INT,term)")

and parse_term assym initial_knowledge priv 
               bounded publics privates fresh_var = function
  | "senc"::l0 -> let as1,t1,l1,b1,ik1,priv1,fv1 = 
    parse_term assym initial_knowledge priv 
    bounded publics privates fresh_var ( remove_symbol "(" l0 ) in
    let as2,t2,l2,b2,ik2,priv2,fv2 = 
      parse_term as1 ik1 priv1 b1 
      publics privates fv1 (remove_symbol "," l1 ) 
    in as2,Senc(t1,t2),remove_symbol ")" l2,b2,ik2,priv2,fv2
  | "pair"::l0 -> let as1,t1,l1,b1,ik1,priv1,fv1 = 
    parse_term assym initial_knowledge priv 
    bounded publics privates fresh_var ( remove_symbol "(" l0 ) in
    let as2,t2,l2,b2,ik2,priv2,fv2 = 
      parse_term as1 ik1 priv1 
      b1 publics privates fv1 (remove_symbol "," l1 ) 
    in as2,Pair(t1,t2),remove_symbol ")" l2,b2,ik2,priv2,fv2
  | "("::l0    -> let as1,t1,l1,b1,ik1,priv1,fv1 = 
    parse_term assym initial_knowledge priv 
    bounded publics privates fresh_var l0 
    in let as2,t2,l2,b2,ik2,priv2,fv2 = 
      parse_term as1 ik1 priv1 
      b1 publics privates fv1 (remove_symbol "," l1 ) 
    in as2,Pair(t1,t2),remove_symbol ")" l2,b2,ik2,priv2,fv2
  | "aenc"::l0 -> let _,t1,l1,b1,ik1,priv1,fv1 = 
      parse_term true  initial_knowledge priv 
      bounded publics privates fresh_var ( remove_symbol "(" l0 ) in
      let _,t2,l2,b2,ik2,priv2,fv2 = 
        parse_term true  ik1 priv1 
        b1 publics privates fv1 (remove_symbol "," l1 ) 
      in true,Aenc(t1,t2),remove_symbol ")" l2,b2,ik2,priv2,fv2
  | "sign"::l0 -> let _,t1,l1,b1,ik1,priv1,fv1 = 
      parse_term true  initial_knowledge priv bounded publics privates fresh_var ( remove_symbol "(" l0 ) in
      let _,t2,l2,b2,ik2,priv2,fv2 = 
        parse_term true ik1 priv1 b1 
        publics privates fv1 (remove_symbol "," l1 ) 
      in true,Sign(t1,t2),remove_symbol ")" l2,b2,ik2,priv2,fv2
  | "h"::l0 -> let _,t1,l1,b1,ik1,priv1,fv1 = 
      parse_term true initial_knowledge priv 
      bounded publics privates fresh_var ( remove_symbol "(" l0 ) 
    in true,Hash(t1),remove_symbol ")" l1,b1,ik1,priv1,fv1
  | "hash"::l0 -> let _,t1,l1,b1,ik1,priv1,fv1 = 
      parse_term true initial_knowledge priv
      bounded publics privates fresh_var ( remove_symbol "(" l0 ) 
    in true,Hash(t1),remove_symbol ")" l1,b1,ik1,priv1,fv1
  | "pub"::l0  -> let _,t1,l1,b1,ik1,priv1,fv1 = 
      parse_term true initial_knowledge priv 
      bounded publics privates fresh_var ( remove_symbol "(" l0 ) 
    in true,Pub(t1),remove_symbol ")" l1,b1,ik1,priv1,fv1
  | "vk"::l0   -> let _,t1,l1,b1,ik1,priv1,fv1 = 
      parse_term true initial_knowledge priv 
      bounded publics privates fresh_var ( remove_symbol "(" l0 ) 
    in true,Vk(t1),remove_symbol ")" l1,b1,ik1,priv1,fv1
  | l0 -> parse_typed_name assym initial_knowledge priv 
                           bounded publics privates fresh_var l0
;;

let parse_protocol initial_knowledge priv 
                   publics privates processes_line fresh_var 
                   =
  let symbols = list_of_symbols processes_line in
  let initial_bounded = fun _ -> raise Undefined in
  let rec principal assym ik priv fresh_var = function
    | [] -> assym,[],ik,priv,fresh_var
    | l0 -> 
        let p1,as1,_,ik1,priv1,fv1,l1 = 
          parse_one_process assym ik priv initial_bounded 
                            publics privates fresh_var l0 
        in let as2,res,ik2,priv2,fv2 = principal as1 ik1 priv1 fv1 l1 
        in as2,p1::res,ik2,priv2,fv2
  in principal false initial_knowledge priv fresh_var symbols
;;

exception Not_simple of int ;;

let clean_and_simple protocolP =
        (* Vérifie que le protocole est simple et enlève les Null *)
        let rec check_one l c = function
                | Null -> not(List.mem c l)
                | p  when get_channel p = c -> check_one l c (get_next p)
                | _ -> false

        in let rec check res l = function
                | [] -> res
                | Null::others -> check res l others
                | p::others when check_one l (get_channel p) (get_next p) -> check (p::res) ((get_channel p)::l) others
                | p::_ -> raise (Not_simple (get_channel p))
        in check [] [] protocolP
;;


let to_biprotocols protocolP protocolQ =
        let p = try clean_and_simple protocolP
                   with
                   | Not_simple n -> raise (Parse_error (String.concat " " ["Protocol P is not simple due to channel";string_of_int n]))
        in let q = try clean_and_simple (List.map untype_process protocolQ)
                   with
                   | Not_simple n -> raise (Parse_error (String.concat " " ["Protocol Q is not simple due to channel";string_of_int n]))
        in let rec find n = function
                | [] -> Null
                | a::_ when get_channel a = n -> a
                | _::t -> find n t
        in let rec to_biprocesses = function
                | [] -> []
                | p0::l -> let n = get_channel p0 in (p0,find n q)::(to_biprocesses l)
        in to_biprocesses p
;;

let complete_parser withOmega ic =
  let ligne_publiques,ligne_privees,ligne_P,ligne_Q = 
    general_parse ic 
  in let priv_list,priv,fresh_priv = 
    parse_private_constant ligne_privees 

  in let pub,potential_initial_knowledge,_ = 
    parse_public_constant ligne_publiques fresh_priv
  in let modified_initial_knowledge = 
    List.map (fun x -> (false,x)) potential_initial_knowledge
  in let modified_priv_list =
    List.map (fun x -> (false,x)) priv_list
  in let as1,protocolP,initial_knowledge_after_P,priv_listP,freshP = 
    try parse_protocol modified_initial_knowledge modified_priv_list 
                       pub priv ligne_P 0 
  with Parse_error str -> 
    raise (Parse_error (String.concat " in " [str;"parse protocol P"]))
  in let as2,protocolQ,initial_knowledge_after_Q,priv_listQ,freshQ = 
    try parse_protocol initial_knowledge_after_P priv_listP
                       pub priv ligne_Q freshP 
  with Parse_error str -> 
    raise (Parse_error (String.concat " in " [str;"parse protocol Q"]))
        
  in
  let rec efface_inutiles = function
    | [] -> []
    | (true,x)::l -> x::(efface_inutiles l)
    | (false,_)::l -> efface_inutiles l 
  in let initial_knowledge = 
    match withOmega with
    | true  -> 
    (Multiset.Ik(Omega,Omega))::
      (efface_inutiles initial_knowledge_after_Q)
    | false -> efface_inutiles initial_knowledge_after_Q
  in let actual_priv = efface_inutiles priv_listQ
  in as1||as2,actual_priv,initial_knowledge, to_biprotocols protocolP protocolQ, to_biprotocols protocolQ protocolP, freshQ
;;  
