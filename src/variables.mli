exception Variable_bound_in_output

val variables_state_typed :
   Terms.typed_term array ->
   Process.typed_process ->
   Terms.typed_term array * Terms.typed_term array

val variables_state_untyped :
  Atoms.atom Terms.term array ->
  Atoms.atom Process.process ->
  Atoms.atom Terms.term array * Atoms.atom Terms.term array
