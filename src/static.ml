open Atoms ;;
open Terms ;;
open Multiset ;;
open Process ;;


(* Règles pour l'équivalence statique. *)
let static_sym = 
        [
                Adv(5,[|x 0|],[|x 1|]),
                [|Ik(yt 0 (At(Cameleon)),x 1) ; Ik(yt 0 (At(Cameleon)),x 1)|],
                [||],
                [||]
        ]
;;
let static_rules =
        let new_rule =
                (
                Adv(4,[|x 0;x 1|],[|x 2;x 3|]),
                [|Ik(Sign(yt 0 (At(Cameleon)), At(Var( 1 ) ,At(Cameleon))),Sign(x 2,x 3));Ik(Vk(At(Var(1),At(Cameleon))),Vk(x 3))|],
                [||],
                [||]
                )
        in new_rule::static_sym
;;

let types liste =

  let rec insert a = function
    | [] -> [a]
    | x::t when a = x -> x::t
    | x::t -> x::(insert a t)
  in
  let rec signed_subterms l = function
    | Sign(t1,t2) -> signed_subterms (signed_subterms (insert t1 l) t2) t1
    | Pair(t1,t2) -> signed_subterms (signed_subterms l t1) t2
    | Senc(t1,t2) -> signed_subterms (signed_subterms l t1) t2
    | Aenc(t1,t2) -> signed_subterms (signed_subterms l t1) t2
    (* Inutile de descendre en dessous du hash
     * car il n'est pas inversible. *)
    | Hash(_)     -> l
    | Pub(t)      -> signed_subterms l t
    | Vk(t)       -> signed_subterms l t
    | Omega       -> l
    | At(_)       -> l
  in
  let rec flat l = function
    | Pair(t1,t2) -> flat (flat l t1) t2
    | Sign(t1,t2) -> insert (Sign(t1,t2)) l
    | Senc(t1,t2) -> flat (flat l t1) t2
    | Aenc(t1,t2) -> insert (Aenc(t1,t2)) l 
    | Hash(t1)    -> insert (Hash(t1)) l
    | Pub(t1)     -> insert (Pub(t1))  l
    | Vk(t1)      -> insert (Vk(t1))   l
    | Omega       -> l
    | At(_)       -> l
  in
  let rec types_process res = function
    | In(_,_,next) -> types_process res next
    | Out(_,t,next) -> 
        let tp_t = get_type t in
        let res1 = flat res tp_t in
        let signed = signed_subterms [] tp_t in
        let res2 = List.fold_left flat res1 signed in
        types_process res2 next
    | Null -> res

  in let rec aux res = function
    | [] -> res
    | (p,_)::t -> aux (types_process res p) t
  in aux [] liste
;;

let simplify liste =
  let rec insert a = function
    | [] -> [a]
    | x::t when x = a -> x::t
    | x::t -> x::(insert a t)
  in let rec all = function
    | [] -> []
    | a::t -> insert a (all t)
  in all liste
;;


let rec add_symbol1 t l =
        (* Seul le symbole de tête de t compte.
         * Il sert à repérer le symbole qu'on doit ajouter. *)
        match t,l with
        | _,[] -> []
        | Hash(_),(x1,x2,atoms,vars)::l -> 
            ((Hash x1), (Hash x2),atoms,vars)::(add_symbol1 t l)
        | Pub(_),(x1,x2,atoms,vars)::l -> 
            ((Pub x1), (Pub x2),atoms,vars)::(add_symbol1 t l)
        | Vk (_),(x1,x2,atoms,vars)::l -> 
            ((Vk  x1), (Vk  x2),atoms,vars)::(add_symbol1 t l)
        | _ -> raise (Invalid_argument "should be used with arity one function symbols")
;;

let add_symbol2 t l1 l2 =
        let rec etape1 res t0 (x1,x2,atoms,vars) l2 =
             match t0,l2 with
             | _,[] -> res
             | Pair(_,_),(y1,y2,a,v)::l -> 
                let natoms = List.rev_append a atoms in
                let nvars  = List.rev_append v vars  in
                etape1 ((Pair(x1,y1),Pair(x2,y2),natoms,nvars)::res) 
                t0 (x1,x2,atoms,vars) l

             | Senc(_,_),(y1,y2,a,v)::l -> 
                let natoms = List.rev_append a atoms in
                let nvars  = List.rev_append v vars  in
                etape1 ((Senc(x1,y1),Senc(x2,y2),natoms,nvars)::res) 
                t0 (x1,x2,atoms,vars) l

             | Aenc(_,_),(y1,y2,a,v)::l -> 
                let natoms = List.rev_append a atoms in
                let nvars  = List.rev_append v vars  in
                etape1 ((Aenc(x1,y1),Aenc(x2,y2),natoms,nvars)::res) 
                t0 (x1,x2,atoms,vars) l

             | Sign(_,_),(y1,y2,a,v)::l -> 
                let natoms = List.rev_append a atoms in
                let nvars  = List.rev_append v vars  in
                etape1 ((Sign(x1,y1),Sign(x2,y2),natoms,nvars)::res) 
                t0 (x1,x2,atoms,vars) l

             | _ -> 
                raise 
                (Invalid_argument 
                "should be used with arity two function symbols")

        in let rec etape2 res t0 l1 l2 =
                match l1 with
                | [] -> res
                | x::l -> etape2 (etape1 res t0 x l2) t0 l l2
        in etape2 [] t l1 l2
;;
                
let for_one_item ruleNumber t1 t2 atoms vars =
        let rec create_ik_list = function
                | ([],[]) -> []
                | x::lx,y::ly -> (Ik(x,y))::(create_ik_list (lx,ly))
                | _ -> raise (Invalid_argument "The length of the lists differ")
        in let pre = Array.of_list (Ik(t1,t2) :: create_ik_list (atoms,vars))
        in ((Adv(ruleNumber,[||],Array.of_list vars),pre,[||],[||]):rule)
;;

let create_items t =
    let rec aux fv = function
      | At(t) -> [((yt fv (At(t))):typed_term),x (fv+1),[yt fv (At(t))],[x (fv+1)]],fv+2
      | Omega -> raise (Invalid_argument "Omega is not a type." )
      | Hash(t)-> 
          let l,nfv = aux fv t in
          let new_l = add_symbol1 (Hash t) l in
          (((Hash(yt nfv t)):typed_term),x (nfv+1),[Hash(yt nfv t)],[x (nfv+1)])::new_l,
          nfv+1

      | Pub(t)-> 
          let l,nfv = aux fv t in
          let new_l = add_symbol1 (Pub t) l in
          (((Pub(yt nfv t)):typed_term),x (nfv+1),[Pub(yt nfv t)],[x (nfv+1)])::new_l,
          nfv+1

      | Vk(t) ->  
          let l,nfv = aux fv t in
          let new_l = add_symbol1 (Vk t) l in
          (Vk (yt nfv t),x (nfv+1),[Vk (yt nfv t)],[x (nfv+1)])::new_l,
          nfv+1

      | Senc(t1,t2) ->  
          let l1,fv1 = aux fv  t1 in
          let l2,fv2 = aux fv1 t2 in
          let new_l = add_symbol2 (Senc (t1,t2)) l1 l2 in
          (Senc(yt fv2 t1,yt (fv2+1) t2),x (fv2+2),
          [Senc(yt fv2 t1,yt (fv2+1) t2)],[x (fv2+2)])::new_l,fv2+3

      | Aenc(t1,t2) ->  
          let l1,fv1 = aux fv  t1 in
          let l2,fv2 = aux fv1 t2 in
          let new_l = add_symbol2 (Aenc (t1,t2)) l1 l2 in
          (Aenc(yt fv2 t1,yt (fv2+1) t2),x (fv2+2),
          [Aenc(yt fv2 t1,yt (fv2+1) t2)],[x (fv2+2)])::new_l,fv2+3

      | Pair(t1,t2) ->  
          let l1,fv1 = aux fv  t1 in
          let l2,fv2 = aux fv1 t2 in
          let new_l = add_symbol2 (Pair (t1,t2)) l1 l2 in
          (Pair(yt fv2 t1,yt (fv2+1) t2),x (fv2+2),
          [Pair(yt fv2 t1,yt (fv2+1) t2)],[x (fv2+2)])::new_l,fv2+3

      | Sign(t1,t2) ->  
          let l1,fv1 = aux fv  t1 in
          let l2,fv2 = aux fv1 t2 in
          let new_l = add_symbol2 (Sign (t1,t2)) l1 l2 in
          (Sign(yt fv2 t1,yt (fv2+1) t2),x (fv2+2),
          [Sign(yt fv2 t1,yt (fv2+1) t2)],[x (fv2+2)])::new_l,fv2+3

      in aux 0 t
 (* on aurait pu éliminer une partie de ces règles *)
;;

let all_static_rules assym proto =
  let terms = simplify (types proto)
  in let rec on_all_items res ruleNumber = function
    | [] -> res,ruleNumber
    | (x1,x2,atoms,vars)::t -> 
       on_all_items 
       ((for_one_item ruleNumber x1 x2 atoms vars)::res) 
       (ruleNumber + 1) t
  in let rec aux res ruleNumber = function
    |[] -> res
    |t0::l -> 
    let items,_  = create_items t0 in 
    let res1,rN1 = on_all_items res ruleNumber items in
        aux res1 rN1 l
  in match assym with
    |false -> static_sym
    |true  -> aux static_rules 6 terms
;;


