type ('a, 'b) general_fact =
    Ik of 'a Terms.term * 'b Terms.term
  | State of int * int * 'a Terms.term array * 'b Terms.term array
  | Bad
type fact = (Atoms.atom * Terms.complete_type, Atoms.atom) general_fact
type untyped_fact = (Atoms.atom, Atoms.atom) general_fact

val print_fact :  fact -> unit
val print_fact_list : fact list -> unit

type rule_label =
    Adv of int * Atoms.atom Terms.term array * Atoms.atom Terms.term array
  | Failing_adv of int * Atoms.atom Terms.term array * fact array
  | Ptcl of int * int * int * Atoms.atom Terms.term array *
      Atoms.atom Terms.term array
  | Failing_ptcl of int * int * int * Atoms.atom Terms.term array *
      fact array * fact array
  | Asym of int * int * int * Atoms.atom Terms.term array *
      Atoms.atom Terms.term array
  | No_op of fact

val print_rule_label : rule_label -> unit
val print_labels_list : rule_label list -> unit

type ('a, 'b) general_rule =
    rule_label * ('a, 'b) general_fact array * ('a, 'b) general_fact array *
    ('a, 'b) general_fact array

type untyped_rule = (Atoms.atom, Atoms.atom) general_rule
type rule = rule_label * fact array * fact array * fact array

val print_rule : rule -> unit 

val print_rule_list : rule list -> unit

val rule_map_old :
  (Atoms.atom * Terms.complete_type) Substitutions.substitution *
  Atoms.atom Substitutions.substitution ->
  rule -> rule

val pm_lab :
  rule_label ->
  rule_label ->
  bool * Atoms.atom Substitutions.substitution *
  Atoms.atom Substitutions.substitution

val ground_reachable_rules :
  fact list -> rule list -> rule list
