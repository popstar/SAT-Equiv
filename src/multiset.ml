open Atoms ;;
open Terms ;;
open Substitutions ;;

        (* Facts and rules *)

type ('a,'b) general_fact = Ik of ('a term)* ('b term) | State of int*int*('a term array)*('b term array) | Bad ;;

type fact = ((atom * complete_type ),atom) general_fact ;;

type untyped_fact = (atom,atom) general_fact ;;

(* State(x,y,AList) : 
        * x is the number of the channel.
        * y is the number of the operation on the channel.
        * atom list is the set of bounded variables or there value.
*)

let print_fact = function
        | Ik(t1,t2) -> begin print_string "Ik(" ; print_typed_term t1 ; print_string " ; " ; print_term t2 ; print_string ")" end
        | State(n,m,ta,tb) -> 
                        begin 
                                print_string "State(" ; 
                                print_int n ; 
                                print_string ";" ; 
                                print_int m ; 
                                print_string ";" ; 
                                Array.iter (fun t -> begin print_typed_term t ; print_string ". "  end) ta ; 
                                print_string ";" ; 
                                Array.iter (fun t -> begin print_term t ; print_string ". "  end) tb ; 
                                print_string ")" 
                        end
        | Bad -> print_string "Bad"
;;

let print_fact_list = List.iter print_fact ;;


(* The rule label or action is used to identify the rule. 
 * It should contain all free variables of the rule. 
 * The labels are as follows:
         * Adv(Symbolic rule number , VARS_P, VARS_Q )
         * Failing_adv(Symbolic rule number , VARS, precondition )
         * Ptcl(Rule id,Channel number, Step number, VARS_P, VARS_Q )
         * Failing_ptcl(Rule id, Channel number, Step number, VARS_P, PRE, ADD )
         * Asym(Rule id,c,n,VARS_P,VARS_Q)
         * No_op(fact)
 *)

type rule_label = 
        Adv of int*(atom term array)*(atom term array) 
        | Failing_adv of int*(atom term array)*(fact array)
        | Ptcl of int*int*int*(atom term array)*(atom term array) 
        | Failing_ptcl of int*int*int*(atom term array)*(fact array)*(fact array)
        | Asym of int*int*int*(atom term array)*(atom term array) 
        | No_op of fact 
;;

let print_rule_label = 
        let print_term_array = Array.iter (fun t -> begin print_term t ; print_string ". " end ) in 
        let print_fact_array = Array.iter (fun f -> begin print_fact f ; print_string ". " end ) in
        function
        | Adv(n,t1,t2)     -> 
                        begin  
                                print_string "Adv("  ; 
                                print_int n ; 
                                print_string ";" ; 
                                print_term_array t1 ; 
                                print_string ";" ; 
                                print_term_array t2 ; 
                                print_string ")" 
                        end
        | Failing_adv(n,t1,pre) -> 
                        begin 
                                print_string "Failing_adv(" ; 
                                print_int n ; 
                                print_string ";" ; 
                                print_term_array t1 ; 
                                print_string ";" ; 
                                print_fact_array pre ; 
                                print_string ")" 
                        end
        | Ptcl(_,c,sn,t1,t2) -> 
                        begin  
                                print_string "Ptcl(" ; 
                                print_int c ; 
                                print_string ";" ;
                                print_int sn ; 
                                print_string ";" ;  
                                print_term_array t1 ; 
                                print_string ";" ;  
                                print_term_array t2 ; 
                                print_string ")" 
                        end
        | Failing_ptcl(_,c,sn,t1,pre,add) -> 
                        begin 
                                print_string "Failing_ptcl(" ; 
                                print_int c ; 
                                print_string ";" ; 
                                print_int sn ; 
                                print_string ";" ; 
                                print_term_array t1 ; 
                                print_string ";" ; 
                                print_fact_array pre ; 
                                print_string ";" ; 
                                print_fact_array add ; 
                                print_string ")" 
                        end
        | Asym(_,c,sn,t1,t2) -> 
                        begin  
                                print_string "Asym(" ; 
                                print_int c ; 
                                print_string ";" ;
                                print_int sn ; 
                                print_string ";" ;  
                                print_term_array t1 ; 
                                print_string ";" ;  
                                print_term_array t2 ; 
                                print_string ")" 
                        end
        | No_op(f)  -> begin print_string "No op(" ; print_fact f ; print_string ")" end 
;;

let print_labels_list = List.iter print_rule_label ;;

type ('a,'b) general_rule = rule_label * ( ('a,'b) general_fact array ) * ( ('a,'b) general_fact array ) * ( ('a,'b) general_fact array );;

type untyped_rule = (atom,atom) general_rule ;;
type rule = rule_label * (fact array) * (fact array) * (fact array);;

(* (T,x,y,z):
        * T: type of rule
        * x : the pre part
        * y : the add part
        * z : the del part
*)

let get_label r = let (lab,_,_,_)=r in lab ;;

let print_rule = function
        (t,pre,add,del) -> let print_array = Array.iter ( fun f -> print_fact f ; print_string ". " )
                            in print_string "Rule :" ; print_rule_label t ; 
                               print_string " [|" ; print_array pre ; print_string "|] " ;
                               print_string " [|" ; print_array add ; print_string "|] " ;
                               print_string " [|" ; print_array del ; print_string "|] "
;;

let print_rule_list = List.iter print_rule ;;

(* Pattern matching *)
        
        (* Pattern matching of facts *)

        (* This function does the pattern matching for a pair of arrays *)

let typed_coordinate_pm tag tap =
        let on_each_pair = function
                | (false, _ ) -> (fun _ -> false,Substitutions.make() )
                | (true, sigma0)   -> (fun (gnd,ptn) -> 
                                                let b,s =  typed_pattern_matching_terms gnd (typed_term_map sigma0 ptn) in
                                                if b then true, merge sigma0 s else false,s )
        in
                let lgt = Array.length tag 
                in
                        if lgt = Array.length tap 
                        then
                                let f = fun i -> (tag.(i),tap.(i)) in
                                let pairs = Array.init lgt f in
                                Array.fold_left on_each_pair (true,Substitutions.make ()) pairs

                        else false, (Substitutions.make ())
;;

let untyped_coordinate_pm tag tap =
        let on_each_pair = function
                | (false, _ ) -> (fun _ -> false,Substitutions.make() )
                | (true, sigma0)   -> (fun (gnd,ptn) -> 
                                                let b,s =  untyped_pattern_matching_terms gnd (untyped_term_map sigma0 ptn) in
                                                if b then true, merge sigma0 s else false,s )
        in
                let lgt = Array.length tag 
                in
                        if lgt = Array.length tap 
                        then
                                let f = fun i -> (tag.(i),tap.(i)) in
                                let pairs = Array.init lgt f in
                                Array.fold_left on_each_pair (true,Substitutions.make ()) pairs

                        else false, (Substitutions.make ())
;;

        (* Je laisse les résultats séparés entre la gauche et la droite
         * car les différences de résultats peuvent être utilisées
         * pour déclarer un problème d'équivalence.
         * En fait, on devrait filtrer à droite chaque fois que l'on filtre à gauche.
         * *)
let pattern_matching_fact ground pattern =
        match ground,pattern with
        | Ik(tgG,tgD),Ik(tpG,tpD) when (is_atomic tgG && not( is_atomic tgD) ) -> 
                        let _,sigmaD = untyped_pattern_matching_terms tgD tpD in
                        typed_pattern_matching_terms tgG tpG , (false,sigmaD)
        | Ik(tgG,tgD),Ik(tpG,tpD) -> typed_pattern_matching_terms tgG tpG , untyped_pattern_matching_terms tgD tpD 
        | State(ng,mg,tagG,tagD),State(np,mp,tapG,tapD) when ng = np && mg = mp -> 
                        typed_coordinate_pm tagG tapG , untyped_coordinate_pm tagD tapD 
        | Bad,Bad -> (true, Substitutions.make ()),(true, Substitutions.make ())
        | _ -> (false, Substitutions.make ()), (false, Substitutions.make ())
;;

 (* Next step : Does a pattern unify with a list of ground facts ? *)

        (* A modifier quand on voudra ajouter Bad chaque fois que c'est nécessaire. *)

let pm_list groundlist pattern =
  let rec aux (resb,resl) = function
    | [] -> resb, resl
    | f::lt -> 
            begin 
            let (mG,sigmaG),(mD,sigmaD) = pattern_matching_fact f pattern in
            match mG with
            | true   -> aux (true, (mD,sigmaG,sigmaD,f)::resl) lt
            | false  -> aux (resb,resl) lt
            end
  in aux (false,[]) groundlist
;;

 (* Instanciation of a fact *)

let fact_map (sigmaG,sigmaD) = function
        | Ik(tG,tD) -> Ik(typed_term_map sigmaG tG,untyped_term_map sigmaD tD)
        | State(n,m,tG,tD) -> State(n,m, Array.map (fun t -> typed_term_map sigmaG t) tG , Array.map (fun t -> untyped_term_map sigmaD t) tD)
        | Bad -> Bad
;;

(* Label instanciation *)

let label_map (sigmaG,sigmaD) = 
        let untyped_sigmaG = untype_subst sigmaG

          (*
        let untyped_sigmaG x =
               match get_substitution sigmaG x with
               |None -> None
               |Some(x) -> Some(untype x) 
*)
        in
        let array_terms_mapG = Array.map (fun u -> untyped_term_map untyped_sigmaG u) in
        let array_terms_mapD = Array.map (fun u -> untyped_term_map sigmaD u) in
        let fact_array_map = Array.map (fact_map (sigmaG,sigmaD)) in
        function
                | Adv (n,varsG,varsD) -> Adv (n, array_terms_mapG varsG, array_terms_mapD varsD)
                | Failing_adv(n,varsG,pre) -> Failing_adv(n, array_terms_mapG varsG, fact_array_map pre)
                | Ptcl(rid,c,sn,varsG,varsD) -> Ptcl(rid,c,sn, array_terms_mapG varsG, array_terms_mapD varsD)
                | Failing_ptcl(rid,c,sn,varsG,pre,add) -> Failing_ptcl(rid,c,sn, array_terms_mapG varsG, fact_array_map pre,fact_array_map add)
                | Asym(rid,c,sn,varsG,varsD) -> Asym(rid,c,sn, array_terms_mapG varsG, array_terms_mapD varsD)
                | No_op(f) -> No_op( fact_map (sigmaG,sigmaD) f)
;;

(* Rule instanciation *)

let rule_map_old sigma = function 
    (lab,pre,add,del) -> 
      let array_map = Array.map (fact_map sigma) 
      in ( label_map sigma lab, array_map pre , 
           array_map add , array_map del )
;; 

let rule_map (mD,sigmaG,sigmaD,int_to_fact) =
  let transform pre = 
    let res = Array.of_list (List.rev int_to_fact)
    in match Array.length pre = Array.length res with
    | true -> res
    | false -> raise (Invalid_argument "in rule_map.")

  in let array_map =
    Array.map (fact_map (sigmaG,sigmaD))

  in let failing_version = function
    |(Adv(n,tp,_),pre,_,_) -> 
      let new_pre = transform pre 
      in (Failing_adv(n,tp,new_pre),new_pre,[|Bad|],[||])
    |(Ptcl(rid,c,sn,tp,_),pre,add,_) -> 
      let new_pre = transform pre 
      in (Failing_ptcl(rid,c,sn,tp,new_pre,add),new_pre,[|Bad|],[||])
    |(Asym(rid,c,sn,varsG,_),pre,_,_) -> 
      let new_pre = transform pre 
      in (Failing_ptcl(rid,c,sn,varsG,new_pre,[||]),new_pre,[|Bad|],[||])
    | _ -> raise (Invalid_argument "in failing_version in rule_map in multiset.ml")

  in let check_atomic = function
    | Ik(tG,tD) -> (has_atomic_keys tG),(has_atomic_keys tD)
    | _ -> true,true
  in let has_atomic_keys_array arr =
    let on_each_fact (atomicG,atomicD) f = 
      let aG,aD = check_atomic f 
      in atomicG&&aG,atomicD&&aD
    in Array.fold_left on_each_fact (true,true) arr

  in function 
  (lab,pre,add,del) -> 
    let new_add = array_map add in
    let atomicG,atomicD = has_atomic_keys_array new_add 
    in match mD&&atomicD with
    | true  -> atomicG,( label_map (sigmaG,sigmaD) lab, array_map pre , new_add , array_map del )
    | false -> atomicG,failing_version ( label_map (sigmaG,sigmaD) lab, pre, new_add, [||])

;; 

let rule_map_list r l_sigma =
  let rec get_result_list res = function
    | [] -> res
    | a::t -> get_result_list ((rule_map a r)::res) t
        
  in let result_list = get_result_list [] l_sigma 
                                
  in let rec clean res = function
        | [] -> res
        | (true, a)::l -> clean (a::res) l
        | (false,_)::l -> clean res l
  in let clean_list = clean [] result_list
  in let rec mem_rule r =
    let lab = get_label r in function
    | [] -> false
    | a::_ when (get_label a = lab ) -> true
    | _::t -> mem_rule r t
  in let rec simplify l0 = function
    | [] -> l0
    | a::t when mem_rule a t -> simplify l0 t
    | a::t -> simplify (a::l0) t
  in simplify [] clean_list
;;


(* pm_pre groundlist pre : 
 * gives a pair of :
        * a boolean : true iff lf may satisfy pre
        * a list of all possible instanciations of variables of pre
        * that allow to realize pre from the list of facts lf. 
 *)


let pm_pre groundlist pre =

  let flatten_subst_list_list list_list =
    let f = function
      | true,l -> (fun (b,ll) -> if b then true, List.append l ll else true,l )
      | false,_ -> (fun (b,ll) -> b,ll)
    in List.fold_left f (true,[]) list_list 

  in let for_each_fact_and_subst f (mD,sG,sD,int_to_fact) =
    (* 1 - Instanciate
       2 - For each instanciation, see if it matches with a fact of groundlist
           and get the list of all possible such instanciation
       3 - For each such list, merge each substitution with the previous one *)

    let fct = fact_map (sG,sD) f in                                          (* 1 *)
    let matches,sigma_res = pm_list groundlist fct in                        (* 2 *) 
      match matches with
      | true  -> true, 
              List.map 
              (fun (matchD,sigmaG,sigmaD,f0) ->
                (mD&&matchD,merge sG sigmaG,merge sD sigmaD,f0::int_to_fact)
              ) 
              (sigma_res)                                                    (* 3 *)
    (* before using merge, we have instanciated the pattern (step 1)
     * and we have defined sigma as a new possible instanciation.
     * So merging is legal.
     *)
     | false -> false,[] 

  in let on_each_fact = function
     | false,_ -> (fun _ -> false,[])
     | true,lsubst -> 
       (fun f -> 
       let matches,next = 
        flatten_subst_list_list (List.map  (for_each_fact_and_subst f) lsubst) 
        in matches,next )

  in Array.fold_left on_each_fact (true,[true,Substitutions.make(),Substitutions.make(),[]]) pre
;;

(* fonction de filtrage utile pour retrouver la règle abstraite d'origine : *)
let pm_lab ground pattern =
        let double_coordinate_pm tag tbg tap tbp =
                let b0,sigma_a = untyped_coordinate_pm tag tap in
                match b0 with
                | true -> let b1,sigma_b = untyped_coordinate_pm tbg tbp in b1,sigma_a,sigma_b 
                | false -> false,Substitutions.make(),Substitutions.make()

        in match ground,pattern with
        |Adv(ng,tag,tbg),Adv(np,tap,tbp)   when ng = np -> double_coordinate_pm tag tbg tap tbp
        |Ptcl(ridg,cg,sng,tag,tbg),Ptcl(ridp,cp,snp,tap,tbp) when (ridg = ridp && cg=cp && sng = snp) -> double_coordinate_pm tag tbg tap tbp
        |Asym(ridg,cg,sng,tag,tbg),Asym(ridp,cp,snp,tap,tbp) when (ridg = ridp && cg=cp && sng = snp) -> double_coordinate_pm tag tbg tap tbp
        |No_op(_),_  -> raise (Invalid_argument "At this point, No_op rules should have been removed in pm_lab in multiset.ml")
        |_, No_op(_) -> raise (Invalid_argument "There should be no no_op symbolic rule in our_rules")
        |Failing_adv(_,_,_),_ -> raise (Invalid_argument "Failing_adv rules have no corresponding symbolic rules in pm_lab in multiset.ml")
        | _ , Failing_adv(_,_,_) -> raise (Invalid_argument "There should be no symbolic rule Failing_adv in our_rules")
        |Failing_ptcl(_,_,_,_,_,_),_ -> raise (Invalid_argument "Failing_ptcl rules have no corresponding symbolic rules in pm_lab in multiset.ml")
        | _ , Failing_ptcl(_,_,_,_,_,_) -> raise (Invalid_argument "There should be no symbolic rule Failing_ptcl in our_rules")
        |_ -> false,Substitutions.make(),Substitutions.make()
;;


let ground_reachable_rules list_of_facts list_of_rules =
        let ground_versions r =
                let (_,pre,_,_) = r in
                let is_reachable,how_reachable = 
                  pm_pre list_of_facts pre 
                in is_reachable, rule_map_list r how_reachable
        in let rec aux l0 = 
          function
                | [] -> l0
                | r::t ->  
                    let is_r,h_r = 
                      ground_versions r 
                    in 
                    if is_r then aux (List.rev_append h_r l0) t 
                    else aux l0 t

        in aux [] list_of_rules
;;
