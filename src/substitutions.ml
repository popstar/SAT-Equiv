open Atoms ;;
open Terms ;;

(* Substitutions *)

type 'a substitution = ((int,'a term) Hashtbl.t) list ;;

(* 
 * Untyped substitution = atom substitution
 * Typed substitution = (atom*complete_type) substitution = int -> typed_term
 *)

exception Substitution_not_defined ;;

let make () = []
;; 

let untype_subst (sigma:('a * ('b term)) substitution) =
  let on_table t =
    let res = Hashtbl.create (Hashtbl.length t) in
    let aux n b = 
      Hashtbl.add res n (untype b)
    in Hashtbl.iter aux t ; res
  in List.map on_table sigma
;;

let add_value (sigma:'a substitution) (n,(t:'a term)) =
  match sigma with
  | [] -> 
      let nouv = Hashtbl.create 7 in
      begin
        Hashtbl.add nouv n t ;
        [nouv]
      end
  | a::l -> Hashtbl.add a n t ; a::l
;;

let get_substitution (sigma: 'a substitution) n =
  let rec aux = function
    | [] -> None
    | a::t ->
        try Some(Hashtbl.find a n) with
        | Not_found -> aux t
  in aux sigma
;;

let apply (sigma: 'a substitution) n =
  match get_substitution (sigma: 'a substitution) n with
  | Some(x) -> x
  | None -> raise Substitution_not_defined
;;

let merge (sigma1:'a substitution) (sigma2:'a substitution) =
  (* Je suppose que sigma2 est la plus petite des deux *)
  List.rev_append sigma2 sigma1
;;

(*
let copy_merge (sigma1:'a substitution) (sigma2:'a substitution) =
  (* Je suppose que sigma2 est la plus nouvelle des deux *)
  let merging_help a b =
    try 
      match (Hashtbl.find sigma2 a) = b with
      | true  -> ()
      | false -> raise  (Invalid_argument "function merge in substitutions.ml ")
    with
      |Not_found -> Hashtbl.add sigma2 a b
  in Hashtbl.iter merging_help sigma1 ; sigma2
;;
*)

(* Instantiation *)

        (* Variable instanciation *)

exception Bad_type ;;

let typed_atom_map sigma = function
  | Var(n),tn -> 
    begin
    match get_substitution sigma n with
    |None -> At(Var(n),tn)
    |Some(t0) -> if (type_checks (get_type t0) tn) then t0 else raise Bad_type
    end
  | x -> At(x)
;;

let untyping_atom_map sigma = function
  | Var(n),_ -> untype (apply sigma n)
  | x -> untype (At x)
;;

let untyped_atom_map sigma = function
  | Var(n) -> 
    begin
    match get_substitution sigma n with
    |None -> At(Var(n))
    |Some(x) -> x
    end
  | x -> At(x)
;;

let untyping_atom_map_var sigma = function
  |x,_ -> untyped_atom_map sigma x
;;

        (* Term Instanciation *)

let term_map at_map sigma =
  let rec main = function
  | At(u) -> at_map sigma u
  | Omega -> Omega
  | Pub(a) -> Pub(main a)
  | Vk (a) -> Vk (main a)
  | Hash (a)  -> Hash(main a)
  | Aenc(a,b) -> Aenc( main a , main b)
  | Sign(a,b) -> Sign( main a , main b)
  | Senc(a,b) -> Senc( main a , main b)
  | Pair(a,b) -> Pair( main a , main b)
  in main
;;

let    typed_term_map sigma  = term_map (typed_atom_map)    sigma  ;;
let untyping_term_map sigma  = term_map (untyping_atom_map) sigma  ;;
let untyped_term_map         = term_map (untyped_atom_map) ;;
let untyping_term_map_var sigma  = term_map (untyping_atom_map_var) sigma  ;;


(* Pattern matching *)


     (* These pattern matching functions send a pair b,sigma
      * where b is a boolean set to true iff the term matches the pattern
      * and sigma is a substitution realizing the pattern matching.
      *)

let typed_pattern_matching_atoms ground pattern =
  match (ground,pattern) with
  | At(Nonce(x),_),(Nonce(y),_) when x=y -> true, (make ())     (* assuming that a nonce    has a unique type *)
  | At(Cst(x),_),(Cst(y),_)     when x=y -> true, (make ())     (* assuming that a constant has a unique type *)
  | x,(Var(y),ty) when (type_checks (get_type x) ty) -> let sigma = make () in true,(add_value sigma (y,x))
  | _ -> false, (make())
;;

let untyped_pattern_matching_atoms ground pattern =
  match (ground,pattern) with
  | At(Nonce x),(Nonce y) when x=y -> true, (make ())
  | At(Cst x),  (Cst y)   when x=y -> true, (make ())
  | x,(Var y) -> let sigma = make () in true,add_value sigma (y,x)
  | _ -> false,(make ())
;;

(* Pattern matching function : *)

let pattern_matching_terms pm_atoms at_map ground pattern =
  let rec on_atoms sigma0 l (x,y) =
    let new_y = at_map sigma0 y in
    match new_y = At(y) with
    | true  -> let bx,sigma1 = pm_atoms x y in 
               if bx 
               then acc (merge sigma0 sigma1) l
               (* Up to the test new_y = At(y),
                * no conflict between sigma0 and sigma1 is possible
                * so the Invalid_argument exception of merge won't be raised.
                * *)
               else false,make ()
    | false -> aux sigma0 l (x,new_y) 
  and acc sigma0 = function
    | a::l -> aux sigma0 l a
    | [] -> true,sigma0
  and aux sigma0 l = function
    | x,At(y) -> on_atoms sigma0 l (x,y)
    | Omega,Omega -> acc sigma0 l
    | Pub(a1),Pub(a2) -> aux sigma0 l (a1,a2)
    | Vk (a1),Vk (a2) -> aux sigma0 l (a1,a2)
    | Hash (a1),Hash (a2) -> aux sigma0 l (a1,a2)
    | Aenc(a1,b1),Aenc(a2,b2) -> acc sigma0 ((a1,a2)::(b1,b2)::l) 
    | Sign(a1,b1),Sign(a2,b2) -> acc sigma0 ((a1,a2)::(b1,b2)::l) 
    | Senc(a1,b1),Senc(a2,b2) -> acc sigma0 ((a1,a2)::(b1,b2)::l) 
    | Pair(a1,b1),Pair(a2,b2) -> acc sigma0 ((a1,a2)::(b1,b2)::l) 
    | _ -> false,make ()
  in match (has_atomic_keys ground) with
    | true -> aux (make ()) [] (ground,pattern)
    | false -> false,make ()
;;

let untyped_pattern_matching_terms = pattern_matching_terms untyped_pattern_matching_atoms untyped_atom_map ;;
let typed_pattern_matching_terms gnd ptn  = pattern_matching_terms (typed_pattern_matching_atoms) (typed_atom_map) gnd ptn  ;;
