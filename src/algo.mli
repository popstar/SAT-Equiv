val algo:
  int -> string -> string ->
  Multiset.fact list -> Multiset.rule list ->  
  Multiset.rule list -> bool
