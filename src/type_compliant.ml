open Atoms;;
open Terms;;
open Process;;

let rec get_encrypted_subterms res = function
        | Pair(x,y) -> (get_encrypted_subterms  (get_encrypted_subterms res x)  y)
        | Senc(x,y) -> (Senc(x,y))::(get_encrypted_subterms (get_encrypted_subterms res x)  y)
        | Aenc(x,y) -> (Aenc(x,y))::(get_encrypted_subterms (get_encrypted_subterms res x)  y)
        | Sign(x,y) -> (Sign(x,y))::(get_encrypted_subterms (get_encrypted_subterms res x)  y)
        | Hash(x) -> (Hash(x))::(get_encrypted_subterms res x)
        | Pub(x) -> (Pub(x))::(get_encrypted_subterms res x)
        | Vk (x) -> (Vk (x))::(get_encrypted_subterms res x)
        | At(_)|Omega -> res
;;

let rec all_encrypted_process res = function
        | Null -> res
        | In(_,t,p)  -> (all_encrypted_process (get_encrypted_subterms res t) p)
        | Out(_,t,p) -> (all_encrypted_process (get_encrypted_subterms res t) p)
;;

let all_encrypted_biprotocol pq =
        let rec aux res = function
                | [] -> res
                | (p,_)::l -> (aux (all_encrypted_process res p)  l)
        in aux [] pq
;;

let rec check_unifiable_terms t1 t2 =
        match t1,t2 with
        |At(x,_),_ when is_variable x -> true
        |_,At(x,_) when is_variable x -> true
        |At(x,_),At(y,_) when x=y -> true
        |Omega,_ | _,Omega -> raise (Invalid_argument "Omega should not occur in a protocol specification in type_compliant.ml" )
        |Pair(x1,x2),Pair(y1,y2) -> check_unifiable_terms x1 y1 && check_unifiable_terms x2 y2
        |Senc(x1,x2),Senc(y1,y2) -> check_unifiable_terms x1 y1 && check_unifiable_terms x2 y2
        |Aenc(x1,x2),Aenc(y1,y2) -> check_unifiable_terms x1 y1 && check_unifiable_terms x2 y2
        |Sign(x1,x2),Sign(y1,y2) -> check_unifiable_terms x1 y1 && check_unifiable_terms x2 y2
        |Hash(x),Hash(y) -> check_unifiable_terms x y
        |Pub(x),Pub(y)   -> check_unifiable_terms x y
        |Vk(x), Vk(y)    -> check_unifiable_terms x y
        | _ -> false
;;


let check_compliance_terms t1 t2 =
        let unif = check_unifiable_terms t1 t2 in
        match unif with
        | false -> true
        | true -> type_checks (get_type t1) (get_type t2)
;;

let check_compliance_biprotocol p =
        let encrypted = all_encrypted_biprotocol p in
        let rec compare_un t = function
                | [] -> true
                | a::l -> (check_compliance_terms t a)&&(compare_un t l)
        in let rec compare_tous = function
                | [] -> true
                | a::t -> (compare_un a t)&&(compare_tous t)
        in compare_tous encrypted
;;
