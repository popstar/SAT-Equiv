open Planning_graph ;;
open Dimacs ;;
open Read_solution ;;
open Translation ;; 
open Parser ;; 
open Type_compliant;;
open Read_plan ;;

let file = "__planning" ;;
let file_sat = String.concat "." [file ; "dimacs"];;
let file_out = String.concat "." [file ; "out"];;
let no_attack_sat = "No attack has been found after SAT compilation.\n"
let no_attack_pg  = "No attack has been found after planning graph construction:\n Bad is not reachable in the planning graph.\n"

exception NoPlan ;;

let create_dimacs pg our_facts our_goal final_step node_to_num size = 
        let oc = open_out file_sat in
        let n = try get_fact_number pg our_goal with Undef("F",_) -> raise NoPlan
        in
        let nb_vars,nb_clauses = explore_and_output oc pg node_to_num size final_step our_facts n in
        begin
                print_graph pg ;
                close_out oc ;
                nb_vars,nb_clauses
        end
;;

let launch_minisat () =
        let instr = String.concat " " ["minisat" ; file_sat ; file_out ; "> /dev/null" ] in
        let _ = Unix.system instr in ()
;;

let print_result nb_vars nb_clauses pg num_to_node size = 
        let print_sat () = 
                let ic = open_in file_out in
                let nlist = node_list pg num_to_node size (read_output ic) in
                begin
                print_string "Node list:\n";
                print_node_list nlist ;
                close_in ic ;
                print_string "\nNumber of variables: " ;
                print_int nb_vars ;
                print_string "\nNumber of clauses: " ;
                print_int nb_clauses ;
                print_string "\n"
                end
        in print_sat ()
;;

let launch_lingeling () =
        let instr = String.concat " " ["./lingeling" ; file_sat ; ">" ; file_out ] in
        let _ = Unix.system instr in ()
;;

exception Contradiction_between_arguments ;;
exception Without_file ;;

let main () =
        let argument_reader =
                let equiv       = ref true  in
                let pq          = ref true in (* true : p , false : q - inutile si !equiv = true *)
                let efface      = ref true in
                let compliance_check = ref "yes" in
                let constants = ref None in (* None : (c0,c1,Omega), Some(true) : (c0,Omega), Some(false): (c0,c1) *)
                let spec_list   =
                        [
                        ("--inclusionP" , Arg.Unit (fun () -> equiv := false; pq := true ) , "Tests only the inclusion of P in Q");
                        ("-p"           , Arg.Unit (fun () -> equiv := false; pq := true ) , "Same as --inclusionP");
                        ("--inclusionQ" , Arg.Unit (fun () -> equiv := false; pq := false ) , "Tests only the inclusion of Q in P");
                        ("-q"           , Arg.Unit (fun () -> equiv := false; pq := false ) , "Same as --inclusionQ");
                        ("--equivalence", Arg.Unit (fun () -> equiv := true ) , "Tests the complete equivalence (default)");
                        ("-e"           , Arg.Unit (fun () -> equiv := true ) , "Same as --equivalence");
                        ("--remove"     , Arg.Unit (fun () -> () ) , "Removes the files used by the SAT solver (default)");
                        ("-r"           , Arg.Unit (fun () -> () ) , "Same as --remove");
                        ("--keep"       , Arg.Unit (fun () -> efface := false ) , "Keeps the files used by the SAT solver");
                        ("-k"           , Arg.Unit (fun () -> efface := false ) , "Same as --keep");
                        ("--compliance"     , Arg.Unit (fun () -> () ) , "Checks the compliance and print a message if the protocol is not type-compliant (default)");
                        ("-c"               , Arg.Unit (fun () -> () ) , "Same as --compliance");
                        ("--compliance-only", Arg.Unit (fun () -> compliance_check := "only" ) , "Only checks compliance");
                        ("-o"               , Arg.Unit (fun () -> compliance_check := "only" ) , "Same as --compliance-only");
                        ("--no-compliance"  , Arg.Unit (fun () -> compliance_check := "none" ) , "Does not check compliance");
                        ("-n"               , Arg.Unit (fun () -> compliance_check := "none" ) , "Same as --no-compliance");
                        ("--three-constants", Arg.Unit (fun () -> constants := None ), "Uses three constants (c0,c1,Omega) (default)" );
                        ("-w", Arg.Unit (fun () -> constants := Some(true) ), "Uses only one constant plus Omega (c0,Omega)" );
                        (* w comme omega *)
                        ("-t", Arg.Unit (fun () -> constants := Some(false) ), "Uses only two constants (c0,c1)" )
                        (* t comme typed *)
                        ]
                in let usage_msg = "This is a tool for verification of cryptographic protocols.\n\nUsage: ./satequiv file [options] \n\n Options available:\n"
                in Arg.parse spec_list print_endline usage_msg; (!equiv,!pq,!efface,!compliance_check,!constants)

        in let instructions assym twoConstants launcher printer p_include_q ik_parse fresh_var =
                let our_facts,our_rules = get_all_rules twoConstants p_include_q ik_parse fresh_var in
                let (final_step,pg)     = planning_graph_until_end assym our_facts our_rules in
                let nb_noeuds = get_bound pg in
                begin
                print_string "\n Number of nodes: ";
                print_int nb_noeuds ;
                print_string "\n Number of steps: ";
                print_int final_step ;
                print_string "\n\n" ;
                let size,node_to_num,num_to_node = sizes pg (final_step+1) in
                let nb_vars,nb_clauses = create_dimacs pg our_facts Multiset.Bad final_step node_to_num size in
                        begin
                        launcher () ;
                        printer nb_vars nb_clauses pg num_to_node size 
                        end
                end

        in let when_equiv assym twoConstants compliance_check launcher printer p_include_q q_include_p ik_parse fresh_var =
                print_string "P include Q:\n" ;
                begin
                if compliance_check && not (check_compliance_biprotocol p_include_q) 
                then print_string "Warning: P is not type compliant!\n"
                end ;
                try instructions assym twoConstants launcher printer p_include_q ik_parse fresh_var with
                | NoPlan ->
                                print_string no_attack_pg ; 
                                print_string "Q include P:\n" ; 
                                begin
                                if compliance_check && not (check_compliance_biprotocol q_include_p) 
                                then print_string "Warning: Q is not type compliant!\n"
                                end ;
                                begin
                                try instructions assym twoConstants launcher printer q_include_p ik_parse fresh_var
                                with
                                | NoPlan -> print_string no_attack_pg
                                | Unsat  -> print_string no_attack_sat
                                end
                | Unsat  -> 
                                print_string no_attack_sat ; 
                                print_string "Q include P:\n" ;
                                begin
                                try
                                instructions assym twoConstants launcher printer q_include_p ik_parse fresh_var
                                with
                                | NoPlan -> print_string no_attack_pg
                                | Unsat  -> print_string no_attack_sat
                                end
        in let when_include assym twoConstants pq compliance_check launcher printer p_include_q ik_parse fresh_var =
                
                let warningMessage,reminder = match pq with
                | true  -> "Warning: P is not type compliant!\n","P include Q:\n"
                | false -> "Warning: Q is not type compliant!\n","Q include P:\n"
                
                in print_string reminder ;
                begin
                if compliance_check && not (check_compliance_biprotocol p_include_q) 
                then print_string warningMessage
                end ;

                try instructions assym twoConstants launcher printer p_include_q ik_parse fresh_var with
                | NoPlan -> print_string no_attack_pg
                | Unsat  -> print_string no_attack_sat

        in let get_solver minisat lingeling =
                match minisat,lingeling with
                | true,false -> (launch_minisat  , print_result    )
                | false,true -> (launch_lingeling, fun _ _ _ _ _ -> () )
                | _ -> raise Contradiction_between_arguments

        in let (equiv,pq,efface,compliance_check,constants) = argument_reader
        in let (launcher, printer )  = get_solver true false
        in let ic_parse = 
                try open_in (Sys.argv.(1))
                with
                | Invalid_argument("index out of bounds") -> raise Without_file
        in let assym,ik_parse,p_include_q,q_include_p,fresh_var = complete_parser (not (constants = Some(false))) ic_parse
        in let inclusionProcess = 
                match pq with
                | true -> p_include_q
                | false -> q_include_p
        in begin match equiv,compliance_check with
        | true,"only"  -> 
                        let compliantp = check_compliance_biprotocol p_include_q in
                        let compliantq = check_compliance_biprotocol q_include_p in
                        begin
                                match compliantp,compliantq with
                                | true,true -> print_string "P and Q are type compliant.\n"
                                | true,false -> print_string "P is type compliant but Q is not (!).\n"
                                | false,true -> print_string "P is not type compliant (!) but Q is.\n"
                                | _ -> print_string "Neither P nor Q is type compliant !!\n"
                        end
        | false,"only" ->
                        let compliantp = check_compliance_biprotocol inclusionProcess in
                        begin
                                match compliantp,pq with
                                | true,true  -> print_string "P is type compliant.\n"
                                | true,false -> print_string "Q is type compliant.\n"
                                | false,true  -> print_string "P is not type compliant !\n"
                                | false,false -> print_string "Q is not type compliant !\n"
                        end
        | true, "none" -> when_equiv assym (not (constants = Some(true))) false launcher printer p_include_q q_include_p ik_parse fresh_var
        | true, "yes"  -> when_equiv assym (not (constants = Some(true))) true  launcher printer p_include_q q_include_p ik_parse fresh_var
        | false,"none" -> when_include assym (not (constants = Some(true))) pq false launcher printer inclusionProcess ik_parse fresh_var
        | false,"yes"  -> when_include assym (not (constants = Some(true))) pq true  launcher printer inclusionProcess ik_parse fresh_var
        | _ -> raise (Invalid_argument "impossible")
        end ;

        if efface then let _ = Unix.system (String.concat " " ["rm -f" ; file_sat ; file_out ] ) in ()
;;

try main () with
        | Without_file -> print_string "You should indicate a file or type ./satequiv --help for more informations.\n"  
