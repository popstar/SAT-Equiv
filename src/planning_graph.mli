type node = Rule of Multiset.rule_label | Fact of Multiset.fact | Undefined
type node_sort = R | F | U
val get_sort : node -> node_sort

exception Undef of string * string

type graph

(*  int * (Multiset.fact -> int) * (Multiset.rule_label -> int) *
    (int -> node * int list * int list * int list * int) * Mutex.mutex
*)

(* fonctions utiles pour obtenir des informations
 * sur les graphes 
 *)

val get_bound : graph -> int

val get_fact_number : graph -> Multiset.fact -> int

val get_rule_number : graph -> Multiset.rule_label -> int

val get_mutex : graph -> Mutex.mutex
val get_node : graph -> int -> node
val get_pre : graph -> int -> int list
val get_add : graph -> int -> int list
val get_del : graph -> int -> int list
val get_activation : graph -> int -> int


(* Fonctions d'affichage.
 * Utiles pour trouver les erreurs
 *)

val print_facts : graph -> unit 
val print_rules : graph -> unit
val print_node  : node  -> unit    
val print_graph : graph -> unit

(* Fonctions utilisées (exclusivement)
 * dans algo.ml
 *)

val get_fact_list :
  graph -> Multiset.fact list

val add_rules :
  graph -> Multiset.rule list -> int -> graph * (Multiset.fact list)

val add_no_op :
  graph -> Multiset.fact list -> graph 

val propagate_mutex :
  graph -> int -> graph

val build_initial_graph :
  Multiset.fact list -> graph

