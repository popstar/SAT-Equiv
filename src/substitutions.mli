type 'a substitution

exception Substitution_not_defined

val make : unit -> 'a substitution
val untype_subst : 
  (Atoms.atom * Terms.complete_type) substitution -> Atoms.atom substitution

val add_value : 'a substitution -> int * 'a Terms.term -> 'a substitution
val get_substitution : 'a substitution -> int -> 'a Terms.term option
val apply: 'a substitution -> int -> 'a Terms.term
val merge : 'a substitution -> 'a substitution -> 'a substitution

exception Bad_type

val typed_atom_map :  
    (Atoms.atom * Terms.complete_type ) substitution -> 
    Atoms.atom * Terms.complete_type -> 
    Terms.typed_term

val untyping_atom_map : 
    (Atoms.atom * Terms.complete_type ) substitution -> 
    Atoms.atom * Terms.complete_type -> 
    Atoms.atom Terms.term

val untyped_atom_map : 
    Atoms.atom substitution  -> 
    Atoms.atom -> 
    Atoms.atom Terms.term

val typed_term_map : 
    (Atoms.atom * Terms.complete_type ) substitution -> 
    Terms.typed_term -> 
    Terms.typed_term

val untyping_term_map : 
    (Atoms.atom * Terms.complete_type ) substitution -> 
    Terms.typed_term -> 
    Atoms.atom Terms.term

val untyped_term_map : 
    Atoms.atom substitution -> 
    Atoms.atom Terms.term -> 
    Atoms.atom Terms.term

val untyping_term_map_var :
  Atoms.atom substitution ->
  Terms.typed_term -> 
  Atoms.atom Terms.term

val untyped_pattern_matching_terms :
  Atoms.atom Terms.term ->
  Atoms.atom Terms.term -> 
  bool * Atoms.atom substitution

val typed_pattern_matching_terms :
  Terms.typed_term ->
  Terms.typed_term ->
  bool * (Atoms.atom * Terms.atom_type Terms.term) substitution
