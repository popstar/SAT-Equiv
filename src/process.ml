open Atoms ;;
open Terms ;;

type 'a process = In of int*('a term)*('a process) | Out of int*('a term)*('a process) | Null ;;

type typed_process = (atom*complete_type) process ;;

type protocol = (typed_process * atom process) list ;;

let rec untype_process = function
        | Null -> Null
        | In(c,t,p) -> In(c,untype t,untype_process p)
        | Out(c,t,p) -> Out(c,untype t,untype_process p)
;;

let get_next = function
        | In(_,_,x) -> x
        | Out(_,_,x) -> x
        | Null -> raise (Invalid_argument "null process in get_next in process.ml")
;;

let get_channel = function
        | In(c,_,_) -> c
        | Out(c,_,_) -> c
        | Null ->  raise (Invalid_argument "null process in get_channel in process.ml")
;;

let is_null = function
        | Null -> true
        | _ -> false
;;

let rec process_length (resOut,resIn) = function
  | Null -> (resOut,resIn)
  | In(_,_,p)  -> process_length (resOut,resIn+1) p
  | Out(_,_,p) -> process_length (resOut+1,resIn) p
;;

let max_interleaving_length (p:protocol) =
  let rec aux (resOut,resIn) = function
  | [] -> (resOut,resIn)
  | (a,_)::t -> aux (process_length (resOut,resIn) a) t
  in aux (0,0) p
;;



let rec print_process print_term = function
        | Null -> print_string "0" 
        | In(n,t,p) -> 
                        begin 
                                print_string "in(c_" ; 
                                print_int n ; print_string ", " ; 
                                print_term t ; print_string ")." ; 
                                print_process print_term p
                        end
        | Out(n,t,p) ->
                        begin 
                                print_string "out(c_" ; 
                                print_int n ; print_string ", " ; 
                                print_term t ; print_string ")." ; 
                                print_process print_term p
                        end
;;

let print_process_P = print_process print_typed_term ;;
let print_process_Q = print_process print_term ;;
