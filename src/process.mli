type 'a process =
    In of int * 'a Terms.term * 'a process
  | Out of int * 'a Terms.term * 'a process
  | Null
type typed_process = (Atoms.atom * Terms.complete_type) process
type protocol = (typed_process * Atoms.atom process) list 
val untype_process : typed_process -> Atoms.atom process
val get_next : 'a process -> 'a process
val get_channel : 'a process -> int
val is_null : 'a process -> bool
val max_interleaving_length : protocol -> int*int
val print_process_P : typed_process -> unit
val print_process_Q : Atoms.atom process -> unit
