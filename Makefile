all:	satequiv


satequiv: atoms.mli atoms.ml terms.mli terms.ml substitutions.mli substitutions.ml multiset.mli multiset.ml process.mli process.ml terminaison.mli terminaison.ml variables.mli variables.ml position.mli position.ml flattening.mli flattening.ml translation.mli translation.ml parser.mli parser.ml type_compliant.mli type_compliant.ml mutex.mli mutex.ml static.mli static.ml planning_graph.mli planning_graph.ml dimacs.mli dimacs.ml read_solution.mli read_solution.ml read_plan.mli read_plan.ml minisat.mli minisat.ml affichage.mli affichage.ml algo.mli algo.ml main.ml
	ocamlopt -o satequiv unix.cmxa atoms.mli atoms.ml terms.mli terms.ml substitutions.mli substitutions.ml multiset.mli multiset.ml process.mli process.ml terminaison.mli terminaison.ml variables.mli variables.ml position.mli position.ml flattening.mli flattening.ml translation.mli translation.ml parser.mli parser.ml type_compliant.mli type_compliant.ml mutex.mli mutex.ml  static.mli static.ml planning_graph.mli planning_graph.ml dimacs.mli dimacs.ml read_solution.mli read_solution.ml read_plan.mli read_plan.ml minisat.mli minisat.ml affichage.mli affichage.ml algo.mli algo.ml main.ml 
	make clean

clean:
	rm -rf *.cmi *.cmx *.o
